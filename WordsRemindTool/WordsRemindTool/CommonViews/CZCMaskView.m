//
//  CZCMaskView.m
//  WordsRemindTool
//
//  Created by iOS on 2020/8/20.
//  Copyright © 2020 czc. All rights reserved.
//

#import "CZCMaskView.h"

@implementation CZCMaskView

- (instancetype)initWithFrame:(CGRect)frame{
      self = [super initWithFrame:frame];
      self.userInteractionEnabled = false;
      [self drawBorderMaskLayer];
      return self;
  }

- (void)drawBorderMaskLayer{
      //描边
     
      CGRect alphaRect = CGRectMake(0, self.frame.size.height/2 - 50 ,self.frame.size.width ,160);
    
      CGFloat cornerRadius = 10;

      CAShapeLayer *borderLayer = [CAShapeLayer layer];
      borderLayer.frame = self.bounds;
      borderLayer.lineWidth = 3;
      borderLayer.strokeColor = kThemeColor.CGColor;
      borderLayer.fillColor = [UIColor clearColor].CGColor;

      UIBezierPath *bezierPath=[UIBezierPath bezierPathWithRoundedRect:alphaRect
                                                          cornerRadius:cornerRadius];
      borderLayer.path = bezierPath.CGPath;

      [self.layer insertSublayer:borderLayer atIndex:0];


        {//镂空
            CAShapeLayer *maskLayer = [CAShapeLayer layer];
            maskLayer.frame = self.bounds;
            maskLayer.fillColor = [[UIColor blackColor] colorWithAlphaComponent:0.5].CGColor;
            
            UIBezierPath *bezierPath=[UIBezierPath bezierPathWithRect:self.bounds];
            [bezierPath appendPath:[[UIBezierPath bezierPathWithRoundedRect:alphaRect cornerRadius:cornerRadius] bezierPathByReversingPath]];
            maskLayer.path = bezierPath.CGPath;
            
            [self.layer insertSublayer:maskLayer atIndex:0];
        }

}

@end
