//
//  CountdownView.m
//  WordsRemindTool
//
//  Created by iOS on 2020/8/19.
//  Copyright © 2020 czc. All rights reserved.
//

#import "CountdownView.h"

@interface CountdownView()

@property (nonatomic,strong) NSTimer * timer;
@property (nonatomic,strong) UILabel * countLabel;
@property (nonatomic,assign) NSUInteger countNum;

@end

@implementation CountdownView

- (instancetype)init{
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}


- (void)createUI {
    
    self.backgroundColor = UIColor.clearColor;
    _countNum = RM.countDownNum;
    UIView *bgView =[[UIView alloc]init];
    bgView.backgroundColor = kRGBColor(244, 221, 0, 1);
    bgView.layer.cornerRadius = 35;
    bgView.clipsToBounds = true;
    [self addSubview:bgView];
    
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.center.equalTo(self);
        make.width.height.mas_equalTo(70);
    }];
    
    _countLabel = [UILabel new];
    _countLabel.textColor = UIColor.whiteColor;
    _countLabel.font = [UIFont systemFontOfSize:30];
    _countLabel.text = [NSString stringWithFormat:@"%ld",_countNum];
    [bgView addSubview:_countLabel];
    
    [_countLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.center.equalTo(bgView);
    }];
    
    
    if(!_timer){
        
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDownAction) userInfo:nil repeats:YES];
    }
    
}

- (void)countDownAction {
    
    _countNum--;
    if(_countNum > 0){
        
        _countLabel.text = [NSString stringWithFormat:@"%ld",_countNum];
        
    }else if(_countNum == 0){
        
        [_timer invalidate];
         _timer = nil;
        [self removeFromSuperview];
    }
    
}




@end
