//
//  CZCPlusButton.m
//  WordsRemindTool
//
//  Created by iOS on 2020/8/18.
//  Copyright © 2020 czc. All rights reserved.
//

#import "CZCPlusButton.h"
#import "CreatRemindViewController.h"
#import "CYLTabBarController.h"



@implementation CZCPlusButton

+ (void)load {
    //请在 `-[AppDelegate application:didFinishLaunchingWithOptions:]` 中进行注册，否则iOS10系统下存在Crash风险。
    //[super registerPlusButton];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.adjustsImageWhenHighlighted = NO;
    }
    return self;
}



#pragma mark - IBActions

- (void)clickPublish {
    // 如果按钮的作用是触发点击事件，则调用此方法
    CYLTabBarController *tabBarController = [self cyl_tabBarController];
    UINavigationController *viewController = tabBarController.selectedViewController;
    CreatRemindViewController *addVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CreatRemindVC"];
    [viewController pushViewController:addVC animated:true];
    

}

#pragma mark - CYLPlusButtonSubclassing

+ (id)plusButton {
    CZCPlusButton *button = [CZCPlusButton buttonWithType:UIButtonTypeCustom];
    UIImage *normalButtonImage = [UIImage imageNamed:@"tab_add"];
 
    [button setImage:normalButtonImage forState:UIControlStateNormal];
    [button setImage:normalButtonImage forState:UIControlStateHighlighted];
    [button setImage:normalButtonImage forState:UIControlStateSelected];
    [button addTarget:button action:@selector(clickPublish) forControlEvents:UIControlEventTouchUpInside];
//
//    [button sizeToFit];
    button.frame = CGRectMake(0.0, 0.0, 80, 80);
    return button;
}


+ (NSUInteger)indexOfPlusButtonInTabBar {
   return 1;
}

+ (CGFloat)multiplierOfTabBarHeight:(CGFloat)tabBarHeight{
    return 0.3;
}

+ (BOOL)shouldSelectPlusChildViewController{
    return true;
}
// constantOfPlusButtonCenterYOffset 大于 0 会向下偏移，小于 0 会向上偏移。
+ (CGFloat)constantOfPlusButtonCenterYOffsetForTabBarHeight:(CGFloat)tabBarHeight {
    return (CYL_IS_IPHONE_X ? - 6 : 4);
}


@end
