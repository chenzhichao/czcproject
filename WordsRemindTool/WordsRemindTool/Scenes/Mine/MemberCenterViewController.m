//
//  MemberCenterViewController.m
//  WordsRemindTool
//
//  Created by iOS on 2020/8/21.
//  Copyright © 2020 czc. All rights reserved.
//

#import "MemberCenterViewController.h"
#import "UINavigationController+Unitl.h"
#import "LYCBaseWebController.h"
#import <StoreKit/StoreKit.h>
#import "LoginViewController.h"

@interface MemberCenterViewController ()<SKPaymentTransactionObserver,SKProductsRequestDelegate>

@property (weak, nonatomic) IBOutlet UIView *vipInfoView;

@property (weak, nonatomic) IBOutlet UIView *vipPriceView;

@property (weak, nonatomic) IBOutlet UIView *vipFuncView;

@property (nonatomic, assign) NSInteger memberType;
@property (nonatomic, strong) NSString *orderID;
@property (nonatomic, strong) NSMutableArray *timeViewList;

@end

@implementation MemberCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"会员中心";
    _timeViewList = [NSMutableArray array];
    [self setOpenMemberView];
    [self setFunctionListView];
    [self loadData];
    [self getVipInfo];
    //设置支付服务
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    self.view.backgroundColor = UIColor.whiteColor;
    // Do any additional setup after loading the view.
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self.navigationController changeBarColor:kRGBColor(40, 40, 40, 1) withTintColor:UIColor.whiteColor withNavigationBarBottomLine:true];

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.tintColor = UIColor.blackColor;
    self.navigationController.navigationBar.barTintColor = kThemeColor;
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:self.navigationController.navigationBar.tintColor, NSForegroundColorAttributeName, nil]];
     [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
}

#pragma mark - 点击事件  同步、开通vip

- (IBAction)updateVip:(id)sender {
    
    if(!USER_DEFAULTS_GET(kLYCReceiptData)){
        
        [LYGeneralTool showTextHUD:@"当前无订单" inView:self.view hideAfterDelay:2.0];
    }else{
        
        [CZCHttpsTool PayCallbackSync:@{@"receipt_data":USER_DEFAULTS_GET(kLYCReceiptData)} withHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
            
            if(error.code == 1021){
                
                [LYGeneralTool showTextHUD:@"订单同步成功" inView:self.view hideAfterDelay:2.0];
                UILabel *vipLabel = [self.vipInfoView viewWithTag:13];
                vipLabel.text = @"已开通会员" ;
                
            }else{
                
                [LYGeneralTool showTextHUD:@"同步失败" inView:self.view hideAfterDelay:2.0];
            }
        }];
    }
    
}


- (IBAction)submitBtnClick:(id)sender {
    
    
    [self buyMember];
    
    
}


- (IBAction)vipAgrentmentClick:(id)sender {
    
    LYCBaseWebController * webVC = [[LYCBaseWebController alloc]init];
    CZCBaseInfoModel * model = [CZCBaseInfoModel readAPPBaseInfo:@"user_agreement"];
    if(model){
        webVC.url = model.val;
    }
    webVC.titleName = @"会员支付协议";
    [self.navigationController pushViewController:webVC animated:true];
}





-(void)loadData{
    
    [CZCHttpsTool getVipPriceHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
        
        
         NSArray * prices = (NSArray *)result;
        if([prices isKindOfClass:[NSArray class]] && prices.count > 0){
            
            for (int i= 0; i< self.timeViewList.count; i++) {
                
                UIView * targetView = self.timeViewList[i];
                UILabel *priceLabel = [targetView viewWithTag:101];
                UILabel *averageLabel = [targetView viewWithTag:102];
                for (NSDictionary * dict in prices) {
                    NSInteger month = [dict[@"month"] integerValue];
                    if((i == 0 && month == 12) || (i == 1 && month == 3) || (i == 2&& month == 1)){
                        NSString * priceStr = dict[@"actual_price"];
                        priceLabel.text = [NSString stringWithFormat:@"￥%@",priceStr];
                        double averagePrice = [priceStr doubleValue]/month;
                        averageLabel.text = [NSString stringWithFormat:@"%.2f元/月",averagePrice];
                    }
                }
            }
        }
       
    }];

}
- (void)getVipInfo {
    
    [CZCHttpsTool getVipInfo:[LYCUserModel fetchUser].token withHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
        
        if(error){
                   
        }else{
             [[NSNotificationCenter defaultCenter] postNotificationName: NOTI_USERLOGIN object: nil];
            NSDictionary *dataDic = result;
            NSInteger vipType = [dataDic[@"is_vip"] intValue];
            //是否vip 0：否； 1：是
            if(vipType == 0){
                USER_DEFAULTS_SET(nil, kLYCLUserVipInfo);
            }else if (vipType == 1){
                 USER_DEFAULTS_SET(@"1", kLYCLUserVipInfo);
            }
            
            dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         
                UILabel *nameLabel = [self.vipInfoView viewWithTag:12];
                nameLabel.text = CheckNonNullNonEmpty([LYCUserModel fetchUser].nickname) ? [LYCUserModel fetchUser].nickname : @"游客" ;;
                UILabel *vipLabel = [self.vipInfoView viewWithTag:13];
                vipLabel.text = CheckNonNullNonEmpty(dataDic[@"enddate"]) ? [NSString stringWithFormat:@"会员到期时间：%@",dataDic[@"enddate"]] : @"未开通会员" ;
                
            });
           
       }
    }];
}




- (void)setOpenMemberView{
    
    
    UIView *timeView = [[UIView alloc]init];
    [_vipPriceView addSubview:timeView];
    
    UIStackView *stackView = [[UIStackView alloc]init];
    [stackView setAxis:UILayoutConstraintAxisHorizontal];       //排列类型
    [stackView setDistribution:UIStackViewDistributionFillEqually]; //子控件大小一样
    [stackView setSpacing:20];                                      //子控件间距
    [timeView addSubview:stackView];
    
    [timeView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.center.width.height.equalTo(_vipPriceView);
    }];
    [stackView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(timeView);
    }];
    
    NSArray * timeInfos = @[@[@"12个月",@"￥188",@"15元/月"],@[@"3个月",@"￥88",@"29元/月"],@[@"1个月",@"￥40",@"40元/月"]];
    
    for (int i = 0; i<timeInfos.count; i++) {
        
        NSArray *infos = timeInfos[i];
        
        UIColor *bgColor = UIColor.whiteColor;
        UIColor *textColor = UIColor.blackColor;
        
        if(i == _memberType){
            bgColor = kRGBColor(238, 200, 126, 1);
        }
        
        
        UIView *bgView = [[UIView alloc]init];
        bgView.layer.cornerRadius = 8;
        bgView.layer.borderWidth = 0.5;
        bgView.layer.borderColor = kRGBColor(200, 200, 200, 1).CGColor;
        bgView.clipsToBounds = true;
        bgView.backgroundColor = bgColor;
        bgView.tag = i;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeBuyWayForMemeber:)];
        [bgView addGestureRecognizer:tapGesture];
        [stackView addArrangedSubview:bgView];
        [self.timeViewList addObject:bgView];
        
        UILabel *titleLabel = [[UILabel alloc]init];
        titleLabel.text =infos[0];
        titleLabel.font = [UIFont boldSystemFontOfSize:15];
        titleLabel.textColor = textColor;
        titleLabel.userInteractionEnabled = true;
        titleLabel.tag = 100;
        [bgView addSubview:titleLabel];
        
        UILabel *priceLabel = [[UILabel alloc]init];
        priceLabel.text = infos[1];
        priceLabel.font = [UIFont boldSystemFontOfSize:22];
        priceLabel.textColor = kRGBColor(226, 0, 0, 1);
        priceLabel.userInteractionEnabled = true;
        priceLabel.tag = 101;
        [bgView addSubview:priceLabel];
        
        UILabel *averageLabel = [[UILabel alloc]init];
        averageLabel.text = infos[2];
        averageLabel.font = [UIFont systemFontOfSize:11];
        averageLabel.textColor = kRGBColor(155, 155, 155, 1);
        averageLabel.textAlignment = NSTextAlignmentCenter;
        averageLabel.layer.cornerRadius = 11;
        averageLabel.clipsToBounds = true;
        averageLabel.tag = 102;
        averageLabel.userInteractionEnabled = true;
        [bgView addSubview:averageLabel];
        
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(bgView).offset(15);
            make.centerX.equalTo(bgView);
        }];
        
        [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.centerX.centerY.equalTo(bgView);
        }];
        
        [averageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.equalTo(bgView.mas_bottom).offset(-15);
            make.centerX.equalTo(bgView);
            make.height.mas_equalTo(22);
            make.width.mas_equalTo(75);
        }];
    }
    
}


- (void)setFunctionListView{
    
    UIView * functionView = [[UIView alloc]init];
    [_vipFuncView addSubview:functionView];
    
    [functionView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.center.width.height.equalTo(_vipFuncView);
    }];
    
    NSArray *titles = @[@"横竖屏切换",@"拍摄美颜",@"台词设置",@"新增特权功能"];
    for (int i = 0; i < titles.count ; i++) {
        
        UIImageView *bgView = [[UIImageView alloc]init];
        bgView.contentMode = UIViewContentModeScaleAspectFit;
        bgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"会员特权%d",i]];
        bgView.userInteractionEnabled = true;
        [functionView addSubview:bgView];
        
        UILabel *nameLabel = [[UILabel alloc]init];
        nameLabel.text = titles[i];
        nameLabel.textAlignment = NSTextAlignmentCenter;
        nameLabel.textColor = kRGBColor(74, 74, 74, 1);
        nameLabel.font = [UIFont boldSystemFontOfSize:kLYCScaleWidth(12)];
        [functionView addSubview:nameLabel];
        
        CGFloat y = 0;
        if(i >=4){
            y = 110;
        }
        CGFloat x = (60 + (kScreenW - 40 - 240)/3) * (i%4);
        
        [bgView  mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(functionView).offset(y);
            make.left.equalTo(functionView).offset(x);
            make.height.width.mas_equalTo(40);
            
        }];
        
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(bgView.mas_bottom).offset(10);
            make.centerX.equalTo(bgView);
            
        }];
        
    }
    
}




- (void)changeBuyWayForMemeber:(UITapGestureRecognizer *)gesture{
    
    NSInteger targetIndex = ((UIView *)gesture.view).tag;
    
    if(targetIndex == _memberType){
        
        return;
    }
    for (int i= 0; i< _timeViewList.count; i++) {
        
        UIView * targetView = _timeViewList[i];
        
        UIColor *bgColor = UIColor.whiteColor;
        UIColor *textColor = UIColor.blackColor;
        
        if(i == targetIndex){
            bgColor = kRGBColor(238, 200, 126, 1);
            textColor = UIColor.whiteColor;
        }
        targetView.backgroundColor = bgColor;

    }
    
    _memberType = targetIndex;
}




#pragma mark - 购买会员
- (void)buyMember{
    
    
    if(!USER_DEFAULTS_GET(kLYCLoginUserId)){
        
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"登录开通会员" preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"游客开通" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            [self makePayment];
            
         }];

        UIAlertAction *addBtn = [UIAlertAction actionWithTitle:@"去登录" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
                 LoginViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginVC"];
                 vc.hidesBottomBarWhenPushed = true;
                 [self.navigationController pushViewController:vc animated:true];
                 
               
            
            }];

         [alertVC addAction:cancel];
         [alertVC addAction:addBtn];
         [self presentViewController:alertVC animated:YES completion:nil];

         
    }else{
        
        [self makePayment];
    }
    
    

    
   

}

-(void)makePayment {
    
    
    //是否允许内购
        if ([SKPaymentQueue canMakePayments]) {
            NSLog(@"用户允许内购");
            
            //检测是否有未完成的交易
            NSArray* transactions = [SKPaymentQueue defaultQueue].transactions;
            if (transactions.count > 0) {
                SKPaymentTransaction* transaction = [transactions firstObject];
                if (transaction.transactionState == SKPaymentTransactionStatePurchased) {
                    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                }
            }
            
            [LYGeneralTool showLoadingHUDInView:self.view];
            [self getOrder];
         
            //bundleid+xxx 就是你添加内购条目设置的产品ID
            NSArray *product = @[@"Preposition1",@"Preposition3",@"Preposition12"];
            NSSet *nsset = [NSSet setWithArray:product];
            
            //初始化请求
            SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:nsset];
            request.delegate = self;
            
            //开始请求
            [request start];
            
        }else{
            NSLog(@"用户不允许内购");
        }

    
}


#pragma mark - 获取预订单

- (void)getOrder{
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc]init];
    
    NSString *month = @"12";
    if(_memberType == 1){
        month = @"3";
    }else if(_memberType == 2){
        month = @"1";
    }
    
  
    [param setValue:@"3" forKey:@"way"];
    [param setValue:@(11) forKey:@"type"];
    [param setValue:month forKey:@"month"];
    if(!USER_DEFAULTS_GET(kLYCLoginUserId)){
        [param setValue:@"1" forKey:@"ios_check"];
    }
    
    
    [CZCHttpsTool buyVipWithOrder:param withHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
        
        if(error){
            
        }else{
            NSDictionary *dataDic = result;
            self.orderID = dataDic[@"orderId"];
            
        }
        
    }];
}

#pragma mark - SKProductsRequestDelegate
//接收到产品的返回信息，然后用返回的商品信息进行发起购买请求
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    NSArray *product = response.products;
    
    //如果服务器没有产品
    if([product count] == 0){
        NSLog(@"没有该商品");
        return;
    }
    
    SKProduct *requestProduct = nil;
    NSString *month = @"12";
       if(_memberType == 1){
           month = @"3";
       }else if(_memberType == 2){
           month = @"1";
       }
    for (SKProduct *pro in product) {
        
        NSLog(@"%@", [pro description]);
        NSLog(@"%@", [pro localizedTitle]);
        NSLog(@"%@", [pro localizedDescription]);
        NSLog(@"%@", [pro price]);
        NSLog(@"%@", [pro productIdentifier]);
        
        //如果后台消费条目的ID与我这里需要请求的一样（用于确保订单的正确性）
        NSString *identfy = [NSString stringWithFormat:@"Preposition%@", month];
        
        if([pro.productIdentifier isEqualToString:identfy]){
            requestProduct = pro;
        }
    }
    
    //发送购买请求
    SKMutablePayment *payment = [SKMutablePayment paymentWithProduct:requestProduct];
    payment.applicationUsername = CheckNonNullNonEmpty([LYCUserModel fetchUser].userId) ? [LYCUserModel fetchUser].userId : @"99999" ; ;//可以是userId，也可以是订单id，跟你自己需要而定
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

#pragma mark - SKRequestDelegate
//请求失败
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"error:%@", error);
}

//请求结束
- (void)requestDidFinish:(SKRequest *)request
{
    NSLog(@"请求结束");
}

#pragma mark - SKPaymentTransactionObserver
//监听购买结果
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for(SKPaymentTransaction *tran in transactions){
        
        
        
        switch (tran.transactionState) {
            case SKPaymentTransactionStatePurchased:
                NSLog(@"交易完成");
                [self completeTransaction:tran];
                
                break;
            case SKPaymentTransactionStatePurchasing:
                NSLog(@"商品添加进列表");
                
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"已经购买过商品");
                //                [[SKPaymentQueue defaultQueue] finishTransaction:tran]; 消耗型商品不用写
                
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"交易失败");
                [[SKPaymentQueue defaultQueue] finishTransaction:tran];
                [LYGeneralTool hiddleHUDInView:self.view];
                [LYGeneralTool showTextHUD:@"交易失败，请稍后尝试" inView:nil hideAfterDelay:1.5];
                
                break;
            default:
                break;
        }
    }
}

//交易结束,当交易结束后还要去appstore上验证支付信息是否都正确,只有所有都正确后,我们就可以给用户方法我们的虚拟物品了。
- (void)completeTransaction:(SKPaymentTransaction *)transaction
{
    
    NSLog(@"交易结束");
    NSString *productID =  transaction.payment.productIdentifier;
    if(!productID){
        return;
    }
//     验证凭据，获取到苹果返回的交易凭据
//     appStoreReceiptURL iOS7.0增加的，购买交易完成后，会将凭据存放在该地址
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    // 从沙盒中获取到购买凭据
    NSData *receiptData = [NSData dataWithContentsOfURL:receiptURL];
    NSString *receiptStr = [receiptData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    
    USER_DEFAULTS_SET(receiptStr, kLYCReceiptData);
    
    if(USER_DEFAULTS_GET(kLYCLoginUserId)){
         
        NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
           [dict setValue:receiptStr forKey:@"receipt_data"];
           [dict setValue:transaction.transactionIdentifier forKey:@"ext"];
           [dict setValue:_orderID forKey:@"orderId"];
        
           
           
           [LYGeneralTool showLoadingHUDInView:self.view];
           [CZCHttpsTool PayCallBack:dict withHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
               
               [LYGeneralTool hiddleHUDInView:self.view];
               if(error){
                   
                   if(error.code == 1021){
                       

                       [LYGeneralTool showTextHUD:@"购买成功" inView:self.view hideAfterDelay:2.0];
                       USER_DEFAULTS_SET(@"1", kLYCLUserVipInfo)
                       [self getVipInfo];
                       

                   }else{
                        [LYGeneralTool showTextHUD:@"购买失败" inView:self.view hideAfterDelay:2.0];
                       
                   }
                   
               }
               
           }];
           //结束交易
    }else{
         [LYGeneralTool hiddleHUDInView:self.view];
         [LYGeneralTool showTextHUD:@"购买成功" inView:self.view hideAfterDelay:2.0];
         UILabel *vipLabel = [self.vipInfoView viewWithTag:13];
         vipLabel.text = @"已开通会员" ;
        
    }
  
   
}







@end
