//
//  CZCSettingViewController.m
//  WordsRemindTool
//
//  Created by iOS on 2020/8/21.
//  Copyright © 2020 czc. All rights reserved.
//

#import "CZCSettingViewController.h"
#import "LYCBaseWebController.h"
#import "LYCSettingItemCell.h"
#import "AboutUsViewController.h"

@interface CZCSettingViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataList;
@property (nonatomic, strong) UIButton *uploadButton;

@end

@implementation CZCSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    
     self.navigationItem.title = @"设置";
     _dataList=@[@"去评分",@"隐私政策",@"用户协议",@"关于我们"];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view);
        make.left.right.bottom.equalTo(self.view);
        
    }];
    self.tableView.backgroundColor = kThemeGrayColor;
    
     _uploadButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _uploadButton.titleLabel.font = [UIFont systemFontOfSize:kLYCScaleWidth(17)];
        [_uploadButton setTitle:@"退出登录" forState:UIControlStateNormal];
        [_uploadButton setTitleColor:kRGBColor(49,49, 49, 1) forState:UIControlStateNormal];
        _uploadButton.backgroundColor = UIColor.whiteColor;
        [_uploadButton addTarget:self action:@selector(signOut) forControlEvents:UIControlEventTouchUpInside];
        
        if (USER_DEFAULTS_GET(kLYCLoginUserId)) {
            
            _uploadButton.hidden =false;
            
        }else{
            _uploadButton.hidden =true;
        }
        [self.view addSubview:_uploadButton];
        [_uploadButton mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self.view).offset(290);
            make.centerX.equalTo(self.view);
            make.left.equalTo(self.view);
            make.height.mas_equalTo(50);
        }];
}

#pragma mark - 退出登录
- (void)signOut{
    
     USER_DEFAULTS_SET(nil, kLYCLoginUserId);
     USER_DEFAULTS_SET(nil, kLYCLUserVipInfo);
    LYCUserModel *model = [[LYCUserModel alloc]init];
    [model recordUserData:^(BOOL isSuccessful) {
        
        [LYGeneralTool showTextHUD:@"退出登录" inView:self.view hideAfterDelay:2.0];
         [[NSNotificationCenter defaultCenter] postNotificationName: NOTI_USERLOGIN object: nil];
        [self.navigationController popToRootViewControllerAnimated:true];
    }];
    
}

#pragma mark - UITableViewDelegate、UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _dataList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LYCSettingItemCell * cell = [[LYCSettingItemCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MySpaceCellID"];
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setMySettingCell:_dataList[indexPath.row] Index:indexPath];
    return  cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 0){
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:APP_URL] options:nil completionHandler:^(BOOL success) {
            
        }];
        
    }else if (indexPath.row == 2){
        
        LYCBaseWebController * webVC = [[LYCBaseWebController alloc]init];
        CZCBaseInfoModel * model = [CZCBaseInfoModel readAPPBaseInfo:@"user_agreement"];
        if(model){
            webVC.url = model.val;
        }
        webVC.titleName = @"用户协议";
        [self.navigationController pushViewController:webVC animated:true];
        
    }else if (indexPath.row == 1){
        
        LYCBaseWebController * webVC = [[LYCBaseWebController alloc]init];
        CZCBaseInfoModel * model = [CZCBaseInfoModel readAPPBaseInfo:@"privacy_policy"];
        if(model){
            webVC.url = model.val;
        }
        webVC.titleName = @"隐私政策";
        [self.navigationController pushViewController:webVC animated:true];
        
    }else if (indexPath.row == 3){
        
        AboutUsViewController *wrVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AboutUsVC"];
        wrVC.hidesBottomBarWhenPushed = true;
        [self.navigationController pushViewController:wrVC animated:true];
    }
}


#pragma mark - 懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0.01)];
        _tableView.backgroundColor = UIColor.whiteColor;
        _tableView.scrollEnabled = false;
        
    }
    return _tableView;
}


@end
