//
//  LoginViewController.m
//  WordsRemindTool
//
//  Created by iOS on 2020/8/21.
//  Copyright © 2020 czc. All rights reserved.
//

#import "LoginViewController.h"
#import "LYCBaseWebController.h"
#import "SFHFKeychainUtils.h"
#import <AuthenticationServices/AuthenticationServices.h>

API_AVAILABLE(ios(13.0))
@interface LoginViewController ()<ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding,TencentSessionDelegate>

@property (nonatomic,strong) TencentOAuth *tencentOAuth;
@property (weak, nonatomic) IBOutlet UIButton *wechatBtn;
@property (weak, nonatomic) IBOutlet UIButton *qqBtn;
@property (weak, nonatomic) IBOutlet UIView *appleView;


@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [[NSNotificationCenter defaultCenter] addObserver: self
                                                selector: @selector(wechatLoginUserCancelled)
                                                    name: NOTI_WXLOGIN_USERCANCELLED
                                                  object: nil];
       
       [[NSNotificationCenter defaultCenter] addObserver: self
                                                selector: @selector(wechatLoginAuthorized:)
                                                    name: NOTI_WXLOGIN_AUTHORIZED
                                                  object: nil];
    [self setupUI];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = kRGBColor(40, 40, 40, 1);
    self.navigationController.navigationBar.barTintColor = UIColor.whiteColor;
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:self.navigationController.navigationBar.tintColor, NSForegroundColorAttributeName, nil]];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.tintColor = UIColor.blackColor;
    self.navigationController.navigationBar.barTintColor = kThemeColor;
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:self.navigationController.navigationBar.tintColor, NSForegroundColorAttributeName, nil]];
   
}


- (void)setupUI{
    
    self.view.backgroundColor = UIColor.whiteColor;
    self.title = @"登录";
    if (@available(iOS 13.0, *)) {
        
        ASAuthorizationAppleIDButton *signButton = [ASAuthorizationAppleIDButton buttonWithType:ASAuthorizationAppleIDButtonTypeSignIn style:ASAuthorizationAppleIDButtonStyleWhiteOutline];
        [_appleView addSubview:signButton];
        [signButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.height.width.equalTo(_appleView);
        }];
        [signButton addTarget:self action:@selector(userAppIDLogin) forControlEvents:UIControlEventTouchUpInside];
    } else {
        // Fallback on earlier versions
    }

     if([WXApi isWXAppInstalled]){
         _wechatBtn.hidden = false;
     }else{
         _wechatBtn.hidden = true;
     }

    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mqq://"]]) {

        _qqBtn.hidden = false;

    }else{
        _qqBtn.hidden = true;
    }
}

- (IBAction)wechatBtnClick:(id)sender {
    
    
    if([WXApi isWXAppInstalled]){//判断用户是否已安装微信App
    
    SendAuthReq *req = [[SendAuthReq alloc] init];
    req.state = @"wx_oauth_authorization_state";//用于保持请求和回调的状态，授权请求或原样带回
    req.scope = @"snsapi_userinfo";//授权作用域：获取用户个人信息
            //唤起微信
    [WXApi sendReq:req completion:^(BOOL success) {
        
    }];
    }else{
    //自己简单封装的alert
        [LYGeneralTool showTextHUD:@"未安装微信应用或版本过低" inView:self.view hideAfterDelay:2.0];
            
    }
}


- (void)userAppIDLogin {
    if (@available(iOS 13.0, *)) {
        [self signInWithApple];
    } else {
        // Fallback on earlier versions
    }
}
- (IBAction)qqBtnClick:(id)sender {
    
     if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mqq://"]]) {
         
         self.tencentOAuth = [[TencentOAuth alloc]initWithAppId:QQ_APP_ID andDelegate:self];
         
         NSMutableArray *permission = [NSMutableArray array];
         
         permission = [NSMutableArray arrayWithObjects:@"get_user_info",@"get_simple_userinfo",nil];
         
         [self.tencentOAuth authorize:permission inSafari:NO];
         
     }else{
     //自己简单封装的alert
         [LYGeneralTool showTextHUD:@"未安装QQ应用或版本过低" inView:self.view hideAfterDelay:2.0];
             
     }
    

}






- (IBAction)agreetmentBtnClick:(id)sender {
    
     LYCBaseWebController * webVC = [[LYCBaseWebController alloc]init];
    CZCBaseInfoModel * model = [CZCBaseInfoModel readAPPBaseInfo:@"user_agreement"];
    if(model){
        webVC.url = model.val;
    }
    webVC.titleName = @"用户协议";
    [self.navigationController pushViewController:webVC animated:true];
}

- (IBAction)privateBtnClick:(id)sender {
    
     [self userServiceAgreement];
}




- (void)userServiceAgreement{
    
    LYCBaseWebController * webVC = [[LYCBaseWebController alloc]init];
    CZCBaseInfoModel * model = [CZCBaseInfoModel readAPPBaseInfo:@"privacy_policy"];
    if(model){
        webVC.url = model.val;
    }
    webVC.titleName = @"隐私政策";
    [self.navigationController pushViewController:webVC animated:true];
}


#pragma mark - 微信登录回调
- (void)wechatLoginUserCancelled {
    
}

- (void)wechatLoginAuthorized: (NSNotification*)notification {
    
    
    // Get Access Token
    
    [CZCHttpsTool weixinGetAccessToken:notification.object withHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
        
        if(error){
           
        }else{
            
            LYCUserModel * model = [[LYCUserModel alloc]init];
            model.refreshToken = result[@"refresh_token"];
            model.thirdToken = result[@"access_token"];
            model.loginType = 2;
            model.openid = result[@"openid"];
            //获取微信信息
            [self getWeiXinUserInfo:model];
        }
        
        
        
    }];
    
}


- (void)getWeiXinUserInfo:(LYCUserModel *)model{
    
    [CZCHttpsTool weixinGetUserInfo:model.thirdToken openid:model.openid withHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
        
        if(error) {return ;}
        
        model.nickname = result[@"nickname"];
        model.avatar = result[@"headimgurl"];
        model.openid = result[@"unionid"];
        model.loginType = 2;
        [self loginWithMoel:model];
    }];
}



#pragma mark --------- qq登录状态回调  TencentSessionDelegate------

- (void)tencentDidLogin{
    
    if (CheckNonNullNonEmpty(_tencentOAuth.accessToken)){
        
        NSLog(@"%@ == %@",_tencentOAuth.accessToken,_tencentOAuth.openId);
        
        [self.tencentOAuth getUserInfo];
        
    }else{
        
        NSLog(@"登录失败！没有获取到accessToken");
        
    }
    
}

/**
 
 * 登录失败后的回调
 
 */

- (void)tencentDidNotLogin:(BOOL)cancelled{
    
    if (cancelled){
        
        NSLog(@"用户取消登录");
        
    }else{
        
        NSLog(@"登录失败");
        
    }
    
}

- (void)tencentDidNotNetWork{
    
    NSLog(@"没有网络，无法登录");
    
}

- (void)getUserInfoResponse:(APIResponse *)response{
    
    NSLog(@"%@",response.jsonResponse);
    
    LYCUserModel *model = [[LYCUserModel alloc]init];
    model.nickname = response.jsonResponse[@"nickname"];
    model.avatar = response.jsonResponse[@"figureurl_qq"];
    model.loginType = 1;
    model.thirdToken = _tencentOAuth.accessToken;
    [self loginWithMoel:model];
}



#pragma mark - 用户登录    -登录方式 1 QQ； 2微信-
- (void)loginWithMoel:(LYCUserModel *)model{
    
    NSMutableDictionary  *param = [[NSMutableDictionary alloc]init];
    [param setValue:@(model.loginType) forKey:@"type"];
    [param setValue:model.nickname forKey:@"nickname"];
    [param setValue:model.avatar forKey:@"avatar"];
    if(model.loginType == 2){
        
        [param setValue:model.openid forKey:@"eid"];
    }else{
        [param setValue:model.thirdToken forKey:@"eid"];
    }
    
    
    [CZCHttpsTool LoginWithInfo:param withHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
      
        
        if(error){
            
        }else{
            NSDictionary *dataDic = result;
            if(dataDic[@"token"]){
                model.token = dataDic[@"token"];
                [self getLoginUserInfo:model];
            }
       
            
            
        }
    }];
}


#pragma mark - 个人数据获取
- (void)getLoginUserInfo:(LYCUserModel *)dataModel{
    
    [CZCHttpsTool getUserInfo:dataModel.token withHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
        
        if(error){ return ;}
        

        NSDictionary * dataDic = result;
        if(dataDic){
            
            LYCUserModel *model = [LYCUserModel mj_objectWithKeyValues:dataDic];
            NSInteger uid = [dataDic[@"uid"] integerValue];
            model.userId = [NSString stringWithFormat:@"%ld",uid];
            model.thirdToken = dataModel.thirdToken;
            model.refreshToken = dataModel.refreshToken;
            model.token  = dataModel.token;
            model.loginType = dataModel.loginType;
            USER_DEFAULTS_SET(model.userId, kLYCLoginUserId);
            USER_DEFAULTS_SET(@"true", kLYCLastLogin);
            [model recordUserData:^(BOOL isSuccessful) {
               
               [[NSNotificationCenter defaultCenter] postNotificationName: NOTI_USERLOGIN object: nil];
                [LYGeneralTool showTextHUD:@"登录成功" inView:nil hideAfterDelay:2.0];
               [self.navigationController popViewControllerAnimated:true];
            }];
            
        }
    }];
}








#pragma mark - 苹果登录
- (void)signInWithApple API_AVAILABLE(ios(13.0))
{
    ASAuthorizationAppleIDProvider *appleIDProvider = [ASAuthorizationAppleIDProvider new];
    ASAuthorizationAppleIDRequest *authAppleIDRequest = [appleIDProvider createRequest];
    // iCloud KeyChain password 需要iCloud权限
//    ASAuthorizationPasswordRequest *passwordRequest = [[ASAuthorizationPasswordProvider new] createRequest];
    NSMutableArray <ASAuthorizationRequest *>* array = [NSMutableArray arrayWithCapacity:2];
    if (authAppleIDRequest) {
        [array addObject:authAppleIDRequest];
    }
//    if (passwordRequest) {
//        [array addObject:passwordRequest];
//    }
    NSArray <ASAuthorizationRequest *>* requests = [array copy];
    
    ASAuthorizationController *authorizationController = [[ASAuthorizationController alloc] initWithAuthorizationRequests:requests];
    authorizationController.delegate = self;
    authorizationController.presentationContextProvider = self;
    [authorizationController performRequests];
}
#pragma mark - ASAuthorizationControllerDelegate
- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithAuthorization:(ASAuthorization *)authorization API_AVAILABLE(ios(13.0))
{
    if ([authorization.credential isKindOfClass:[ASPasswordCredential class]]) {
        // iCloud KeyChain password
        ASPasswordCredential *passwordCredential = (ASPasswordCredential *)authorization.credential;
        NSString *userIdentifier = passwordCredential.user;
        NSString *password = passwordCredential.password;
        
        NSLog(@"userIdentifier: %@", userIdentifier);
        NSLog(@"password: %@", password);
    } else if ([authorization.credential isKindOfClass:[ASAuthorizationAppleIDCredential class]])       {
        ASAuthorizationAppleIDCredential *credential = (ASAuthorizationAppleIDCredential *)authorization.credential;
        NSString *state = credential.state;
        NSString *userID = credential.user; //与后台账号绑定的
        NSPersonNameComponents *fullName = credential.fullName;
        NSString *email = credential.email;
        //Verification data: Identity token, code，验证数据，用于传给开发者后台服务器，然后开发者服务器再向苹果的身份验证服务端验证本次授权登录请求数据的有效性和真实性，详见 Sign In with Apple REST API。如果验证成功，可以根据 userIdentifier 判断账号是否已存在，若存在，则返回自己账号系统的登录态，若不存在，则创建一个新的账号，并返回对应的登录态给 App。
        NSString *authorizationCode = [[NSString alloc] initWithData:credential.authorizationCode encoding:NSUTF8StringEncoding]; // refresh token
        NSString *identityToken = [[NSString alloc] initWithData:credential.identityToken encoding:NSUTF8StringEncoding]; // access token
        // 用于判断当前登录的苹果账号是否是一个真实用户，取值有：unsupported、unknown、likelyReal。
        ASUserDetectionStatus realUserStatus = credential.realUserStatus;
        if (realUserStatus == ASUserDetectionStatusLikelyReal) {
            // 与后台交互
            [SFHFKeychainUtils storeUsername:kYBGLoginWithAppleUserName andPassword:userID forServiceName:kYBGLoginWithAppleServer updateExisting:YES error:nil];
            
        }
        LYCUserModel *model= [[LYCUserModel alloc]init];
        model.loginType = 4;
        model.nickname = [NSString stringWithFormat:@"苹果用户%@",[userID substringToIndex:6]];
        model.avatar = @"";
        model.thirdToken = identityToken;
        [self loginWithMoel:model];
        
        NSLog(@"state: %@", state);
        NSLog(@"userID: %@", userID);
        NSLog(@"fullName: %@", fullName);
        NSLog(@"email: %@", email);
        NSLog(@"authorizationCode: %@", authorizationCode);
        NSLog(@"identityToken: %@", identityToken);
        NSLog(@"realUserStatus: %@", @(realUserStatus));
    }
}

- (void)authorizationController:(ASAuthorizationController *)controller didCompleteWithError:(NSError *)error API_AVAILABLE(ios(13.0))
{
    NSString *errorMsg = nil;
    switch (error.code) {
        case ASAuthorizationErrorCanceled:
            errorMsg = @"用户取消了授权请求";
            break;
        case ASAuthorizationErrorFailed:
            errorMsg = @"授权请求失败";
            break;
        case ASAuthorizationErrorInvalidResponse:
            errorMsg = @"授权请求响应无效";
            break;
        case ASAuthorizationErrorNotHandled:
            errorMsg = @"未能处理授权请求";
            break;
        case ASAuthorizationErrorUnknown:
            errorMsg = @"授权请求失败未知原因";
            break;
    }
    NSLog(@"%@", errorMsg);
}
#pragma mark - ASAuthorizationControllerPresentationContextProviding

- (ASPresentationAnchor)presentationAnchorForAuthorizationController:(ASAuthorizationController *)controller API_AVAILABLE(ios(13.0))
{
    return self.view.window;
}

@end
