//
//  MineViewController.m
//  WordsRemindTool
//
//  Created by iOS on 2020/8/18.
//  Copyright © 2020 czc. All rights reserved.
//

#import "MineViewController.h"
#import "LYCBaseWebController.h"
#import "MySpaceCell.h"
#import "MemberCenterViewController.h"
#import "CZCSettingViewController.h"
#import "LoginViewController.h"

@interface MineViewController ()<UITableViewDelegate,UITableViewDataSource>


@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIView *loginView;
@property (nonatomic, strong) UIView *userInfoView;
@property (nonatomic, strong) NSArray *dataList;

@end

@implementation MineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self setupUI];
    [self getUserInfo];
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(updateBasicView) name: NOTI_USERLOGIN object: nil];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(USER_DEFAULTS_GET(kLYCLoginUserId)){
        [self getVipInfo];
    }
}

- (void)setupUI {
   
    self.view.backgroundColor = UIColor.whiteColor;
    _dataList=@[@[@"会员中心"],@[@"分享给好友",@"QQ反馈客服",@"设置"]];

    [self setupUserInfoView];
    
    
    [self.view addSubview:self.tableView];
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view).offset(155);
        make.left.right.bottom.equalTo(self.view);
        
    }];
}

- (void)setupUserInfoView{
    

    _userInfoView = [[UIView alloc]init];
    _userInfoView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:_userInfoView];
    
    UIImageView * iconView = [[UIImageView alloc]init];
    iconView.layer.cornerRadius = 30;
    iconView.layer.masksToBounds = YES;
    iconView.clipsToBounds = YES;
    iconView.tag = 10;
    [iconView sd_setImageWithURL:[NSURL URLWithString:[LYCUserModel fetchUser].avatar] placeholderImage:[UIImage imageNamed:@"user_photo"]];
    [_userInfoView addSubview:iconView];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.text = CheckNonNullNonEmpty([LYCUserModel fetchUser].nickname) ? [LYCUserModel fetchUser].userName : @"游客" ;
    titleLabel.font = [UIFont boldSystemFontOfSize:17];
    
    titleLabel.tag = 11;
    [_userInfoView addSubview:titleLabel];
 
    UILabel *detailLabel = [[UILabel alloc]init];
    if(CheckNonNullNonEmpty([LYCUserModel fetchUser].userId)){
          detailLabel.text = [NSString stringWithFormat:@"ID:%@", [LYCUserModel fetchUser].userId];
    }
    detailLabel.tag = 12;
    detailLabel.font = [UIFont systemFontOfSize:14];
    
    [_userInfoView addSubview:detailLabel];
    titleLabel.textColor = UIColor.blackColor;
    detailLabel.textColor = UIColor.blackColor;
    
    UIButton *btn = [[UIButton alloc]init];
    NSString *btnText = USER_DEFAULTS_GET(kLYCLoginUserId) ? @"开通会员" : @"我要登陆";
    if(USER_DEFAULTS_GET(kLYCLUserVipInfo)){
        btnText = @"尊享会员";
    }
    [btn setTitle:btnText forState:UIControlStateNormal];
    [btn setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
    [btn setBackgroundColor:kThemeColor];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    btn.tag = 13;
    btn.layer.cornerRadius = 22;
    btn.clipsToBounds = true;
    [_userInfoView addSubview:btn];
    [btn addTarget:self action:@selector(loginBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [_userInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.height.mas_equalTo(155);
        make.left.right.top.equalTo(self.view);
       
    }];
    
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.height.mas_equalTo(60);
        make.left.equalTo(self.userInfoView).offset(30);
        make.top.equalTo(self.userInfoView).offset(70);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(iconView.mas_right).offset(15);
        make.top.equalTo(self.userInfoView).offset(75);
        make.height.mas_equalTo(20);
    }];
    
    [detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(iconView.mas_right).offset(15);
        make.top.equalTo(titleLabel.mas_bottom).offset(5);
    }];
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.width.height.mas_equalTo(100);
        make.right.equalTo(self.userInfoView).offset(-30);
        make.top.equalTo(self.userInfoView).offset(75);
        make.height.mas_equalTo(44);
    }];
    
    
}


#pragma mark - 数据获取
- (void)getUserInfo{
    
    [CZCHttpsTool getUserInfo:LYCUserModel.fetchUser.token withHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
        
        if(error){ return ;}
        

        NSDictionary * dataDic = result;
        if(dataDic){
            
            LYCUserModel *model = [LYCUserModel mj_objectWithKeyValues:dataDic];
            NSInteger uid = [dataDic[@"uid"] integerValue];
            model.userId = [NSString stringWithFormat:@"%ld",(long)uid];
            USER_DEFAULTS_SET(model.userId, kLYCLoginUserId);
            [model recordUserData:^(BOOL isSuccessful) {
                dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [self updateBasicView];
                });
            }];
        
        }
    }];
}

- (void)updateBasicView{
    
    UIImageView *iconView = [_userInfoView viewWithTag:10];
    [iconView sd_setImageWithURL:[NSURL URLWithString:[LYCUserModel fetchUser].avatar] placeholderImage:[UIImage imageNamed:@"user_photo"]];
    UILabel *nameLabel = [_userInfoView viewWithTag:11];
    nameLabel.text = CheckNonNullNonEmpty([LYCUserModel fetchUser].nickname) ? [LYCUserModel fetchUser].nickname : @"游客";
    
    UILabel *detailLabel = [_userInfoView viewWithTag:12];
    if(USER_DEFAULTS_GET(kLYCLoginUserId)){
          detailLabel.text = [NSString stringWithFormat:@"ID:%@", [LYCUserModel fetchUser].userId];;
    }else{
        detailLabel.text = @"";
    }
     UIButton *button = [_userInfoView viewWithTag:13];
    NSString *btnText = USER_DEFAULTS_GET(kLYCLoginUserId) ? @"开通会员" : @"我要登陆";
    if(USER_DEFAULTS_GET(kLYCLUserVipInfo)){
        btnText = @"尊享会员";
    }
    [button setTitle:btnText forState:UIControlStateNormal];
    
   
}

- (void)getVipInfo {
    
    [CZCHttpsTool getVipInfo:[LYCUserModel fetchUser].token withHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
        
        if(error){
                   
        }else{
          
            NSDictionary *dataDic = result;
            NSInteger vipType = [dataDic[@"is_vip"] intValue];
            //是否vip 0：否； 1：是
            if(vipType == 0){
                USER_DEFAULTS_SET(nil, kLYCLUserVipInfo);
            }else if (vipType == 1){
                 USER_DEFAULTS_SET(@"1", kLYCLUserVipInfo);
            }
            dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         
                UIButton *button = [self.userInfoView viewWithTag:13];
               NSString *btnText = USER_DEFAULTS_GET(kLYCLoginUserId) ? @"开通会员" : @"我要登陆";
               if([dataDic[@"is_vip"] intValue] == 1){
                   btnText = @"尊享会员";
               }
               [button setTitle:btnText forState:UIControlStateNormal];
            });
       }
    }];
}




- (void)loginBtnClick {
    
    if(USER_DEFAULTS_GET(kLYCLoginUserId)){
        
    
        MemberCenterViewController *member = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MemberCenterVC"];
        member.hidesBottomBarWhenPushed = true;
        [self.navigationController pushViewController:member animated:true];
        
    }else{
         LoginViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginVC"];
        vc.hidesBottomBarWhenPushed = true;
        [self.navigationController pushViewController:vc animated:true];
        
    }
    
    
}


#pragma mark - UITableViewDelegate、UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(section == 0){
        return 1;
    }else if(section == 1){
        return 3;
    }else{
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
     MySpaceCell * cell = [[MySpaceCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MySpaceCellID"];
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setMySpaceCell:_dataList[indexPath.section][indexPath.row] Index:indexPath];
    
    return  cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0){
        
        MemberCenterViewController *member = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MemberCenterVC"];
        member.hidesBottomBarWhenPushed = true;
        [self.navigationController pushViewController:member animated:true];
        
    }else if (indexPath.section == 1 && indexPath.row == 0){
        
//        LYCBaseWebController * webVC = [[LYCBaseWebController alloc]init];
//        CZCBaseInfoModel * model = [CZCBaseInfoModel readAPPBaseInfo:@"share_url"];
//        if(model){
//            webVC.url = model.val;
//        }
//        webVC.titleName = @"分享给好友";
//        webVC.rigthBtnType = 1;
//        [self.navigationController pushViewController:webVC animated:true];
         [self shareAction];
//
    }else if (indexPath.section == 1 && indexPath.row == 3){
        
//        //常见问题help_url、意见反馈feed_back
//        LYCBaseWebController * webVC = [[LYCBaseWebController alloc]init];
//        CZCBaseInfoModel * model = [CZCBaseInfoModel readAPPBaseInfo:@"feed_back"];
//        if(model){
//            webVC.url = model.val;
//        }
//        webVC.titleName = @"意见反馈";
//        [self.navigationController pushViewController:webVC animated:true];
        
    }else if (indexPath.section == 1 && indexPath.row == 1){
        
        NSString *qqStr = @"621398905";
        CZCBaseInfoModel * model = [CZCBaseInfoModel readAPPBaseInfo:@"qq_group_ios"];
        if(model){
            qqStr = model.val;
        }
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mqq://"]]) {
                
            NSString* urlStr = [NSString stringWithFormat:@"mqqapi://card/show_pslcard?src_type=internal&version=1&uin=%@&key=%@&card_type=group&source=external", qqStr, @"d2b26edf701959915753245605d87e415506cd38e20211d32eb4d43a6106c3c0"];
            NSURL *url = [NSURL URLWithString:urlStr];
             if([[UIApplication sharedApplication] canOpenURL:url]){
                 [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
             }
                
        }else{
        //自己简单封装的alert
            [LYGeneralTool showTextHUD:[NSString stringWithFormat:@"QQ群：%@",qqStr] inView:self.view hideAfterDelay:4.0];
                
        }
    
       
    }else if (indexPath.section == 1 && indexPath.row == 2){
        
        CZCSettingViewController *member = [[CZCSettingViewController alloc]init];
        member.hidesBottomBarWhenPushed = true;
        [self.navigationController pushViewController:member animated:true];
        
    }
}

- (void)shareAction {
    NSString *imgpath=[[NSBundle mainBundle] pathForResource:@"share" ofType:@".jpg"];
    UIImage * shareImage = [UIImage imageWithContentsOfFile:imgpath];
    NSString *titleName = [NSString stringWithFormat:@"提词器-提词神器"];
    NSURL *urlToShare = [NSURL URLWithString:APP_URL];
    NSArray *activityItems = @[titleName,shareImage,urlToShare];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityVC.excludedActivityTypes = @[
                                         UIActivityTypePrint,
                                         UIActivityTypeCopyToPasteboard,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypeMessage,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         UIActivityTypeOpenInIBooks,
                                         UIActivityTypeAirDrop];
    [self presentViewController:activityVC animated:YES completion:nil];
    // 分享之后的回调
    activityVC.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
        if (completed) {
            NSLog(@"completed");
            //分享 成功
        } else  {
            NSLog(@"cancled");
            //分享 取消
        }
    };
}



#pragma mark - 懒加载
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 1, kScreenW, 1)];
        _tableView.backgroundColor = kThemeGrayColor;
        _tableView.scrollEnabled = false;
        
    }
    return _tableView;
}




@end
