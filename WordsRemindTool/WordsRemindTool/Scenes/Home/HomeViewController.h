//
//  HomeViewController.h
//  WordsRemindTool
//
//  Created by iOS on 2020/8/18.
//  Copyright © 2020 czc. All rights reserved.
//

#import "CZCBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HomeViewController : CZCBaseViewController

@property (nonatomic,assign) NSInteger type;  //1为搜索

@end

NS_ASSUME_NONNULL_END
