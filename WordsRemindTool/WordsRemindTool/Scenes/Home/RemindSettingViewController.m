//
//  RemindSettingViewController.m
//  WordsRemindTool
//
//  Created by iOS on 2020/8/19.
//  Copyright © 2020 czc. All rights reserved.
//

#import "RemindSettingViewController.h"
#import "AppDelegate.h"

@interface RemindSettingViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UISlider *sizeSlider;
@property (weak, nonatomic) IBOutlet UILabel *sizeLabel;
@property (weak, nonatomic) IBOutlet UISlider *speedSlider;
@property (weak, nonatomic) IBOutlet UILabel *speedLabel;
@property (weak, nonatomic) IBOutlet UIStackView *textColorStack;
@property (weak, nonatomic) IBOutlet UIView *textColorView;
@property (weak, nonatomic) IBOutlet UIStackView *bgColorStack;
@property (weak, nonatomic) IBOutlet UIView *bgColorView;
@property (weak, nonatomic) IBOutlet UISlider *countdownSlider;
@property (weak, nonatomic) IBOutlet UILabel *countdownLaber;
@property (weak, nonatomic) IBOutlet UISwitch *mirrorSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *highSelectSwitch;
@property (weak, nonatomic) IBOutlet UIButton *verticalBtn;
@property (weak, nonatomic) IBOutlet UIButton *horizontalBtn;

@property (nonatomic,strong) NSArray *  textColorList;

@property (nonatomic,assign) BOOL orginSreenMode;


@end

@implementation RemindSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setBasicInfo];
    // Do any additional setup after loading the view.
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [RM recordRemindInfoModel:^(BOOL isSuccessful) {
        
    }];
    if(_orginSreenMode != RM.isVerticalMode){
        
         [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_VERTICAL_MODE object:nil];
    }
   

}

- (void)setupUI{
    
    AppDelegate * appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.allowRotation = false;
    appDelegate.shwoWordSetting = true;
    
    self.view.backgroundColor = kRGBColor(32, 32, 32, 0.05);
    _orginSreenMode = RM.isVerticalMode;
    
    if(RM.isVerticalMode){
        _scrollView.contentSize = CGSizeMake(kScreenW, KScreenH*1.5);
    }else{
         _scrollView.contentSize = CGSizeMake(KScreenH, kScreenW*2);
    }
    
   
    _sizeSlider.minimumValue = 15;
    _sizeSlider.maximumValue = 60;
   
    
    _speedSlider.minimumValue = 1;
    _speedSlider.maximumValue = 20;
    
    
    _countdownSlider.minimumValue = 1;
    _countdownSlider.maximumValue =10;
    
    NSArray *arrSlider = @[_sizeSlider,_speedSlider,_countdownSlider];
    for (UISlider *slider in arrSlider) {
         [slider addTarget:self action:@selector(sliderValueChange:) forControlEvents:UIControlEventValueChanged];
        slider.minimumTrackTintColor = kThemeColor;
        slider.maximumTrackTintColor = UIColor.whiteColor;
        slider.thumbTintColor = kThemeColor;
    }
     [_mirrorSwitch addTarget:self action:@selector(switchValueChange:) forControlEvents:UIControlEventTouchUpInside];
     [_highSelectSwitch addTarget:self action:@selector(switchValueChange:) forControlEvents:UIControlEventTouchUpInside];
    
    _textColorList = @[UIColor.whiteColor,UIColor.blackColor,UIColor.yellowColor,UIColor.cyanColor,UIColor.orangeColor,UIColor.redColor,UIColor.greenColor];
    
    NSArray *textColorBtns = _textColorStack.arrangedSubviews;
    for (UIButton *btn in textColorBtns) {
        [btn addTarget:self action:@selector(textColorBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    NSArray *textBgColorBtns = _bgColorStack.arrangedSubviews;
    for (UIButton *btn in textBgColorBtns) {
        [btn addTarget:self action:@selector(textBgColorBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
}



- (void)setBasicInfo{
    
    _sizeSlider.value = RM.fontSise;
    _sizeLabel.text = [NSString stringWithFormat:@"%ld",RM.fontSise];
    
    _speedSlider.value = RM.textSpeed;
    _speedLabel.text = [NSString stringWithFormat:@"%ld",RM.textSpeed];
    
    _textColorView.backgroundColor = RM.textColor;
    _bgColorView.backgroundColor = RM.textBgColor;
    
    _countdownSlider.value = RM.countDownNum;
    _countdownLaber.text = [NSString stringWithFormat:@"%ld",RM.countDownNum];
    
    _mirrorSwitch.on = RM.showMirrorMode;
    _highSelectSwitch.on = RM.showHighSelectMode;
    
    [self changeVerticalAndHorizontalBtnState:RM.isVerticalMode];

}


#pragma mark - 点击方法
- (void)sliderValueChange:(UISlider *)slider{
    
    NSUInteger value = round(slider.value);
    
    if(slider == _sizeSlider){
        
        RM.fontSise = value;
        _sizeLabel.text = [NSString stringWithFormat:@"%ld",value];
        [self sendChangeNotify];
    }else if(slider == _speedSlider){
        
        RM.textSpeed = value;
        _speedLabel.text = [NSString stringWithFormat:@"%ld",value];
        
    }else if(slider == _countdownSlider){
        RM.countDownNum = value;
        _countdownLaber.text = [NSString stringWithFormat:@"%ld",value];
    }
    
}

- (void)switchValueChange:(UISwitch *)sender{
    
    if(sender == _mirrorSwitch){
        
        RM.showMirrorMode = sender.on;
    }else if(sender == _highSelectSwitch){
        
        RM.showHighSelectMode = sender.on;
        
    }
}


- (void)textColorBtnClick:(UIButton *)sender{
    
    UIColor *textColor = _textColorList[sender.tag -10];
    _textColorView.backgroundColor = textColor;
    RM.textColor = textColor;
    [self sendChangeNotify];
}


- (void)textBgColorBtnClick:(UIButton *)sender{
    UIColor *textColor = _textColorList[sender.tag -20];
    _bgColorView.backgroundColor = textColor;
    RM.textBgColor = textColor;
    [self sendChangeNotify];
}



- (IBAction)verticalAction:(id)sender {
    
//    RM.isVerticalMode = true;
//    [self changeVerticalAndHorizontalBtnState:true];
    [self showDirationView];
}


- (IBAction)horizontalBtn:(id)sender {
    
//    RM.isVerticalMode = false;
//    [self changeVerticalAndHorizontalBtnState:false];
     [self showDirationView];
}

- (void)changeVerticalAndHorizontalBtnState:(BOOL)isVerticalMode{
    
    _verticalBtn.backgroundColor = isVerticalMode ? kThemeColor : kRGBColor(250, 250, 250, 1);
    
    _horizontalBtn.backgroundColor = !isVerticalMode ? kThemeColor : kRGBColor(250, 250, 250, 1);
    
    
}

- (void)sendChangeNotify {
    
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_WORD_SETTING object:nil];
}

- (void)showDirationView{
    
    
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"\n关闭竖屏锁定\n旋转屏幕即可切换横竖屏" preferredStyle:UIAlertControllerStyleAlert];

//      UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//       }];

      UIAlertAction *addBtn = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
          
          
       }];

//       [alertVC addAction:cancel];
       [alertVC addAction:addBtn];
       [self presentViewController:alertVC animated:YES completion:nil];
}


















@end
