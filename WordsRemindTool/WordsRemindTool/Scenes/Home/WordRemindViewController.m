//
//  WordRemindViewController.m
//  WordsRemindTool
//
//  Created by iOS on 2020/8/19.
//  Copyright © 2020 czc. All rights reserved.
//

#import "WordRemindViewController.h"
#import "CountdownView.h"
#import "UIViewController+CWLateralSlide.h"
#import "RemindSettingViewController.h"
#import "CZCMaskView.h"
#import "AppDelegate.h"

@interface WordRemindViewController ()<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *playBtn;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (nonatomic,strong) NSTimer * timer;
@property (nonatomic,strong) CZCMaskView * maskView;
@property (nonatomic,assign) float textTopSpace;

@end

@implementation WordRemindViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _textView.editable = false;
    [self changeScreenShowMode];
    float  topSpace = _textView.frame.size.height/2-50;
    _textTopSpace = _textView.frame.size.height/2 ;
    //文本与边框的距离(上,左,下右)
    _textView.textContainerInset = UIEdgeInsetsMake(topSpace, 30, 0, 30);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeScreenShowMode) name:NOTI_VERTICAL_MODE object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(changeRotate:) name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(setTextView) name:NOTI_WORD_SETTING object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self setTextView];
//   
//     [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

- (void)viewWillDisappear:(BOOL)animated{
   
    [super viewWillDisappear:animated];
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
}

- (void)setTextView {
    
    // 字体的行间距
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 20;
    NSDictionary *attributes = @{
                                    NSFontAttributeName:[UIFont systemFontOfSize:RM.fontSise],
                                    NSParagraphStyleAttributeName:paragraphStyle
                                        };
    _textView.text = [NSString stringWithFormat:@"%@ \n %@",_infoModel.title,_infoModel.content];
    
    _textView.attributedText = [[NSAttributedString alloc] initWithString:_textView.text attributes:attributes];
    
    _textView.backgroundColor = RM.textBgColor;
    _textView.textColor = RM.textColor;
    
  
    [_timer invalidate];
    _timer = nil;
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(textViewScroll) userInfo:nil repeats:YES];
          [self.timer setFireDate:[NSDate distantFuture]];
    _playBtn.selected = false;
    
    if(RM.showHighSelectMode){
        _maskView.hidden = false;
    }else{
        _maskView.hidden = true;
    }
    
    
    AppDelegate * appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.shwoWordSetting = false;
    appDelegate.allowRotation = true;//(以上2行代码,可以理解为打开横屏开关)
    [self setNewOrientation:!RM.isVerticalMode];//调用转屏代码

    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        float  topSpace = self.textView.frame.size.height/2 ;
        self.textTopSpace = self.textView.frame.size.height/2 ;
        
        //文本与边框的距离(上,左,下右)
        self.textView.textContainerInset = UIEdgeInsetsMake(topSpace, 30, 0, 30);
        [self.textView setContentOffset:CGPointMake(0, 0)];
    });
    
    if(RM.showMirrorMode){
        
        CGAffineTransform transform = CGAffineTransformMakeScale(-1,1);
         _textView.transform = transform;
        
    }else{
        
        _textView.transform = CGAffineTransformIdentity;
    }
    

}

- (void)changeScreenShowMode{

    [self.maskView removeFromSuperview];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        
        if(RM.showHighSelectMode){
            
            self.maskView = [[CZCMaskView alloc]initWithFrame:self.textView.frame];
             [self.view addSubview:self.maskView];
        }
        
    });
}


#pragma mark -  设置内容
- (void)showCountDownView {
    
    CountdownView *cdView = [[CountdownView alloc]init];
    [self.view addSubview:cdView];
    [cdView mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.width.height.mas_equalTo(70);
        make.centerX.equalTo(self.view);
        make.bottom.equalTo(self.textView).offset(-70);
    }];

}
#pragma mark - 横竖屏
- (void)setNewOrientation:(BOOL)fullscreen{
    if (fullscreen) {
        NSNumber *resetOrientationTarget = [NSNumber numberWithInt:UIInterfaceOrientationUnknown];
        [[UIDevice currentDevice] setValue:resetOrientationTarget forKey:@"orientation"];
        NSNumber *orientationTarget = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeRight];
        [[UIDevice currentDevice] setValue:orientationTarget forKey:@"orientation"];
    }else{
        NSNumber *resetOrientationTarget = [NSNumber numberWithInt:UIInterfaceOrientationUnknown];
        [[UIDevice currentDevice] setValue:resetOrientationTarget forKey:@"orientation"];
        NSNumber *orientationTarget = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
        [[UIDevice currentDevice] setValue:orientationTarget forKey:@"orientation"];
    }
}

- (void)changeRotate:(NSNotification*)noti {
    BOOL isVerticalMode = false;
    if ([[UIDevice currentDevice] orientation] == UIInterfaceOrientationPortrait
        || [[UIDevice currentDevice] orientation] == UIInterfaceOrientationPortraitUpsideDown) {
        //竖屏

        isVerticalMode = true;
    } else {
        //横屏
        isVerticalMode = false;
    }
    RM.isVerticalMode = isVerticalMode;
    [self setNewOrientation:!isVerticalMode];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
           
           float  topSpace = self.textView.frame.size.height/2 ;
           self.textTopSpace = self.textView.frame.size.height/2 ;
           
           //文本与边框的距离(上,左,下右)
           self.textView.textContainerInset = UIEdgeInsetsMake(topSpace, 30, 0, 30);
           [self.textView setContentOffset:CGPointMake(0, 0)];
       });
    [self changeScreenShowMode];
}




#pragma mark - 侧滑设置
- (void)setLeftSlide {
    
    RemindSettingViewController *settingVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"RemindSettingVC"];
    CWLateralSlideConfiguration *config = [[CWLateralSlideConfiguration alloc]init];
    config.direction = CWDrawerTransitionFromRight;
    [self cw_showDrawerViewController:settingVC animationType:CWDrawerAnimationTypeDefault configuration:config];
    
}




#pragma mark - 点击事件
- (IBAction)returnAction:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)startAction:(id)sender {
    
    //滚动完回到顶部
    if (_textView.contentOffset.y-_textTopSpace> (_textView.contentSize.height-_textView.bounds.size.height)) {
        
        [_textView setContentOffset:CGPointMake(0, 0)];
    }
    
    if(!_timer){
       
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(textViewScroll) userInfo:nil repeats:YES];
                 [self.timer setFireDate:[NSDate distantFuture]];
    }
    
    //点击播放、更改为选中
    _playBtn.selected = !_playBtn.selected;
    
    if(!_playBtn.selected){
        
         [self.timer setFireDate:[NSDate distantFuture]];
        
    }else{
        
        if(RM.showCountDownView){
            

             [self showCountDownView];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(RM.countDownNum * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.timer setFireDate:[NSDate distantPast]];
            });
            
        }else{
            
             [self.timer setFireDate:[NSDate distantPast]];
        }
         
    }


}
- (IBAction)settingAction:(id)sender {

    [self setLeftSlide];
    
}

#pragma mark - 滚动方法
- (void)textViewScroll {
    
    CGPoint pt = [_textView contentOffset];
    //值越大越快
    CGFloat n = pt.y + 0.25 + 0.1*(RM.textSpeed-1);
    [_textView setContentOffset:CGPointMake(pt.x, n)];
    if (n-_textTopSpace > (_textView.contentSize.height-_textView.bounds.size.height)) {
        [_timer invalidate];
        _timer = nil;
        _playBtn.selected = false;
    }
}


#pragma mark - 代理

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
}

- (void)dealloc{
    
    [_timer invalidate];
    _timer = nil;
  
    AppDelegate * appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.allowRotation = false;//关闭横屏仅允许竖屏
    [self setNewOrientation:false];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end
