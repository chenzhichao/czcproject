//
//  PlayViewController.m
//  WAVideoBox
//
//  Created by 黄锐灏 on 2019/1/6.
//  Copyright © 2019 黄锐灏. All rights reserved.
//

#import "PlayViewController.h"
#import <AVKit/AVKit.h>


@interface PlayViewController ()

@property(nonatomic,strong) AVPlayerViewController *playerController;
@property (nonatomic , strong) NSString *filePath;

@end

@implementation PlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"视频播放";
    self.view.backgroundColor = UIColor.clearColor;
    _playerController = [[AVPlayerViewController alloc] init];
    NSURL * url = [NSURL fileURLWithPath:_filePath];
    _playerController.player = [AVPlayer playerWithURL:url];
    _playerController.view.frame = self.view.bounds;
    _playerController.showsPlaybackControls = YES;
  
    [self.view addSubview:_playerController.view];
   
 
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[_playerController player] play];
}

- (void)loadWithFilePath:(NSString *)filePath{
    
    self.filePath = filePath;
    //程序缓存中的视频文件
    //var/mobile/Containers/Data/Application/6BDDBC10-0A75-4970-AA6F-060D5092FED1/Documents/TXUGC/TXUGCRecord.mp4  地址不能直接播放
    
}


@end
