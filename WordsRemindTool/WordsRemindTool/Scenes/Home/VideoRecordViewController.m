//
//  VideoRecordViewController.m
//  WordsRemindTool
//
//  Created by iOS on 2020/8/24.
//  Copyright © 2020 czc. All rights reserved.
//

#import "VideoRecordViewController.h"
@import TXLiteAVSDK_UGC;
#import <objc/message.h>
#import "CountdownView.h"
#import <UGCKit/UGCKitTheme.h>
#import <MediaPlayer/MediaPlayer.h>
#import <TCBeautyPanel/TCBeautyPanel.h>
#import <UGCKit/UGCKitRecordViewController.h>
#import "VideoPlayViewController.h"
#import "AppDelegate.h"


@interface VideoRecordViewController ()<TXUGCRecordListener> 
@property (weak, nonatomic) IBOutlet UIView *videoContentView;
@property (weak, nonatomic) IBOutlet UIView *videoRecordView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIView *settingView;
@property (weak, nonatomic) IBOutlet UIView *textAreaView;
@property (weak, nonatomic) IBOutlet UIStackView *stackView;
@property (weak, nonatomic) IBOutlet UIView *beatyView;
@property (weak, nonatomic) IBOutlet UIView *rateView;
@property (weak, nonatomic) IBOutlet UIView *recordView;
@property (weak, nonatomic) IBOutlet UIButton *recordBtn;
@property (weak, nonatomic) IBOutlet UIButton *cameraTypeBtn;

@property (weak, nonatomic) IBOutlet UISlider *wordSizeSlider;
@property (weak, nonatomic) IBOutlet UISlider *speedSlider;
@property (weak, nonatomic) IBOutlet UISlider *areaSlider;
@property (weak, nonatomic) IBOutlet UISlider *transSlider;
@property (weak, nonatomic) IBOutlet UISlider *psSlider;
@property (weak, nonatomic) IBOutlet UISlider *beatySlider;
@property (weak, nonatomic) IBOutlet UISlider *recordSlider;



@property (nonatomic,strong) id<TCBeautyPanelThemeProtocol> theme;;
@property (nonatomic,assign) BOOL isCameraPreviewOn;
@property (nonatomic,assign) BOOL isFrontCamera;
@property (nonatomic, strong) NSArray *arrSetingViews;
@property (nonatomic,strong) NSArray * arrTextColorList;
@property (nonatomic,strong) NSArray * arrFilterList;
@property (nonatomic,strong) NSArray * arrRateList;
@property (nonatomic, assign) NSInteger currentSettingIndex;
@property (nonatomic,strong) NSTimer * timer;
@property (nonatomic,assign) float textTopSpace;
@property (nonatomic,strong) TXUGCCustomConfig  *config;      // 录制配置
@property (nonatomic, strong) NSString* currentRecordTime;
@property (nonatomic, assign) CGFloat recordTime;


@end

@implementation VideoRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setBasicInfo];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(statusBarOrientationChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    AppDelegate * appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegate.shwoWordSetting = false;
    appDelegate.allowRotation = true;//(以上2行代码,可以理解为打开横屏开关)
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];

    if (_isCameraPreviewOn == NO) {
        [self setVideoRecordView];
    }else{
        //停止特效的声音
        [[[TXUGCRecord shareInstance] getBeautyManager] setMotionMute:NO];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:false animated:NO];
    [RM recordRemindInfoModel:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self stopCameraPreview];
}

- (void)statusBarOrientationChange:(NSNotification *)notification{
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    if(orientation == UIDeviceOrientationLandscapeRight || orientation == UIDeviceOrientationLandscapeLeft){
        [[TXUGCRecord shareInstance] setHomeOrientation:VIDOE_HOME_ORIENTATION_RIGHT];
        [[TXUGCRecord shareInstance] setRenderRotation:0];

    }else if(orientation == UIDeviceOrientationPortrait){
         [[TXUGCRecord shareInstance] setHomeOrientation:VIDEO_HOME_ORIENTATION_DOWN];
        [[TXUGCRecord shareInstance] setRenderRotation:0 ];
    }

}


- (void)setupUI{
      
    _arrSetingViews = @[_settingView,_textAreaView,_beatyView,_rateView];
    _currentSettingIndex = 0;
     _textTopSpace = _textView.frame.size.height/2 ;
    NSArray *sliders = @[_wordSizeSlider,_speedSlider,_areaSlider,_transSlider,_psSlider,_beatySlider,_recordSlider];
    for (UISlider *slider in sliders) {
        
         slider.minimumTrackTintColor = kThemeColor;
         slider.maximumTrackTintColor = UIColor.whiteColor;
         slider.thumbTintColor = kThemeColor;
         [slider addTarget:self action:@selector(sliderValueChange:) forControlEvents:UIControlEventValueChanged];
    }
    
     
    for (UIButton *btn in _stackView.arrangedSubviews) {
      
        [btn addTarget:self action:@selector(bottomSettingBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
     _wordSizeSlider.minimumValue = 15;
     _wordSizeSlider.maximumValue = 60;
    
     _speedSlider.minimumValue = 1;
     _speedSlider.maximumValue = 20;
    
     _areaSlider.minimumValue = 1;
     _areaSlider.maximumValue =10;
    
    _transSlider.minimumValue = 1;
    _transSlider.maximumValue =50;
    
    _psSlider.minimumValue = 1;
    _psSlider.maximumValue =10;
    
    _beatySlider.minimumValue = 1;
    _beatySlider.maximumValue = 10;
    
    _recordSlider.minimumValue = 1;
    _recordSlider.maximumValue = 20;
    
    
    _textView.textColor = RM.textColor;
    _textView.userInteractionEnabled = true;
    _textView.font = [UIFont boldSystemFontOfSize:RM.fontSise];
    
    _arrTextColorList = @[UIColor.whiteColor,UIColor.blackColor,UIColor.yellowColor,UIColor.cyanColor,UIColor.orangeColor,UIColor.redColor,UIColor.greenColor];
    
    UIStackView *colorStack = (UIStackView *)[_settingView viewWithTag:23];
    for (UIButton *btn in colorStack.arrangedSubviews) {
        [btn addTarget:self action:@selector(textColorBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }

    _arrFilterList = @[@"normal",@"chunzhen",@"white",@"weimei",@"huaijiu",@"landiao",@"rixi"];
    _arrRateList = @[];
    UIStackView *fiterStack = (UIStackView *)[_beatyView viewWithTag:17];
    for (int i = 0; i<fiterStack.arrangedSubviews.count; i++) {
        UIButton *btn = fiterStack.arrangedSubviews[i];
        [btn addTarget:self action:@selector(filterBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    UIStackView *rateStack = (UIStackView *)[_rateView viewWithTag:11];
       for (UIButton *btn in rateStack.arrangedSubviews) {
           [btn addTarget:self action:@selector(rateBtnClick:) forControlEvents:UIControlEventTouchUpInside];
       }
    
    [_videoContentView mas_updateConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(self.view.mas_top).offset(kLYCStatusBarHeight );
        make.height.mas_equalTo(self.view.frame.size.height - kLYCStatusBarHeight);
    }];
}

- (void)setBasicInfo{
    
     _textView.text = [NSString stringWithFormat:@"%@ \n %@",_infoModel.title,_infoModel.content];
    
    _wordSizeSlider.value = RM.fontSise;
    UILabel * sizeLabel = (UILabel *)[_settingView viewWithTag:22];
    sizeLabel.text = [NSString stringWithFormat:@"%ld",RM.fontSise];
    
    _speedSlider.value = RM.textSpeed;
    UILabel * speedLabel = (UILabel *)[_settingView viewWithTag:25];
    speedLabel.text = [NSString stringWithFormat:@"%ld",RM.textSpeed];
    
    UIView * colorView = (UILabel *)[_settingView viewWithTag:20];
    colorView.backgroundColor = RM.textColor;
    
    
    _areaSlider.value = RM.textArea;
    UILabel * areaLabel = (UILabel *)[_textAreaView viewWithTag:31];
    areaLabel.text = [NSString stringWithFormat:@"%ld",RM.textArea];
    
    
    _transSlider.value = RM.textTrans;
    UILabel * transLabel = (UILabel *)[_textAreaView viewWithTag:33];
    transLabel.text = [NSString stringWithFormat:@"%ld",RM.textTrans];
   
  
    [_textView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(100*RM.textArea/10 + 180);
        make.height.mas_equalTo(300*RM.textArea/10 + 100);
    }];
    
    
    _psSlider.value = RM.cameraMY;
    UILabel * psLabel = (UILabel *)[_beatyView viewWithTag:12];
    psLabel.text = [NSString stringWithFormat:@"%ld",RM.cameraMY];
    
    _beatySlider.value = RM.cameraMB;
    UILabel * beatyLabel = (UILabel *)[_beatyView viewWithTag:15];
    beatyLabel.text = [NSString stringWithFormat:@"%ld",RM.cameraMB];
    
    _recordSlider.value = RM.textSpeed;
    UILabel * recordSpeedLabel = (UILabel *)[_recordView viewWithTag:14];
    recordSpeedLabel.text = [NSString stringWithFormat:@"%ld",RM.textSpeed];
    
    _textView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:RM.textTrans /50.0f];
}



#pragma mark - 点击方法
- (void)sliderValueChange:(UISlider *)slider{
    
    NSUInteger value = round(slider.value);
    UILabel *label;
    TXBeautyManager *beautyManager = [[TXUGCRecord shareInstance] getBeautyManager];
    
    if(slider == _wordSizeSlider){
        
        RM.fontSise = value;
        label = (UILabel *)[_settingView viewWithTag:22];
        _textView.font = [UIFont boldSystemFontOfSize:value];

    }else if(slider == _speedSlider){
        
        RM.textSpeed = value;
        label = (UILabel *)[_settingView viewWithTag:25];
       
        
    }else if(slider == _areaSlider){
        RM.textArea = value;
         label = (UILabel *)[_textAreaView viewWithTag:31];
        //改变大小
      
        [_textView mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.width.mas_equalTo(100*RM.textArea/10 + 180);
            make.height.mas_equalTo(300*RM.textArea/10 + 100);
        }];
    }else if(slider == _transSlider){
        RM.textTrans = value;
         label = (UILabel *)[_textAreaView viewWithTag:33];
        //改变透明度
        self.textView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:value/50.0f];
    }else if(slider == _psSlider){
        RM.cameraMY = value;
        label = (UILabel *)[_beatyView viewWithTag:12];
        
        [beautyManager setBeautyLevel:RM.cameraMY];
//        [self stopCameraPreview];
//        [self setVideoRecordView];
        
    }else if(slider == _beatySlider){
        RM.cameraMB = value;
        label = (UILabel *)[_beatyView viewWithTag:15];
        [beautyManager setWhitenessLevel:RM.cameraMB];
    }
    else if(slider == _recordSlider){
        RM.textSpeed = value;
        label = (UILabel *)[_recordView viewWithTag:14];
    }
    
    label.text = [NSString stringWithFormat:@"%ld",value];
           
}


//底部设置栏
- (void) bottomSettingBtnClick:(UIButton *)sender{
    
    if(sender.tag == _currentSettingIndex + 10){
        return;
    }
    
    for (int i=0; i<_arrSetingViews.count; i++) {
        
        UIButton *btn = _stackView.arrangedSubviews[i];
        UIView *settingView = _arrSetingViews[i];
        if(i+10 == sender.tag){
            //选中，改变btn颜色 ，更改设置画面
            [btn setTitleColor:kThemeColor forState:UIControlStateNormal];
            settingView.hidden = false;
            _currentSettingIndex = i;
        }else{
            [btn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
            settingView.hidden = true;
        }
    }
}


- (void)textColorBtnClick:(UIButton *)sender{
    
    UIColor *textColor = _arrTextColorList[sender.tag -10];
    RM.textColor = textColor;
    UIView *colorView = (UILabel *)[_settingView viewWithTag:20];
    colorView.backgroundColor = textColor;
    
    
    _textView.textColor = textColor;
}

//滤镜
- (void)filterBtnClick:sender{
    
    UIButton *btn = sender;
    NSArray *arrImageName = @[@"normal",@"chunzhen",@"white",@"weimei",@"huaijiu",@"landiao",@"rixi"];
    TXBeautyManager *beautyManager = [[TXUGCRecord shareInstance] getBeautyManager];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"FilterResource" ofType:@"bundle"];
    NSString * itemPath = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", arrImageName[btn.tag-10]]];
    if(btn.tag== 10){
        [beautyManager setFilter:nil];
    }else{
        [beautyManager setFilter:[UIImage imageWithContentsOfFile:itemPath]];
        [beautyManager setFilterStrength:0.6];
    }
    

}

//视频比例
- (void)rateBtnClick:sender{
    
    UIButton *btn = sender;
    self.view.backgroundColor = UIColor.blackColor;
    CGFloat parentHeight = self.view.frame.size.height - kLYCStatusBarHeight;
    CGFloat topSpace= 0;
    CGFloat heigthSpace = self.view.frame.size.height - kLYCStatusBarHeight;
    TXVideoAspectRatio  ratio = VIDEO_ASPECT_RATIO_9_16;
    switch (btn.tag) {
        case 10:
            topSpace = kLYCStatusBarHeight;
            heigthSpace = heigthSpace;
             
            break;
        case 11:
            topSpace = (parentHeight - self.view.frame.size.width * 4 / 3)/2;
            heigthSpace = self.view.frame.size.width * 4 / 3;
            ratio = VIDEO_ASPECT_RATIO_3_4;
            break;
        case 12:
            topSpace = (parentHeight - self.view.frame.size.width * 9 / 16)/2;
            heigthSpace = self.view.frame.size.width * 9 / 16;
            ratio = VIDEO_ASPECT_RATIO_16_9;
            break;
        case 13:
            topSpace = (parentHeight - self.view.frame.size.width * 3 / 4)/2;
            heigthSpace = self.view.frame.size.width * 3 / 4;
             ratio = VIDEO_ASPECT_RATIO_4_3;
            break;
        case 14:
           topSpace = (parentHeight - self.view.frame.size.width )/2;
           heigthSpace = self.view.frame.size.width;
          ratio = VIDEO_ASPECT_RATIO_1_1;
            break;
            
        default:
            ratio = VIDEO_ASPECT_RATIO_9_16;
            break;
    }

    [[TXUGCRecord shareInstance] setAspectRatio:ratio];
    [_videoContentView mas_updateConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(self.view.mas_top).offset(topSpace );
        make.height.mas_equalTo(heigthSpace);
    }];
    

   
}



#pragma mark - 倒计时
- (void)showCountDownView {
    
    CountdownView *cdView = [[CountdownView alloc]init];
    [self.view addSubview:cdView];
    [cdView mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.width.height.mas_equalTo(70);
        make.centerX.equalTo(self.view);
        make.bottom.equalTo(self.stackView).offset(-70);
    }];

}

#pragma mark - 滚动方法
- (void)textViewScroll {
    
    CGPoint pt = [_textView contentOffset];
    CGFloat n = pt.y + 0.25 + 0.1*(RM.textSpeed-1);
    [_textView setContentOffset:CGPointMake(pt.x, n)];
    if (n-_textTopSpace > (_textView.contentSize.height-_textView.bounds.size.height)) {
        [_timer invalidate];
        _timer = nil;
        self.recordBtn.userInteractionEnabled = true;
    }
}





- (void)setVideoRecordView{

    
    if (_isCameraPreviewOn == NO){
        
        // 1. 配置录制参数
        _config = [[TXUGCCustomConfig alloc] init];
        _config.maxDuration = 30000;
        _config.minDuration = 1;
        //分辨率为1080
        _config.videoResolution = VIDEO_RESOLUTION_1080_1920;
        _config.videoBitratePIN = 12000;
        _config.videoFPS = 30;
        _config.GOP = 3;
        
        TXBeautyManager *beautyManager = [[TXUGCRecord shareInstance] getBeautyManager];
        [beautyManager setBeautyStyle:TXBeautyStyleNature];
        [beautyManager setBeautyLevel:RM.cameraMY-1];
        [beautyManager setWhitenessLevel:RM.cameraMB-1];
        // 2. 启动预览, 设置参数与在哪个View上进行预览
        [[TXUGCRecord shareInstance] startCameraCustom:_config preview:_videoContentView];
        
        _isCameraPreviewOn = YES;
        
        // 设置横竖屏录制
        [[TXUGCRecord shareInstance] setHomeOrientation:VIDEO_HOME_ORIENTATION_DOWN];
        
    }

}

-(void)stopCameraPreview
{
    if (_isCameraPreviewOn == YES)
    {
        
        [[TXUGCRecord shareInstance] stopCameraPreview];
        _isCameraPreviewOn = NO;
    }
    [self resetRecordViews];

}

- (void)resetRecordViews{
    
    UIView *view  =  _arrSetingViews[_currentSettingIndex];
    view.hidden = false;
    _stackView.hidden = false;
    self.recordView.hidden = true;
    if(_timer){
        [_timer invalidate];
        _timer = nil;
    }
    [[TXUGCRecord shareInstance].partsManager deleteAllParts];
    [_textView setContentOffset:CGPointMake(0, 0)];
}



#pragma mark - Record Control
-(void)startSDKRecord
{
    [TXUGCRecord shareInstance].recordDelegate = self;
    int result = [[TXUGCRecord shareInstance] startRecord];
    NSLog(@"开始拍摄,code:%d",result);
    _cameraTypeBtn.userInteractionEnabled = false;
    
    //显示录制时间
    
}

// 录制完成回调
-(void)onRecordComplete:(TXUGCRecordResult*)result
{
   if (result.retCode == UGC_RECORD_RESULT_OK) {
      // 录制成功， 视频文件在result.videoPath中
       NSLog(@"%@",result.videoPath);
       [self pushToVideoPlayVCPath:result.videoPath coverImage:result.coverImage];
   } else {
      // 错误处理，错误码定义请参见 TXUGCRecordTypeDef.h 中 TXUGCRecordResultCode 的定义
       [LYGeneralTool showTextHUD:@"录制失败" inView:nil hideAfterDelay:2.0];
       [self resetRecordViews];
   }
}

#pragma mark - TXUGCRecordListener
-(void) onRecordProgress:(NSInteger)milliSecond{
    _recordTime =  milliSecond / 1000.0;
    BOOL shouldPause = _recordTime >= _config.maxDuration;
    [self updateRecordProgressLabel: _recordTime];
    if (shouldPause) {
        [[TXUGCRecord shareInstance] stopRecord];
    }
}
- (void)updateRecordProgressLabel:(CGFloat)second {
    long min = (int)second / 60;
    long sec = (int)second % 60;
    UILabel * timeLabel = (UILabel *)[_recordView viewWithTag:11];
    _currentRecordTime = [NSString stringWithFormat:@"%02ld:%02ld", min, sec];
    [timeLabel setText:_currentRecordTime];
}



-(void)pushToVideoPlayVCPath:(NSString *)path coverImage:(UIImage *)image{
    VideoPlayViewController *playVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"VideoPlayVC"];
    playVC.strVideoPath = path;
    playVC.strVideoTime = _currentRecordTime;
    playVC.coverImage = image;
    playVC.hidesBottomBarWhenPushed = true;
    [self.navigationController pushViewController:playVC animated:true];
}


#pragma mark - 按钮点击事件

- (IBAction)returnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
- (IBAction)cameraAction:(id)sender {
        _isFrontCamera = !_isFrontCamera;
       [[TXUGCRecord shareInstance] switchCamera:_isFrontCamera];

       [[TXUGCRecord shareInstance] toggleTorch:false];
}

- (IBAction)recordBtnAction:(id)sender {
   
    if(!_recordBtn.selected){
        
        UIView *view  =  _arrSetingViews[_currentSettingIndex];
        view.hidden = true;
        _stackView.hidden = true;
       
        if(!_timer){
            
            _timer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(textViewScroll) userInfo:nil repeats:YES];
            [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
            [self.timer setFireDate:[NSDate distantFuture]];
            self.recordBtn.userInteractionEnabled = false;
            [self showCountDownView];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(RM.countDownNum * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.timer setFireDate:[NSDate distantPast]];
                self.recordView.hidden = false;
                self.recordBtn.userInteractionEnabled = true;
                //开始录制
                [self startSDKRecord];
            });
        }
        
        
    }else{
        
        //结束录制
         [[TXUGCRecord shareInstance] stopRecord];
    }
    
     _recordBtn.selected = !_recordBtn.selected;
   
}

- (IBAction)parseBtnAction:(id)sender {
    
    UIButton *pauseBtn = (UIButton *)sender;
    
    if(!pauseBtn.selected){
        
         [self.timer setFireDate:[NSDate distantFuture]];
        
    }else{
        
        [self.timer setFireDate:[NSDate distantPast]];
    }
    
    pauseBtn.selected = !pauseBtn.selected;
}



- (void)dealloc
{
    [[TXUGCRecord shareInstance] stopRecord];
    [[TXUGCRecord shareInstance] stopCameraPreview];
    [[TXUGCRecord shareInstance].partsManager deleteAllParts];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
