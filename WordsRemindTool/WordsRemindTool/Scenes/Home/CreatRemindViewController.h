//
//  CreatRemindViewController.h
//  WordsRemindTool
//
//  Created by iOS on 2020/8/18.
//  Copyright © 2020 czc. All rights reserved.
//

#import "CZCBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CreatRemindViewController : CZCBaseViewController

@property (nonatomic, strong) DialogueInfoModel *model;
@end

NS_ASSUME_NONNULL_END
