//
//  MySpaceCell.m
//  CZCWaterMarkTool
//
//  Created by czc on 2019/8/29.
//  Copyright © 2019年 czc. All rights reserved.
//

#import "MySpaceCell.h"

@interface MySpaceCell ()
{
    
    UIImageView * imageView;
    UILabel *titleLabel;
    UILabel *timeLabel;
}
@end

@implementation MySpaceCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        
        [self setupViews];
    }
    return  self;
}
-(void) setupViews{
    
    imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:imageView];
    
   
    titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont systemFontOfSize:kLYCScaleWidth(14)];
    titleLabel.textColor = kRGBColor(49, 49, 49, 1);
    [self addSubview:titleLabel];
    [titleLabel sizeToFit];
    
    timeLabel = [[UILabel alloc] init];
    timeLabel.font = [UIFont systemFontOfSize:kLYCScaleWidth(13)];
    timeLabel.textColor = kRGBColor(105, 105, 105, 1);
    timeLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:timeLabel];
    timeLabel.hidden = true;
    [timeLabel sizeToFit];
    
    titleLabel.textColor = UIColor.blackColor;
    timeLabel.textColor = kRGBColor(242, 242, 242, 1);
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self).offset(15);
        make.centerY.equalTo(self);
        make.height.width.mas_equalTo(25);
    }];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
         make.left.equalTo(self).offset(55);
        make.centerY.equalTo(self);
    }];
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.mas_right).offset(-40);
        make.centerY.equalTo(self);
    }];
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}



- (void)setMySpaceCell:(NSString *)context Index:(NSIndexPath *)indexPath{
    
    imageView.image = [UIImage imageNamed:context];
    titleLabel.text = context;
    
     timeLabel.hidden = true;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
