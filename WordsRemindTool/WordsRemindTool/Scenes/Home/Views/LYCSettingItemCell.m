//
//  LYCSettingItemCell.m
//  LYCampus-iPhone
//
//  Created by 4work on 22/08/2017.
//  Copyright © 2017 LYCampus. All rights reserved.
//

#import "LYCSettingItemCell.h"
@interface LYCSettingItemCell ()
{
    
    UILabel *titleLabel;
    UILabel *timeLabel;
}
@end

@implementation LYCSettingItemCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        
        [self setupViews];
    }
    return  self;
}
-(void) setupViews{
    
    
    titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont systemFontOfSize:kLYCScaleWidth(15)];
    titleLabel.textColor = kRGBColor(49, 49, 49, 1);
    [self addSubview:titleLabel];
    [titleLabel sizeToFit];
    
    timeLabel = [[UILabel alloc] init];
    timeLabel.font = [UIFont systemFontOfSize:kLYCScaleWidth(13)];
    timeLabel.textColor = kRGBColor(105, 105, 105, 1);
    timeLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:timeLabel];
    timeLabel.hidden = true;
    [timeLabel sizeToFit];
    
    titleLabel.textColor = UIColor.blackColor;
    timeLabel.textColor = kRGBColor(242, 242, 242, 1);
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self).offset(20);
        make.centerY.equalTo(self);
    }];
    
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.mas_right).offset(-40);
        make.centerY.equalTo(self);
    }];
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}



- (void)setMySettingCell:(NSString *)context Index:(NSIndexPath *)indexPath{
    
    titleLabel.text = context;
    
    if(indexPath.row == 0){
        
        timeLabel.hidden = false;
        timeLabel.text = @"5星好评";
        
    }else{
         timeLabel.hidden = true;
         timeLabel.textColor = kRGBColor(105, 105, 105, 1);
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
