//
//  LYCSettingItemCell.h
//  LYCampus-iPhone
//
//  Created by 4work on 22/08/2017.
//  Copyright © 2017 LYCampus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LYCSettingItemCell : UITableViewCell


- (void)setMySettingCell:(NSString *)context Index:(NSIndexPath *)indexPath;

@end
