//
//  MySpaceCell.h
//  CZCWaterMarkTool
//
//  Created by czc on 2019/8/29.
//  Copyright © 2019年 czc. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MySpaceCell : UITableViewCell

- (void)setMySpaceCell:(NSString *)context Index:(NSIndexPath *)indexPath;

@end

NS_ASSUME_NONNULL_END
