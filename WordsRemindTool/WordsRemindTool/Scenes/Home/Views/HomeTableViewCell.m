//
//  HomeTableViewCell.m
//  WordsRemindTool
//
//  Created by iOS on 2020/8/18.
//  Copyright © 2020 czc. All rights reserved.
//

#import "HomeTableViewCell.h"

@interface HomeTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *headLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *textBtn;
@property (weak, nonatomic) IBOutlet UIButton *cameraBtn;



@end

@implementation HomeTableViewCell

- (IBAction)deleteClick:(id)sender {
    
    if(_deleteHandle){
        self.deleteHandle(_deleteBtn.tag);
    }
}

- (IBAction)textClick:(id)sender {
    if(_remindHandle){
        self.remindHandle(_deleteBtn.tag);
    }
}

- (IBAction)cameraClick:(id)sender {
    if(_cameraBtn){
        self.cameraHandle(_deleteBtn.tag);
    }
}


-(void)setHomePageCellWithModel:(DialogueInfoModel *)model index:(NSInteger)index{
    
    //这里用model的id
    _deleteBtn.tag = index;
    _headLabel.text = model.title;
    _detailLabel.text= model.content;
    _timeLabel.text = model.edittime;
    
    
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}





@end
