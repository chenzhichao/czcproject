//
//  HomeTableViewCell.h
//  WordsRemindTool
//
//  Created by iOS on 2020/8/18.
//  Copyright © 2020 czc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^HomeCellDeleteHandle)(NSInteger index);
typedef void(^HomeCellRemindHandle)(NSInteger index);
typedef void(^HomeCellCameraHandle)(NSInteger index);

NS_ASSUME_NONNULL_BEGIN

@interface HomeTableViewCell : UITableViewCell

@property (nonatomic, copy) HomeCellDeleteHandle deleteHandle;
@property (nonatomic, copy) HomeCellRemindHandle remindHandle;
@property (nonatomic, copy) HomeCellCameraHandle cameraHandle;

 -(void)setHomePageCellWithModel:(DialogueInfoModel *)model index:(NSInteger)index;


@end

NS_ASSUME_NONNULL_END
