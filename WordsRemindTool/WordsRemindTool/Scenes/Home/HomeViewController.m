//
//  HomeViewController.m
//  WordsRemindTool
//
//  Created by iOS on 2020/8/18.
//  Copyright © 2020 czc. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeTableViewCell.h"
#import "WordRemindViewController.h"
#import "CreatRemindViewController.h"
#import "VideoRecordViewController.h"
#import "MemberCenterViewController.h"
#import "UIScrollView+EmptyDataSet.h"
#import "LoginViewController.h"
#import <BUAdSDK/BUAdSDK.h>

@interface HomeViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate,BUNativeExpressBannerViewDelegate>

@property (nonatomic,strong) UITableView * tableView;
@property (nonatomic,strong) NSMutableArray * dataArr;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic,assign) BOOL showEmptyView;
@property (nonatomic,strong) BUNativeExpressBannerView * bannerView;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    CZCBaseInfoModel * model = [CZCBaseInfoModel readAPPBaseInfo:@"ios_ad"];
    
    if(model && [model.val intValue] == 1){
        
        [self refreshBanner];
    }
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(_type != 1){
        [self reqeustData];
    }
   
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:true];
   
}

- (void)setupUI {
   
    if(_type != 1){
        self.title = @"提词器";
        UIButton *searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
        [searchBtn setBackgroundImage:[UIImage imageNamed:@"home_search"] forState:UIControlStateNormal];
          [searchBtn setBackgroundImage:[UIImage imageNamed:@"home_search"] forState:UIControlStateHighlighted];
        [searchBtn setBackgroundColor:UIColor.clearColor];
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:searchBtn];
         self.navigationItem.rightBarButtonItem = rightBarButtonItem;
        [searchBtn addTarget:self action:@selector(searchRemind) forControlEvents:UIControlEventTouchUpInside];
        
        [self getSystemData];
    }else{
        
        [_dataArr removeAllObjects];
        self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc]initWithCustomView:[UIView new]];
        
        UIButton *searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
        [searchBtn setTitle:@"取消" forState:UIControlStateNormal];
        [searchBtn setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:searchBtn];
         self.navigationItem.rightBarButtonItem = rightBarButtonItem;
        [searchBtn addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
        //搜索框
        _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(10, 0, 250, 45)];
        self.navigationItem.titleView = _searchBar;
        _searchBar.barStyle = UIBarStyleDefault;
        _searchBar.delegate = self;
        _searchBar.placeholder = @"搜索提词"; // 设置提示文字
        _showEmptyView = false;
        
        UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeKeyBoard)];
        [self.view addGestureRecognizer:tap];
    }
    
    
    
    
    _dataArr =[NSMutableArray array];
    _currentPage = 1;
    CGRect rect=CGRectMake(0, 0,kScreenW, self.view.frame.size.height - 90);
    if (@available(iOS 13.0, *)) {
        _tableView = [[UITableView alloc]initWithFrame:rect style:UITableViewStyleInsetGrouped];
    } else {
       _tableView = [[UITableView alloc]initWithFrame:self.view.frame style:UITableViewStyleGrouped];
    }
    [self.view addSubview:_tableView];
    [self.tableView registerNib:[UINib nibWithNibName:@"HomeTableViewCell" bundle:nil]
      forCellReuseIdentifier:@"HomeCell"];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    
    if(_type != 1){
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self.currentPage = 1;
            [self reqeustData];
        }];
        _tableView.mj_footer = [MJRefreshFooter footerWithRefreshingBlock:^{
            self.currentPage++;
            [self reqeustData];
        }];
    }

    
}






-(void)searchRemind {
    
    HomeViewController *homeVC =[[HomeViewController alloc]init];
    homeVC.type = 1;
    [self.navigationController pushViewController:homeVC animated:true];
    
}


-(void)doneAction {
    
    [self.navigationController popViewControllerAnimated:true];
}

-(void)closeKeyBoard {
    [_searchBar resignFirstResponder];
}

#pragma mark - 获取数据
- (void)getSystemData{
    
   
    
    [CZCHttpsTool getSystemConfigWithHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
       
        if(error){
            return ;
        }
        NSArray *configList = [CZCBaseInfoModel mj_objectArrayWithKeyValuesArray:result];
        [CZCBaseInfoModel recordAPPBaseInfo:configList];
    }];
    
    NSInteger delayTime = 1;
    CZCBaseInfoModel * model = [CZCBaseInfoModel readAPPBaseInfo:@"ios_ad"];
    
    if(model && [model.val intValue] == 1){
        delayTime = 6;
    }
    
    if(USER_DEFAULTS_GET(kLYCLoginUserId)){
        
        USER_DEFAULTS_SET(@"true", kLYCLastLogin);
    }
    
 
    
    //登录过的用户、获取vip信息:无效token，提示登录
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        
        if(USER_DEFAULTS_GET(kLYCLastLogin)){
            
            
            
            [CZCHttpsTool getVipInfo:[LYCUserModel fetchUser].token withHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
                
                if(error){
                    
                    if(error.code == 1001){
                        
                        USER_DEFAULTS_SET(nil, kLYCLoginUserId);
                        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"\n登录失效" preferredStyle:UIAlertControllerStyleAlert];

                        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                            
                            
                         }];

                        UIAlertAction *addBtn = [UIAlertAction actionWithTitle:@"去登录" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            
                                 LoginViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginVC"];
                                 vc.hidesBottomBarWhenPushed = true;
                                 [self.navigationController pushViewController:vc animated:true];
                                 
                               
                            
                            }];

                         [alertVC addAction:cancel];
                         [alertVC addAction:addBtn];
                         [self presentViewController:alertVC animated:YES completion:nil];
                        
                        
                    }
                    
                }else{
                    
                    NSDictionary * dataDic = result;
                    NSInteger vipType = [dataDic[@"is_vip"] intValue];
                    //是否vip 0：否； 1：是
                    if(vipType == 0){
                        USER_DEFAULTS_SET(nil, kLYCLUserVipInfo);
                    }else if (vipType == 1){
                         USER_DEFAULTS_SET(@"1", kLYCLUserVipInfo);
                    }
                }
                
                NSLog(@"用户vip状态：%@", USER_DEFAULTS_GET(kLYCLUserVipInfo));
            }];
        }
        
    });
    
}

- (void)addDefaultModel{
    
    DialogueInfoModel *defaultModel = [[DialogueInfoModel alloc]init];
    defaultModel.title = @"如何使用提词器？";
    defaultModel.content= @"今天刚用了一下，非常好用推荐大家都来试试，绝对会让你满意。主要是省去了很多麻烦的工作今天刚用了一下，非常好用推荐大家都来试试，绝对会让你满意。主要是省去了很多麻烦的工作今天刚用了一下，非常好用推荐大家都来试试，绝对会让你满意。主要是省去了很多麻烦的工作";
    defaultModel.edittime = [LYGeneralTool dateToString:[NSDate date] withDateFormat:@"yyyy-MM-dd HH:mm"];
    defaultModel.identify = 10000;
    _dataArr = [NSMutableArray arrayWithObject:defaultModel];

}



- (void)reqeustData {
    
    [CZCHttpsTool getDialogueList:_currentPage withHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
        if(error){
            [self addDefaultModel];
            [self.tableView reloadData];
             return ;
         }
               

           NSArray * list = result;
        
           if(list && list.count > 0){
               
               NSArray *restList = [DialogueInfoModel mj_objectArrayWithKeyValuesArray:list];
               if(self.currentPage == 1){
                   self.dataArr = [NSMutableArray arrayWithArray:restList];
               }else{
                   [self.dataArr addObjectsFromArray:restList];
               }
               [self.tableView reloadData];
               
           }else{
               [self addDefaultModel];
               [self.tableView reloadData];
           }
        
    }];
}


- (void)searchWordRemind {
    
    
    [CZCHttpsTool getSearchDialogue:_searchBar.text withHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
       
        NSArray * list = result;
        if(list && list.count > 0){
                      
              NSArray *restList = [DialogueInfoModel mj_objectArrayWithKeyValuesArray:list];
              self.dataArr = [NSMutableArray arrayWithArray:restList];
              
        }
        self.showEmptyView = true;
        [self.tableView reloadData];
        
    }];
}




#pragma mark - UISearchBar

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {

    NSLog(@"您点击了键盘上的Search按钮");
    
    if(CheckNonNullNonEmpty(searchBar.text)){
        [self searchWordRemind];
    }

}


- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {

    NSLog(@"输入完毕");
    
    if(CheckNonNullNonEmpty(searchBar.text)){
           [self searchWordRemind];
       }

    return YES;

}






#pragma mark - tableviewDelegete
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 175;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return _dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
        return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
       return 20;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
return [[UIView alloc]init];
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
return [[UIView alloc]init];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HomeTableViewCell * cell = (HomeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"HomeCell" forIndexPath:indexPath];
    cell.selectionStyle =  UITableViewCellSelectionStyleNone;
    if(_dataArr.count > 0){
         [cell setHomePageCellWithModel:_dataArr[indexPath.section] index:indexPath.section];
    }
    __weak typeof(self) weakSelf = self;
    cell.deleteHandle = ^(NSInteger index) {
        
        NSLog(@"点击删除%ld",(long)index);
        [self deleteBtnClick:index];
    };
    cell.remindHandle = ^(NSInteger index) {
        NSLog(@"点击提词%ld",(long)index);
        if([self testFreeNumberType:kLYCLFreeNumWord]){
            
            WordRemindViewController *wrVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"WordRemindVC"];
            wrVC.infoModel = self.dataArr[indexPath.section];
            wrVC.modalPresentationStyle = UIModalPresentationFullScreen;
            [weakSelf presentViewController:wrVC animated:true completion:nil];
        }

    };
    cell.cameraHandle = ^(NSInteger index) {
        NSLog(@"点击拍摄%ld",(long)index);
        if([self testFreeNumberType:kLYCLFreeNumVideo]){
            
            VideoRecordViewController *wrVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"VideoRecordVC"];
            wrVC.infoModel = self.dataArr[indexPath.section];
            [self.navigationController pushViewController:wrVC animated:true];
        }

    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"\n是否修改文本" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
     }];

    UIAlertAction *addBtn = [UIAlertAction actionWithTitle:@"修改" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        CreatRemindViewController *addVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CreatRemindVC"];
        //传值
        addVC.model = self.dataArr[indexPath.section];
        [self.navigationController pushViewController:addVC animated:true];
        
        
     }];

     [alertVC addAction:cancel];
     [alertVC addAction:addBtn];
     [self presentViewController:alertVC animated:YES completion:nil];

    
}

-(void)deleteBtnClick:(NSInteger)index{
    
    
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"\n是否删除提词" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                }];

    UIAlertAction *addBtn = [UIAlertAction actionWithTitle:@"删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        DialogueInfoModel *model = self.dataArr[index];
        [CZCHttpsTool getDeleteDialogueId:model.identify withHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
            [self reqeustData];
        }];
        
               }];

     [alertVC addAction:cancel];
     [alertVC addAction:addBtn];
     [self presentViewController:alertVC animated:YES completion:nil];

}

- (BOOL)testFreeNumberType:(NSString *)type{
    
    NSInteger number = 1;
    if(USER_DEFAULTS_GET(type)){
        number = [USER_DEFAULTS_GET(type) intValue];
    }
    
    
    BOOL result = true;
    CZCBaseInfoModel * model = [CZCBaseInfoModel readAPPBaseInfo:@"free_num"];
    if(model){
        if(!USER_DEFAULTS_GET(kLYCLUserVipInfo) && number > [model.val intValue]){
            result = false;
        }
    }
    if(!result){
        
        if(USER_DEFAULTS_GET(kLYCLoginUserId)){
            
            
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"\n免费体验次数已用完" preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        }];

            UIAlertAction *addBtn = [UIAlertAction actionWithTitle:@"开通vip" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                MemberCenterViewController *member = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MemberCenterVC"];
                member.hidesBottomBarWhenPushed = true;
                [self.navigationController pushViewController:member animated:true];
                
                       }];

             [alertVC addAction:cancel];
             [alertVC addAction:addBtn];
             [self presentViewController:alertVC animated:YES completion:nil];
        }else{
            
            NSString *title = @"\n免费体验次数已用完";
            NSString *action = @"开通vip";
            
            if(USER_DEFAULTS_GET(kLYCLastLogin)){
                
                title = @"\n当前未登录";
                action = @"去登陆";
            }
            
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:title preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        }];

            UIAlertAction *addBtn = [UIAlertAction actionWithTitle:action style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                MemberCenterViewController *member = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MemberCenterVC"];
                member.hidesBottomBarWhenPushed = true;
                [self.navigationController pushViewController:member animated:true];
                
                       }];

             [alertVC addAction:cancel];
             [alertVC addAction:addBtn];
             [self presentViewController:alertVC animated:YES completion:nil];
            
            
        }
        
      

        
    }else{
        USER_DEFAULTS_SET(@(number+1), type)
    }
    NSLog(@"%ld",number);
    return result;
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    if(self.showEmptyView){
        return [UIImage imageNamed:@"noData"];
    }else{
        return [UIImage new];
    }
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    
    if(self.showEmptyView){
        
        NSString *text = @"暂无数据";
        
          NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
          paragraph.lineBreakMode = NSLineBreakByWordWrapping;
          paragraph.alignment = NSTextAlignmentCenter;
          
          NSDictionary *attributes = @{
                                       NSFontAttributeName:[UIFont systemFontOfSize:14.0f],
                                       NSForegroundColorAttributeName:[UIColor lightGrayColor],
                                       NSParagraphStyleAttributeName:paragraph
                                       };
          
          return [[NSAttributedString alloc] initWithString:text attributes:attributes];
        
    }else{
        return [[NSAttributedString alloc]init];
    }
    
}


-  (void)refreshBanner {
    if (self.bannerView == nil) {
        CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
        CGFloat bannerHeigh = screenWidth/600*90;
        BUSize *imgSize = [BUSize sizeBy:BUProposalSize_Banner600_150];
        self.bannerView = [[BUNativeExpressBannerView alloc]initWithSlotID:@"887386491" rootViewController:self adSize:CGSizeMake(screenWidth, bannerHeigh) IsSupportDeepLink:YES interval:10.0];
        self.bannerView.frame = CGRectMake(0, 10, screenWidth, bannerHeigh);
        self.bannerView.delegate = self;
        [self.view addSubview:self.bannerView];
    }
    [self.bannerView loadAdData];
}

@end
