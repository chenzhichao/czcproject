//
//  CreatRemindViewController.m
//  WordsRemindTool
//
//  Created by iOS on 2020/8/18.
//  Copyright © 2020 czc. All rights reserved.
//

#import "CreatRemindViewController.h"
#import "LoginViewController.h"


@interface CreatRemindViewController ()

@property (weak, nonatomic) IBOutlet UITextField *texfield;

@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation CreatRemindViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setTextInfo];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:true];

}

-(void)setupUI {
    
    self.title = @"创建文本";
    UIButton *searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
    [searchBtn setTitle:@"完成" forState:UIControlStateNormal];
    [searchBtn setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:searchBtn];
     self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    [searchBtn addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    [_texfield becomeFirstResponder];
    _texfield.layer.borderColor = kRGBColor(242, 242, 242, 1).CGColor;
    _texfield.layer.cornerRadius = 4;
    _texfield.layer.borderWidth = 0.5;
    
//    _textView.layer.borderColor = kRGBColor(242, 242, 242, 1).CGColor;
//    _textView.layer.cornerRadius = 4;
//    _textView.layer.borderWidth = 0.5;
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeKeyBoard)];
    [self.view addGestureRecognizer:tap];
}

-(void)setTextInfo{
    
    if(_model !=nil){
        [_texfield resignFirstResponder];
        _texfield.text = _model.title;
        _textView.text = _model.content;
    }
}

-(void)doneAction {
    if(!CheckNonNullNonEmpty(_textView.text)){
        
        [LYGeneralTool showTextHUD:@"提词文本不能为空" inView:nil hideAfterDelay:1.5];
        return;
    }
    
    if(USER_DEFAULTS_GET(kLYCLoginUserId)){
        
            if(_model !=nil){
                
                [CZCHttpsTool getEditDialogue:_texfield.text content:_textView.text diaId:_model.identify withHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
                    [LYGeneralTool showTextHUD:@"修改成功" inView:self.view hideAfterDelay:1.5];
                    [self.navigationController popViewControllerAnimated:true];
                }];
            }else{
                [CZCHttpsTool getAddDialogue:_texfield.text content:_textView.text withHandle:^(id  _Nonnull result, NSError * _Nonnull error) {
                             
                             [LYGeneralTool showTextHUD:@"创建成功" inView:self.view hideAfterDelay:1.5];
                             [self.navigationController popViewControllerAnimated:true];
                     //        [[NSNotificationCenter defaultCenter] postNotificationName: NOTI_ADD_REMIND object: nil];
                         
                 }];
            }
            
        
         
    }else{
        
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"\n尚未登录" preferredStyle:UIAlertControllerStyleAlert];

                   UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                               }];

                   UIAlertAction *addBtn = [UIAlertAction actionWithTitle:@"去登录" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                       
                       LoginViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginVC"];
                       vc.hidesBottomBarWhenPushed = true;
                       [self.navigationController pushViewController:vc animated:true];
                       
                    }];

                    [alertVC addAction:cancel];
                    [alertVC addAction:addBtn];
                    [self presentViewController:alertVC animated:YES completion:nil];

    }
    

}



-(void)closeKeyBoard {
    [_texfield resignFirstResponder];
    [_textView resignFirstResponder];
}


@end
