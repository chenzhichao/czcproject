//
//  VideoPlayViewController.m
//  WordsRemindTool
//
//  Created by czc on 2020/8/28.
//  Copyright © 2020 czc. All rights reserved.
//

#import "VideoPlayViewController.h"
#import "PlayViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <Photos/Photos.h>

@interface VideoPlayViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end

@implementation VideoPlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    // Do any additional setup after loading the view.
}


- (void)setupUI{
    self.title =@"拍摄完成";
    UIButton *searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, 25)];
    [searchBtn setTitle:@"返回首页" forState:UIControlStateNormal];
    searchBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [searchBtn setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:searchBtn];
     self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    [searchBtn addTarget:self action:@selector(returnHomeAction) forControlEvents:UIControlEventTouchUpInside];
    _imageView.image = _coverImage;
    _timeLabel.text = [NSString stringWithFormat:@"时长：%@",_strVideoTime];
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
           
       }];
}

- (IBAction)playBtnAction:(id)sender {
    

    PlayViewController *playVC = [[PlayViewController alloc]init];
    [playVC loadWithFilePath:self.strVideoPath];
    [self.navigationController pushViewController:playVC animated:true];
    
}


- (void)saveVideoToAlbum{
    [LYGeneralTool showLoadingHUDInView:self.view];
    PHPhotoLibrary *photoLibrary = [PHPhotoLibrary sharedPhotoLibrary];
    [photoLibrary performChanges:^{
        [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:[NSURL fileURLWithPath:self.strVideoPath]];
    } completionHandler:^(BOOL success, NSError * _Nullable error) {
        

    }];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
          [LYGeneralTool showTextHUD:@"已保存到相册" inView:nil hideAfterDelay:1.5];
          [self returnHomeAction];
    });


}


//- (void)saveVideoToAlbum{
//    [LYGeneralTool showLoadingHUDInView:self.view];
//    PHPhotoLibrary *photoLibrary = [PHPhotoLibrary sharedPhotoLibrary];
//
//    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
//        if (status == PHAuthorizationStatusAuthorized) {
//            [photoLibrary performChanges:^{
//                PHAssetCreationRequest *request = [PHAssetCreationRequest creationRequestForAsset];
//                [request addResourceWithType:PHAssetResourceTypeVideo fileURL:[NSURL fileURLWithPath:self.strVideoPath] options:nil];
//
//            } completionHandler:^(BOOL success, NSError * _Nullable error) {
//
//            }];
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                  [LYGeneralTool showTextHUD:@"已保存到相册" inView:nil hideAfterDelay:1.5];
//                  [self returnHomeAction];
//            });
//
//        } else {
//
//            [LYGeneralTool showTextHUD:@"无相册写入权限" inView:nil hideAfterDelay:1.5];
//        }
//    }];
//
//}

- (IBAction)saveVideoAction:(id)sender {
    [self saveVideoToAlbum];
}


- (void)returnHomeAction{
    
    [self.navigationController popToRootViewControllerAnimated:true];
}


@end
