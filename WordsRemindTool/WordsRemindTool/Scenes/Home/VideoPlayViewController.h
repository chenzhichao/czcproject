//
//  VideoPlayViewController.h
//  WordsRemindTool
//
//  Created by czc on 2020/8/28.
//  Copyright © 2020 czc. All rights reserved.
//

#import "CZCBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface VideoPlayViewController : CZCBaseViewController

@property (nonatomic, strong) NSString *strVideoPath;
@property (nonatomic, strong) NSString *strVideoTime;
@property (nonatomic, strong) UIImage *coverImage;
@end

NS_ASSUME_NONNULL_END
