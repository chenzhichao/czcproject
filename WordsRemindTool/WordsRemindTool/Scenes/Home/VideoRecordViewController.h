//
//  VideoRecordViewController.h
//  WordsRemindTool
//
//  Created by iOS on 2020/8/24.
//  Copyright © 2020 czc. All rights reserved.
//

#import "CZCBaseViewController.h"
@protocol TCBeautyPanelThemeProtocol;

NS_ASSUME_NONNULL_BEGIN

@interface VideoRecordViewController : CZCBaseViewController

@property (nonatomic, strong) DialogueInfoModel *infoModel;

@end

NS_ASSUME_NONNULL_END
