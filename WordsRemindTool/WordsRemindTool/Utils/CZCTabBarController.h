//
//  CZCTabBarController.h
//  WordsRemindTool
//
//  Created by czc on 2020/8/17.
//  Copyright © 2020 czc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CYLTabBarController.h"

NS_ASSUME_NONNULL_BEGIN

@interface CZCTabBarController : CYLTabBarController

@end

NS_ASSUME_NONNULL_END
