//
//  HideNavBarBaseViewController.h
//  WordsRemindTool
//
//  Created by iOS on 2020/8/19.
//  Copyright © 2020 czc. All rights reserved.
//

#import "CZCBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface HideNavBarBaseViewController : CZCBaseViewController

@end

NS_ASSUME_NONNULL_END
