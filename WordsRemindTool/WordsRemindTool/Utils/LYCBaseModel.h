//
//  LYCBaseModel.h
//  LYCampus-iPhone
//
//  Created by 4work on 24/08/2017.
//  Copyright © 2017 LYCampus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface LYCBaseModel : NSObject


+ (void)attempLYCDBHelperDealloc;
+ (LKDBHelper *)getLYCDBHelper;
@property (nonatomic, assign) NSInteger colorIndex;

//默认主键是rowid。如需设置主键，必须在子类实现主键设置
/*
 + (NSString *)getPrimaryKey {
    return @"(PrimaryKey)";
 }
 */

//操作数据库例子
/**
 *    @brief  查询方法可以是SQL、字典
 */
- (void)demoRecordData:(void (^)(BOOL))block;

@end
