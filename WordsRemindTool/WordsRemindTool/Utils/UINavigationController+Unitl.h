
#import <UIKit/UIKit.h>

@interface UINavigationController (Unitl)

//隐藏导航栏底部的线条,viewWillDisappear
- (void)setNavigationBarBottomLine:(BOOL)hide;
//改变导航栏颜色
- (void)changeBarColor:(UIColor *)barTintColor withTintColor:(UIColor *)tintColor withNavigationBarBottomLine:(BOOL)hide;
- (void)setDefaultBarColor:(BOOL)hideBottomLine;

@end
