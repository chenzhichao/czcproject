//
//  CZCTabBarController.m
//  WordsRemindTool
//
//  Created by czc on 2020/8/17.
//  Copyright © 2020 czc. All rights reserved.
//

#import "CZCTabBarController.h"
#import "HomeViewController.h"
#import "MineViewController.h"
#import "CZCPlusButton.h"

@interface CZCTabBarController ()

@end

@implementation CZCTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

- (instancetype)init {
   if (!(self = [super init])) {
       return nil;
   }
    // 💡💡💡 注册加号按钮
    [CZCPlusButton registerPlusButton];
    CYLTabBarController *tabBarController = [CYLTabBarController tabBarControllerWithViewControllers:self.viewControllers tabBarItemsAttributes:self.tabBarItemsAttributesForController];
    self.tabBarController.tabBar.backgroundColor  = UIColor.whiteColor;
   return (self = (CZCTabBarController *)tabBarController);
    
    
}

- (NSArray *)viewControllers {
   HomeViewController *firstViewController = [[HomeViewController alloc] init];
   UIViewController *firstNavigationController = [[CYLBaseNavigationController alloc]
                                                  initWithRootViewController:firstViewController];
   [firstViewController cyl_setHideNavigationBarSeparator:YES];
   MineViewController *secondViewController = [[MineViewController alloc] init];
   UIViewController *secondNavigationController = [[CYLBaseNavigationController alloc]
                                                   initWithRootViewController:secondViewController];
   [secondViewController cyl_setHideNavigationBarSeparator:YES];
    
    
    
   NSArray *viewControllers = @[
                                firstNavigationController,
                                secondNavigationController,
                                ];
   return viewControllers;
}


- (NSArray *)tabBarItemsAttributesForController {
   NSDictionary *firstTabBarItemsAttributes = @{
                                                CYLTabBarItemTitle : @"首页",
                                                CYLTabBarItemImage :  @"tab_home",  /* NSString and UIImage are supported*/
                                                CYLTabBarItemSelectedImage : @"tab_home_select",  /* NSString and UIImage are supported*/
                                                };
   NSDictionary *secondTabBarItemsAttributes = @{
                                                 CYLTabBarItemTitle : @"我的",
                                                 CYLTabBarItemImage :  @"tab_mine",
                                                 CYLTabBarItemSelectedImage : @"tab_mine_select",
                                                 };
   

   NSArray *tabBarItemsAttributes = @[
                                      firstTabBarItemsAttributes,
                                      secondTabBarItemsAttributes,
                                      ];
   return tabBarItemsAttributes;
}


@end
