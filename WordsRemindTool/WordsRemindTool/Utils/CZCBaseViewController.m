//
//  CZCBaseViewController.m
//  WordsRemindTool
//
//  Created by czc on 2020/8/17.
//  Copyright © 2020 czc. All rights reserved.
//

#import "CZCBaseViewController.h"

@interface CZCBaseViewController ()

@end

@implementation CZCBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//     self.view.backgroundColor = kThemeBgColor;
    self.navigationController.navigationBar.translucent= false;
    self.navigationController.navigationBar.tintColor = UIColor.blackColor;
    self.navigationController.navigationBar.barTintColor = kThemeColor;
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:self.navigationController.navigationBar.tintColor, NSForegroundColorAttributeName, nil]];
    
      
    // Do any additional setup after loading the view.
}


@end
