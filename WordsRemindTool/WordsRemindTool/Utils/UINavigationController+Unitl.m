

#import "UINavigationController+Unitl.h" 

@implementation UINavigationController (Unitl)

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setDefaultBarColor:YES];
}
#pragma mark - 隐藏导航栏底部的线条
- (void)setNavigationBarBottomLine:(BOOL)hide {
    if (hide == YES) {
        [self.navigationBar setBackgroundImage:[[UIImage alloc]init] forBarMetrics:UIBarMetricsDefault];
        [self.navigationBar setShadowImage:[UIImage new]];
    } else {
        [self.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
        [self.navigationBar setShadowImage:nil];
    }
}
#pragma mark - 改变导航栏颜色
- (void)changeBarColor:(UIColor *)barTintColor withTintColor:(UIColor *)tintColor withNavigationBarBottomLine:(BOOL)hide {
    
    self.navigationBar.translucent= false;
    self.navigationBar.tintColor = tintColor;
    self.navigationBar.barTintColor = barTintColor;
    
    [self.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:self.navigationBar.tintColor, NSForegroundColorAttributeName, nil]];
    
    [self setNavigationBarBottomLine:hide];
}

- (void)setDefaultBarColor:(BOOL)hideBottomLine {
    self.navigationBar.tintColor = [UIColor blackColor];
    self.navigationBar.barTintColor = [UIColor whiteColor];
    
    [self.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:self.navigationBar.tintColor, NSForegroundColorAttributeName, nil]];
    
    [self setNavigationBarBottomLine:hideBottomLine];
} 
#pragma mark -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
