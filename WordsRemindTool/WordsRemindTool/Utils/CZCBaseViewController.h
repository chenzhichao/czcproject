//
//  CZCBaseViewController.h
//  WordsRemindTool
//
//  Created by czc on 2020/8/17.
//  Copyright © 2020 czc. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CZCBaseViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
