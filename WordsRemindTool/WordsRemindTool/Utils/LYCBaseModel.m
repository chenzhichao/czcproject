//
//  LYCBaseModel.m
//  LYCampus-iPhone
//
//  Created by 4work on 24/08/2017.
//  Copyright © 2017 LYCampus. All rights reserved.
//

#import "LYCBaseModel.h"

static LKDBHelper *LYCBaseDBHelper;
static dispatch_once_t LYCBaseDBOnceToken;



@implementation LYCBaseModel
#pragma mark - 销毁单例
+ (void)attempLYCDBHelperDealloc {
    LYCBaseDBOnceToken = 0;
    LYCBaseDBHelper = nil;
     
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dict = [defaults dictionaryRepresentation];
    for (id key in dict) {
        if (![(NSString *)key containsString:@"kTipAlert"] && ![(NSString *)key containsString:@"LoginUserName"] && ![(NSString *)key containsString:@"LYCHomepageTabIndex"]) {
            [defaults removeObjectForKey:key]; 
        }
    }
    
    [defaults synchronize];
}
#pragma mark - init
+ (LKDBHelper *)getLYCDBHelper {
    if (LYCBaseDBHelper == nil) {
        dispatch_once(&LYCBaseDBOnceToken, ^{
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *dbPath = [NSString stringWithFormat:@"%@/.LYCCacheDB", documentsDirectory];
            
            NSLog(@"data path:%@", dbPath);
            
            LYCBaseDBHelper = [[LKDBHelper alloc] initWithDBPath:dbPath];
        });
    }
    
    return LYCBaseDBHelper;
}

- (instancetype)init {
    self = [super init];
    
    
    return self;
}

+ (LKDBHelper *)getUsingLKDBHelper {
    return [LYCBaseModel getLYCDBHelper];
}
#pragma mark - 继承父类属性
+ (BOOL)isContainParent {
    return YES;
}
#pragma mark - 替换掉nil值
- (id)mj_newValueFromOldValue:(id)oldValue property:(MJProperty *)property { 
    if (property.type.typeClass == [NSArray class] || property.type.typeClass == [NSMutableArray class]) {
        if (oldValue == nil || [oldValue isEqual:[NSNull null]] || [oldValue isKindOfClass:[NSNull class]]) {
            if (property.type.typeClass == [NSArray class]) {
                oldValue = @[];
            } else if (property.type.typeClass == [NSMutableArray class]) {
                oldValue = @[].mutableCopy;
            }
        }
    } else {
        if (oldValue == nil || [oldValue isEqual:[NSNull null]] || [oldValue isKindOfClass:[NSNull class]]) {
            oldValue = @"";
        }
    }
    
    return oldValue;
}
#pragma mark - 忽略字段
+ (NSMutableArray *)mj_totalIgnoredPropertyNames {
    return [NSMutableArray arrayWithObjects:@"colorIndex", @"notiCount", nil];
}
#pragma mark - 返回字段描述
- (NSString *)description {
    return [NSString stringWithFormat:@"%@",  [self mj_keyValues]];
}
#pragma mark - 将要插入数据库
+ (BOOL)dbWillInsert:(NSObject *)entity {
    LKErrorLog(@"will insert : %@", NSStringFromClass(self));
    return YES;
}
#pragma mark - 已经插入数据库
+ (void)dbDidInserted:(NSObject *)entity result:(BOOL)result {
    LKErrorLog(@"did insert : %@", NSStringFromClass(self));
}
#pragma mark - 表名
+ (NSString *)getTableName {
    NSString *name = [NSString stringWithFormat:@"%@", [self class]];
    if ([name containsString:@"Model"]) {
        name = [name stringByReplacingOccurrencesOfString:@"Model" withString:@""];
    }
    
    return [NSString stringWithFormat:@"%@Table", name];
}
#pragma mark - 操作数据库例子
/*
 查询方法可以是SQL、字典
 */
- (void)demoRecordData:(void (^)(BOOL))block {
    NSDictionary *query = @{@"rowid": @(0)};;
    
    LKDBHelper *helper = [LYCBaseModel getLYCDBHelper];
    
    id model = [helper searchSingle:[self class] where:query orderBy:nil];
    
    if (model) {
        NSLog(@"update");
        [helper updateToDB:self where:query callback:^(BOOL result) {
            if (block) {
                block(result);
            }
        }];
    } else {
        [helper insertToDB:self callback:^(BOOL result) {
            if (block) {
                block(result);
            }
        }];
        NSLog(@"insert");
    }
}

@end
