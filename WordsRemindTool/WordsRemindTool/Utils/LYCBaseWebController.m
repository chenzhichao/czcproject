//
//  LYCBaseWebController.m
//  LYCampus-iPhone
//
//  Created by 4work on 31/08/2017.
//  Copyright © 2017 LYCampus. All rights reserved.
//

#import "LYCBaseWebController.h"
#import <WebKit/WebKit.h>
#import <SafariServices/SafariServices.h>
#import <Social/Social.h>


@interface LYCBaseWebController () <WKNavigationDelegate, WKScriptMessageHandler, WKUIDelegate>

@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, strong) WKNavigationAction *navigationAction;

@end

@implementation LYCBaseWebController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.  
    
    if (_url.length == 0) {
        _url = @"http://www.baidu.com";
    }
    self.navigationItem.title = _titleName;
    _progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    _progressView.frame = CGRectMake(0, 0, kScreenW,4);
    [_progressView setTrackTintColor:[UIColor whiteColor]];
    _progressView.progressTintColor = kThemeColor;
    [self.view addSubview:_progressView];
    
    _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, kScreenW, KScreenH-kLYCNavigationStatusBarHeight) configuration:[self forbadeWebViewScalable]];
    [self.view insertSubview:_webView belowSubview:_progressView];
    _webView.navigationDelegate = self;
    _webView.UIDelegate = self;
    [_webView addObserver:self forKeyPath:NSStringFromSelector(@selector(estimatedProgress))
                    options:0
                    context:nil];
    
    [_webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:NULL];
    

    [self webViewLoadRequest];
    
    if(_rigthBtnType == 1){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
         button.frame = CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.height+10, self.navigationController.navigationBar.frame.size.height);
         [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
         button.titleLabel.font = [UIFont systemFontOfSize:18];
         button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
         UIBarButtonItem *closeItem = [[UIBarButtonItem alloc] initWithCustomView:button];
         self.navigationItem.rightBarButtonItems = @[closeItem];
        [button setTitle:@"分享" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(shareAction) forControlEvents:UIControlEventTouchUpInside];


    }

}

#pragma mark - 加载网页
- (void)webViewLoadRequest{
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:_url]];
    
    [self.webView loadRequest:request];
    
}



#pragma mark - 禁止网页缩放
- (WKWebViewConfiguration *)forbadeWebViewScalable {
    WKWebViewConfiguration *webConfig = [WKWebViewConfiguration new];
    //是否支持JavaScript
    webConfig.preferences.javaScriptEnabled = YES;
    //不通过用户交互，是否可以打开窗口
    webConfig.preferences.javaScriptCanOpenWindowsAutomatically = YES;
    
    WKUserContentController *userController = [WKUserContentController new];
    NSString *js = @" $('meta[name=description]').remove(); $('head').append( '<meta name=\"viewport\" content=\"width=device-width, initial-scale=1,user-scalable=no\">' );";
    
    WKUserScript *script = [[WKUserScript alloc] initWithSource:js injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
    [userController addUserScript:script];
  
    webConfig.userContentController = userController;
    
    return webConfig;
}

#pragma mark - 返回
- (void)webCustomBackAction {

    if(_webView.canGoBack){
        [_webView goBack];
    
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - kvo 监听进度
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(nullable id)object change:(nullable NSDictionary<NSKeyValueChangeKey,id> *)change context:(nullable void *)context {
    if ([keyPath isEqualToString:NSStringFromSelector(@selector(estimatedProgress))] && object == _webView) {
        [_progressView setAlpha:1.0f];
        BOOL animated = _webView.estimatedProgress > _progressView.progress;
        [_progressView setProgress:_webView.estimatedProgress
                              animated:animated];
        
        if (_webView.estimatedProgress >= 1.0f) {
            [UIView animateWithDuration:0.3f
                                  delay:0.3f
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 [self.progressView setAlpha:0.0f];
                             }
                             completion:^(BOOL finished) {
                                 [self.progressView setProgress:0.0f animated:NO];
                             }];
        }
    } else if ([keyPath isEqualToString:@"title"]) {
        if (object == _webView && _webView.title.length > 0) {
            self.title = _webView.title;
        } else {
           
        }
    } else {
    }
}
#pragma mark - WKNavigationDelegate
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(null_unspecified WKNavigation *)navigation {
    //开始加载的时候，让进度条显示
    _progressView.hidden = NO;
   
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation {
    [LYGeneralTool hiddleHUDInView:_webView];

}

- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    
    [LYGeneralTool hiddleHUDInView:_webView];
    
} 

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    
    NSString *currlentUrl = [[navigationAction.request URL] absoluteString];
    NSLog(@"url:%@", currlentUrl);
    NSLog(@"host:%@", [navigationAction.request URL].host);
    NSLog(@"title:%@", webView.title);
    NSLog(@"navigationType:%ld", navigationAction.navigationType);
    NSLog(@"targetFrame:%@", navigationAction.targetFrame);
    
    decisionHandler(WKNavigationActionPolicyAllow);
}

- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * _Nullable credential))completionHandler {
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        NSURLCredential *card = [[NSURLCredential alloc] initWithTrust:challenge.protectionSpace.serverTrust];
        completionHandler(NSURLSessionAuthChallengeUseCredential, card);
    }
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
    decisionHandler(WKNavigationResponsePolicyAllow);
}
#pragma mark - WKUIDelegate
- (nullable WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures; {
    [webView loadRequest:navigationAction.request];
    
    return nil;
}

- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:message?:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:([UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler();
    }])];
    
    [alertController addAction:([UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        completionHandler();
    }])];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL result))completionHandler {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:message?:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:([UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(NO);
    }])];
    [alertController addAction:([UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(YES);
    }])];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(nullable NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * _Nullable result))completionHandler {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:prompt message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.text = defaultText;
    }];
    [alertController addAction:([UIAlertAction actionWithTitle:@"完成" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(alertController.textFields[0].text?:@"");
    }])];
    
    
    [self presentViewController:alertController animated:YES completion:nil];
}
#pragma mark - WKScriptMessageHandler
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    if([message.name isEqualToString:@"JsCallIos"]){
        
        if([message.body isEqualToString:@"ydxy_GetPhoto"]){
            
           
        }
    }
}


#pragma mark - 分享
- (void)shareAction {
    NSString *imgpath=[[NSBundle mainBundle] pathForResource:@"share" ofType:@".jpg"];
    UIImage * shareImage = [UIImage imageWithContentsOfFile:imgpath];
    NSString *titleName = [NSString stringWithFormat:@"提词器-提词神器"];
    NSURL *urlToShare = [NSURL URLWithString:APP_URL];
    NSArray *activityItems = @[titleName,shareImage,urlToShare];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityVC.excludedActivityTypes = @[
                                         UIActivityTypePrint,
                                         UIActivityTypeCopyToPasteboard,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypeMessage,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         UIActivityTypeOpenInIBooks,
                                         UIActivityTypeAirDrop];
    [self presentViewController:activityVC animated:YES completion:nil];
    // 分享之后的回调
    activityVC.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
        if (completed) {
            NSLog(@"completed");
            //分享 成功
        } else  {
            NSLog(@"cancled");
            //分享 取消
        }
    };
}



#pragma mark -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_webView removeObserver:self forKeyPath:NSStringFromSelector(@selector(estimatedProgress))];
    [_webView removeObserver:self forKeyPath:@"title"];
    
    [LYGeneralTool deleteWebCache];

}





@end
