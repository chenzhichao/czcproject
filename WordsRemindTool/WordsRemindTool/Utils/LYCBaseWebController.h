//
//  LYCBaseWebController.h
//  LYCampus-iPhone
//
//  Created by 4work on 31/08/2017.
//  Copyright © 2017 LYCampus. All rights reserved.
//

#import "CZCBaseViewController.h"



@interface LYCBaseWebController : CZCBaseViewController



@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *titleName;
@property (nonatomic,assign) NSInteger rigthBtnType; //1分享


@end
