//
//  NSDictionary+Unitl.m
//  LYCampus-iPhone
//
//  Created by 4work on 25/08/2017.
//  Copyright © 2017 LYCampus. All rights reserved.
//

#import "NSDictionary+Unitl.h"
#import "NSArray+Unitl.h"

@implementation NSDictionary (Unitl)

- (NSDictionary *)fetchDictionaryEmptyValue {
    NSMutableDictionary *mdic = [[NSMutableDictionary alloc] initWithDictionary:self];
    NSMutableArray *set = [[NSMutableArray alloc] init];
    NSMutableDictionary *dicSet = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *arrSet = [[NSMutableDictionary alloc] init];
    for (id obj in mdic.allKeys) {
        id value = mdic[obj];
        if ([value isKindOfClass:[NSDictionary class]]) {
            NSDictionary *changeDic = [value fetchDictionaryEmptyValue];
            [dicSet setObject:changeDic forKey:obj];
        } else if ([value isKindOfClass:[NSArray class]]) {
            NSArray *changeArr = [value fetchArrayEmptyValue];
            [arrSet setObject:changeArr forKey:obj];
        } else {
            if ([value isKindOfClass:[NSNull class]]) {
                [set addObject:obj];
            }
        }
    }
    
    for (id obj in set) {
        mdic[obj] = @"";
    }
    
    for (id obj in dicSet.allKeys) {
        mdic[obj] = dicSet[obj];
    }
    
    for (id obj in arrSet.allKeys) {
        mdic[obj] = arrSet[obj];
    }
    
    return mdic;
}
#pragma mark - 空处理
+ (instancetype)dictionaryWithObjects:(const id[])objects forKeys:(const id[])keys count:(NSUInteger)cnt {
    NSMutableArray *validKeys = [NSMutableArray new];
    NSMutableArray *validObjs = [NSMutableArray new];
    
    for (NSUInteger i = 0; i < cnt; i ++) {
        if (objects[i] && keys[i]) {
            [validKeys addObject:keys[i]];
            [validObjs addObject:objects[i]];
        }
    }
    
    return [self dictionaryWithObjects:validObjs forKeys:validKeys];
}

@end
