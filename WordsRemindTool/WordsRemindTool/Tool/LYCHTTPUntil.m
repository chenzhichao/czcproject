//
//  LYCHTTPUntil.m
//  LYCampus-iPhone
//
//  Created by 4work on 28/08/2017.
//  Copyright © 2017 LYCampus. All rights reserved.
//

#import "LYCHTTPUntil.h"
#import <sys/utsname.h>

@implementation LYCHTTPUntil
#pragma mark - 设备
/*
 deviceType：设备类型（1：android,2:apple）deviceModel:设备型号（eg: iphose 6s）deviceVersion:设备版本（eg: 10.1）clientVersion：客户端版本（eg: 3.0）
 */
+ (NSDictionary *)deviceBaseInfo {
    NSMutableDictionary* mdic = [[NSMutableDictionary alloc]init];
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];

    NSString *platform;
    platform = [LYCHTTPUntil devicePlatform];
    //手机型号
    [mdic setValue:platform forKey:@"phonemodel"];
    //系统版本号
    [mdic setValue:[infoDictionary objectForKey:@"CFBundleShortVersionString"] forKey:@"version"];
    //产品升级号(build)
    [mdic setValue:[infoDictionary objectForKey:@"CFBundleVersion"] forKey:@"vercode"];
    //产品渠道号
    [mdic setValue:@"qsy_ios" forKey:@"sid"];
    //产品id
    [mdic setValue:[NSString stringWithFormat:@"%d",4] forKey:@"pid"];
    //uuid
    [mdic setValue: [[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"uuid"];
    
    
    [mdic setValue:[NSString stringWithFormat:@"%d",4] forKey:@"sysver"];
    
    //时间戳
    NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];//获取当前时间0秒后的时间
    NSTimeInterval time=[date timeIntervalSince1970];// *1000 是精确到毫秒，不乘就是精确到秒
    NSString *timeString = [NSString stringWithFormat:@"%.0f", time];
    
    [mdic setValue:timeString forKey:@"timestamp"];
    
    return mdic;
}
#pragma mark - 设备平台
+ (NSString *)devicePlatform {
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *deviceString = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    //iPhone
    if ([deviceString isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([deviceString isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([deviceString isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([deviceString isEqualToString:@"iPhone3,1"])    return @"iPhone4";
    if ([deviceString isEqualToString:@"iPhone3,2"])    return @"Verizon iPhone 4";
    if ([deviceString isEqualToString:@"iPhone4,1"])    return @"iPhone4S";
    if ([deviceString isEqualToString:@"iPhone5,1"])    return @"iPhone5";
    if ([deviceString isEqualToString:@"iPhone5,2"])    return @"iPhone5";
    if ([deviceString isEqualToString:@"iPhone5,3"])    return @"iPhone5C";
    if ([deviceString isEqualToString:@"iPhone5,4"])    return @"iPhone5C";
    if ([deviceString isEqualToString:@"iPhone6,1"])    return @"iPhone5S";
    if ([deviceString isEqualToString:@"iPhone6,2"])    return @"iPhone5S";
    if ([deviceString isEqualToString:@"iPhone7,1"])    return @"iPhone6 Plus";
    if ([deviceString isEqualToString:@"iPhone7,2"])    return @"iPhone6";
    if ([deviceString isEqualToString:@"iPhone8,1"])    return @"iPhone6s";
    if ([deviceString isEqualToString:@"iPhone8,2"])    return @"iPhone6s Plus";
    if ([deviceString isEqualToString:@"iPhone8,3"])    return @"iPhoneSE";
    if ([deviceString isEqualToString:@"iPhone8,4"])    return @"iPhoneSE";
    if ([deviceString isEqualToString:@"iPhone9,1"])    return @"iPhone7";
    if ([deviceString isEqualToString:@"iPhone9,2"])    return @"iPhone7 Plus";
    if ([deviceString isEqualToString:@"iPhone10,1"])   return @"iPhone8";
    if ([deviceString isEqualToString:@"iPhone10,2"])   return @"iPhone8 Plus";
    if ([deviceString isEqualToString:@"iPhone10,3"])   return @"iPhoneX";
    if ([deviceString isEqualToString:@"iPhone10,4"])   return @"iPhone8";
    if ([deviceString isEqualToString:@"iPhone10,5"])   return @"iPhone8 Plus";
    if ([deviceString isEqualToString:@"iPhone10,6"])   return @"iPhoneX";
    //iPod
    if ([deviceString isEqualToString:@"iPod1,1"])      return @"iPod Touch";
    if ([deviceString isEqualToString:@"iPod2,1"])      return @"iPod Touch 2nd";
    if ([deviceString isEqualToString:@"iPod3,1"])      return @"iPod Touch 3rd";
    if ([deviceString isEqualToString:@"iPod4,1"])      return @"iPod Touch 4th";
    if ([deviceString isEqualToString:@"iPod5,1"])      return @"iPod Touch 5th";
    if ([deviceString isEqualToString:@"iPod7,1"])      return @"iPod Touch 6th";
    
    //iPad
    if ([deviceString isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([deviceString isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([deviceString isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([deviceString isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([deviceString isEqualToString:@"iPad2,4"])      return @"iPad 2 (32nm)";
    if ([deviceString isEqualToString:@"iPad2,5"])      return @"iPad mini (WiFi)";
    if ([deviceString isEqualToString:@"iPad2,6"])      return @"iPad mini (GSM)";
    if ([deviceString isEqualToString:@"iPad2,7"])      return @"iPad mini (CDMA)";
    
    if ([deviceString isEqualToString:@"iPad3,1"])      return @"iPad 3(WiFi)";
    if ([deviceString isEqualToString:@"iPad3,2"])      return @"iPad 3(CDMA)";
    if ([deviceString isEqualToString:@"iPad3,3"])      return @"iPad 3(4G)";
    if ([deviceString isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([deviceString isEqualToString:@"iPad3,5"])      return @"iPad 4 (4G)";
    if ([deviceString isEqualToString:@"iPad3,6"])      return @"iPad 4 (CDMA)";
    
    if ([deviceString isEqualToString:@"iPad4,1"])      return @"iPad Air";
    if ([deviceString isEqualToString:@"iPad4,2"])      return @"iPad Air";
    if ([deviceString isEqualToString:@"iPad4,3"])      return @"iPad Air";
    if ([deviceString isEqualToString:@"iPad5,3"])      return @"iPad Air 2";
    if ([deviceString isEqualToString:@"iPad5,4"])      return @"iPad Air 2";
    
    if ([deviceString isEqualToString:@"iPad6,3"])      return @"iPad Pro 9.7";
    if ([deviceString isEqualToString:@"iPad6,4"])      return @"iPad Pro 9.7";
    if ([deviceString isEqualToString:@"iPad6,7"])      return @"iPad Pro 12.9";
    if ([deviceString isEqualToString:@"iPad6,8"])      return @"iPad Pro 12.9";
    if ([deviceString isEqualToString:@"iPad6,11"])     return @"iPad 5";
    if ([deviceString isEqualToString:@"iPad6,12"])     return @"iPad 5";
    if ([deviceString isEqualToString:@"iPad7,1"])      return @"iPad Pro 12.9 2nd";
    if ([deviceString isEqualToString:@"iPad7,2"])      return @"iPad Pro 12.9 2nd";
    if ([deviceString isEqualToString:@"iPad7,3"])      return @"iPad Pro 10.5";
    if ([deviceString isEqualToString:@"iPad7,4"])      return @"iPad Pro 10.5";
    
    if ([deviceString isEqualToString:@"i386"])         return @"Simulator";
    if ([deviceString isEqualToString:@"x86_64"])       return @"Simulator";
    
    if ([deviceString isEqualToString:@"iPad4,4"]
        ||[deviceString isEqualToString:@"iPad4,5"]
        ||[deviceString isEqualToString:@"iPad4,6"])      return @"iPad mini 2";
    
    if ([deviceString isEqualToString:@"iPad4,7"]
        ||[deviceString isEqualToString:@"iPad4,8"]
        ||[deviceString isEqualToString:@"iPad4,9"])      return @"iPad mini 3";
    
    if ([deviceString isEqualToString:@"iPad5,1"]
        ||[deviceString isEqualToString:@"iPad5,2"])      return @"iPad mini 4";
    
    return deviceString;
}
#pragma mark - 字典转JSON
+ (NSString *)dictionaryToJson:(NSDictionary *)dict {
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&parseError];
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

#pragma mark - 错误码转换
+ (NSError *)fetchErrorResponse:(NSDictionary *)responseDict {
    NSError *error = [NSError errorWithDomain:@"LYCHTTPClientErrorDomain" code:[[responseDict objectForKey:@"code"] integerValue] userInfo:@{NSLocalizedDescriptionKey : [responseDict objectForKey:@"msg"]}];
    
    NSString *code = responseDict[@"code"];
    if ([code isEqualToString:@"1000"]){
        USER_DEFAULTS_SET(nil, kLYCLoginUserId);
     
    }else if([code isEqualToString:@"1001"] || [code isEqualToString:@"1003"]  || [code isEqualToString:@"1006"] || [code isEqualToString:@"1008"]) {
        USER_DEFAULTS_SET(nil, kLYCLoginUserId);
        
    } else if ([code isEqualToString:@"0006"] || [code isEqualToString:@"0007"] || [code isEqualToString:@"0008"] || [code isEqualToString:@"0005"]) {
       
    }
    
    return error;
}
#pragma mark - 解密、处理请求返回数据
+ (void)fechRespons:(NSMutableDictionary *)responseDict withHandle:(void(^)(id result, NSError *requestError))handle {

    NSDictionary *header = [responseDict objectForKey:@"Header"];
    id content = [responseDict objectForKey:@"Content"];
    NSInteger code = 1000;
    NSString *message = @"系统错误";
    if([header isKindOfClass:[NSDictionary class]]){
        
        code =  [header[@"Code"] integerValue];
        message =  header[@"Msg"];
        NSLog(@"%@,%@",message,content);
    }
    if(code == 1000){
        
        if(handle){
            
             handle(content, nil);
        }
    }else{
        
        NSError *error = [NSError errorWithDomain:@"HTTPClientError" code:code userInfo:@{NSLocalizedDescriptionKey : message}];
        
        handle(nil, error);
    }
    
}


@end
