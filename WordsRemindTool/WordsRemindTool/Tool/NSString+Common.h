//
//  NSString+Common.h
//  LYCampus-iPhone
//
//  Created by MAC-LY001 on 13-12-24.
//  Copyright (c) 2013年  All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Common)

//**** 可以显示的object-->string ****
+ (NSString*)stringFromObject:(id)object;
- (id)showString;
- (BOOL)isEmpty;

- (NSString *)weekDayTransform;
/*
 转化为UTF8字符
*/
- (id)toUTF8String;
- (NSString*)replace:(NSString*)aReplace
				  to:(NSString*)aTo;
- (NSString*)removingHTMLTags;
- (NSString*)MD5;
- (NSString*)filterUnKnowJson;
/**
 * url编码
 */
- (id)urlEncoded;

/**
 * 邮箱合法
 */
- (BOOL)validateEmail;

/**
 * 电话号码合法
 */
- (BOOL)validetePhoneNumber:(BOOL)isMobile;
/**
 *  传入正则表达式进行判断
 *
 */
- (BOOL)validateWithRegExp: (NSString *)regExp;
/**
 *  获取字符个数
 */
- (NSUInteger)unicodeLengthOfString;
/**
 *  判断字符串是否不超过limit个数
 */
- (BOOL)isValidateNameWithNoMoreThanLimit:(NSInteger)limit;

- (NSArray *)getUrlWithString;


+ (BOOL)isStringEmpty:(NSString *)str;
//json转字典、数组
+ (id)jsonToDictionaryArray:(NSString *)jsonString;


+(CGSize)sizeWithString:(NSString *)string font:(UIFont *)font;
@end



