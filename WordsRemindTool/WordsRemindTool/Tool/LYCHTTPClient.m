//
//  LYCHTTPClient.m
//  LYCampus-iPhone
//
//  Created by 4work on 23/08/2017.
//  Copyright © 2017 LYCampus. All rights reserved.
//

#import "LYCHTTPClient.h"
#import "LYCHTTPUntil.h"
#import "NSDictionary+Unitl.h"
#import "NSString+Common.m"

@implementation LYCHTTPClient

+ (instancetype)sharedInstance {
    static LYCHTTPClient *instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init]; 
    });
    
    return instance;
}


#pragma mark - 网络请求封装(POST、GET)，默认设置Content-Type
+ (void)requestWithServiceIdentifierAfterGetDeviceUUIDFromServer:(NSString *)serviceIdentifier
                                                   requestParams:(id)requestParams
                                                httpHeaderFields:(NSDictionary *)headerFields
                                                      methodName:(NSString *)methodName
                                                    withComplete:(void(^)(id result, NSError *requestError))handle {
    NSLog(@"\n{\npath:%@\nparams:%@\nheader:%@\nmethod:%@\n}", serviceIdentifier, [requestParams isKindOfClass:[NSData class]]==YES?@"图片数据":requestParams, headerFields, methodName);
 
    
    NSString *requestUrl = serviceIdentifier;
    
    
    AFHTTPSessionManager *manager = [LYCHTTPClient shareAFNManager];
    //https
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.securityPolicy setValidatesDomainName:NO];
    [headerFields enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        [manager.requestSerializer setValue:obj forHTTPHeaderField:key];
    }];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain", @"text/html", nil];
    manager.requestSerializer.timeoutInterval = 10.f;
    
    id params = requestParams;
    
    NSDictionary *iphoneDic = [LYCHTTPUntil deviceBaseInfo];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithDictionary:iphoneDic];
    if(params){
        [parameters addEntriesFromDictionary:params];
    }
    
    //参数加密
     NSString * sign =  [self sortedDictionary:parameters];

    [parameters setObject:sign forKey:@"sign"];
    
    if ([methodName isEqualToString:@"GET"]) {
        
       
        [manager GET:requestUrl parameters:parameters headers:nil progress:^(NSProgress * _Nonnull downloadProgress) {
            // 这里可以获取到目前的数据请求的进度
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"TIM_GET请求成功:%@", responseObject);
            [LYCHTTPUntil fechRespons:responseObject withHandle:handle];
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"TIM_GET请求失败%@", [error localizedDescription]);
            if (handle) {
                handle(nil, error);
            }
        }];
    }else{
        
        [manager POST:requestUrl parameters:parameters headers:nil progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"TIM_POST请求成功:%@", responseObject);
            [LYCHTTPUntil fechRespons:responseObject withHandle:handle];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"TIM_POST请求失败:%@", error.description);
            if (handle) {
                handle(nil, error);
            }
        }];
    }

}

+ (void)requestWithServiceIdentifier:(NSString *)serviceIdentifier
                       requestParams:(id)requestParams
                    httpHeaderFields:(NSDictionary *)headerFields
                          methodName:(NSString *)methodName
                        withComplete:(void(^)(id result, NSError *requestError))handle {
    requestParams = [(NSDictionary *)requestParams fetchDictionaryEmptyValue];
    
    [self requestWithServiceIdentifierAfterGetDeviceUUIDFromServer:serviceIdentifier requestParams:requestParams httpHeaderFields:headerFields methodName:methodName withComplete:handle];
}




+ (void)requestWithNoFetchData:(NSString *)serviceIdentifier
                       requestParams:(id)requestParams
                    httpHeaderFields:(NSDictionary *)headerFields
                          methodName:(NSString *)methodName
                        withComplete:(void(^)(id result, NSError *requestError))handle {
    
    requestParams = [(NSDictionary *)requestParams fetchDictionaryEmptyValue];
    
    AFHTTPSessionManager *manager = [LYCHTTPClient shareAFNManager];
    //https
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.securityPolicy.allowInvalidCertificates = YES;
    [manager.securityPolicy setValidatesDomainName:NO];
    [headerFields enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        [manager.requestSerializer setValue:obj forHTTPHeaderField:key];
    }];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain", @"text/html", nil];
    manager.requestSerializer.timeoutInterval = 10.f;
    
    id params = requestParams;
    
    NSDictionary *iphoneDic = [LYCHTTPUntil deviceBaseInfo];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithDictionary:iphoneDic];
    if(params){
        [parameters addEntriesFromDictionary:params];
    }
 
    
    [manager GET:serviceIdentifier  parameters:nil headers:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        // 这里可以获取到目前的数据请求的进度
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"TIM_GET请求成功:%@", responseObject);
        if (handle) {
            handle(responseObject, nil);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"TIM_GET请求失败%@", [error localizedDescription]);
        if (handle) {
            handle(nil, error);
        }
    }];
        
        
}



+ (AFHTTPSessionManager *)shareAFNManager
{
    static AFHTTPSessionManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [AFHTTPSessionManager manager];
    });
    return manager;
}

+ (NSString *)sortedDictionary:(NSDictionary *)dict{
    NSLog(@"%@",dict);
    
    NSString * basicStr = @"keyValue=c3e71e91f424e6f6279a445636a09921";
    
    //将所有的key放进数组
    NSArray *allKeyArray = [dict allKeys];
    
    //序列化器对数组进行排序的block 返回值为排序后的数组
    NSArray *afterSortKeyArray = [allKeyArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id
                                                                                            _Nonnull obj2) {
        
        //排序操作
        NSComparisonResult resuest = [obj1 compare:obj2];
        return resuest;
    }];
    
    //通过排列的key值获取value
    NSMutableArray *valueArray = [NSMutableArray array];
    for (NSString *sortsing in afterSortKeyArray) {
        NSString *valueString = [dict objectForKey:sortsing];
        [valueArray addObject:valueString];
    }
    NSLog(@"valueArray:%@",valueArray);
    NSMutableString * formatUrlMap = [[NSMutableString alloc]initWithString:basicStr];
    for (int i =0; i<valueArray.count; i++) {
        NSString * item = [NSString stringWithFormat:@"&%@=%@",afterSortKeyArray[i],valueArray[i]];
        [formatUrlMap appendString:item];
    }
    
    NSString * resultStr = [formatUrlMap.MD5 uppercaseString];
    return resultStr;
}



@end
