//
//  NSDictionary+Unitl.h
//  LYCampus-iPhone
//
//  Created by 4work on 25/08/2017.
//  Copyright © 2017 LYCampus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Unitl)

- (NSDictionary *)fetchDictionaryEmptyValue;

@end
