//
//  NSArray+Unitl.m
//  LYCampus-iPhone
//
//  Created by 4work on 25/08/2017.
//  Copyright © 2017 LYCampus. All rights reserved.
//

#import "NSArray+Unitl.h"
#import "NSDictionary+Unitl.h"

@implementation NSArray (Unitl)

- (NSArray *)fetchArrayEmptyValue {
    NSMutableArray *marr = [NSMutableArray arrayWithArray:self];
    NSMutableArray *set = [[NSMutableArray alloc] init];
    NSMutableDictionary *dicSet = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *arrSet = [[NSMutableDictionary alloc] init];
    
    for (id obj in marr)  {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            
            NSDictionary *changeDic = [obj fetchDictionaryEmptyValue];
            NSInteger index = [marr indexOfObject:obj];
            [dicSet setObject:changeDic forKey:@(index)];
        } else if ([obj isKindOfClass:[NSArray class]]) {
            NSArray *changeArr = [obj fetchArrayEmptyValue];
            NSInteger index = [marr indexOfObject:obj];
            [arrSet setObject:changeArr forKey:@(index)];
        } else {
            if ([obj isKindOfClass:[NSNull class]]) {
                NSInteger index = [marr indexOfObject:obj];
                [set addObject:@(index)];
            }
        }
    }
    
    for (id obj in set) {
        marr[(int)obj] = @"";
    }
    
    for (id obj in dicSet.allKeys) {
        int index = [obj intValue];
        marr[index] = dicSet[obj];
    }
    
    for (id obj in arrSet.allKeys) {
        int index = [obj intValue];
        marr[index] = arrSet[obj];
    }
    
    return marr;
}

@end
