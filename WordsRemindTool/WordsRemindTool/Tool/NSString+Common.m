//
//  NSString+Common.m
//  LYCampus-iPhone
//
//  Created by MAC-LY001 on 13-12-24.
//  Copyright (c) 2013年  All rights reserved.
//

#import "NSString+Common.h"
#import <CommonCrypto/CommonDigest.h>


@implementation NSString (Common)


//**** 可以显示的object-->string ****
+ (NSString*)stringFromObject:(id)object
{
	if ([object isKindOfClass:[NSString class]]) {
		if (![object isEmpty]) {
			return object;
		}
	}
	return @"";
}

//**** 可以打印的字符 ****
- (id)showString
{
	if ([self isEmpty]) {
		return @"";
	}
	return self;
}
//**** 判断字符串是否为空 ****
- (BOOL)isEmpty
{
	if ((NSNull *)self == [NSNull null]){
        return YES;
    }
    if (self == nil) {
        return YES;
    }
	else if ([self length] == 0) {
        return YES;
    }
	else {
        
        if ([[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
            return YES;
        }
    }
    return NO;
}

- (NSString *)weekDayTransform
{
    if (self.length <= 0) return @"";
    NSInteger index = [self integerValue];
    
    switch (index) {
        case 1:
            return @"周一";
        case 2:
            return @"周二";
        case 3:
            return @"周三";
        case 4:
            return @"周四";
        case 5:
            return @"周五";
        case 6:
            return @"周六";
        case 7:
            return @"周日";
            
        default:
            break;
    }
    return @"";
}

//**** UTF8编码 ****
- (id)toUTF8String
{
	return [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

//**** 替换字符 ****
- (NSString *)replace:(NSString*)aReplace
                   to:(NSString*)aTo
{
	NSMutableString *aMutableString = [NSMutableString stringWithString:self];
	[aMutableString replaceOccurrencesOfString:aReplace
									withString:aTo
									   options:NSLiteralSearch
										 range:NSMakeRange(0, [aMutableString length])];
	return aMutableString;
}



//**** MD5编码 ****
- (NSString*)MD5{
	if ([self isEmpty]) {
		return nil;
	}
    const char *value = [self UTF8String];
    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(value, (CC_LONG)strlen(value), outputBuffer);
    
    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++){
        [outputString appendFormat:@"%02x",outputBuffer[count]];
    }
	
    return outputString;
}

//**** 过来非法json字符 ****
-(NSString*)filterUnKnowJson{
	if (nil==self
		|| [self isEqualToString:@""]
		|| [self length]==0) {
		return self;
	}
	NSString *aString = [self stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
	NSCharacterSet *controlChars = [NSCharacterSet controlCharacterSet];
	NSRange range = [aString rangeOfCharacterFromSet:controlChars];
	if (range.location != NSNotFound) {
		NSMutableString *mutable = [NSMutableString stringWithString:aString];
		while (range.location != NSNotFound) {
			[mutable deleteCharactersInRange:range];
			range = [mutable rangeOfCharacterFromSet:controlChars];
		}
		return mutable;
	}
	return aString;
}
- (id)urlEncoded {
    CFStringRef cfUrlEncodedString = CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                             (CFStringRef)self,NULL,
                                                                             (CFStringRef)@"!#$%&'()*+,/:;=?@[]",
                                                                             kCFStringEncodingUTF8);
    
    NSString *urlEncoded = [NSString stringWithString:(__bridge NSString *)cfUrlEncodedString];
    CFRelease(cfUrlEncodedString);
    return urlEncoded;
}
- (BOOL)validateEmail{
	NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
	return [emailTest evaluateWithObject:self];
}
-(BOOL)validetePhoneNumber:(BOOL)isMobile{
    NSString *PhoneNumberRegex = @"";
    if (isMobile) {
        //**** 手机 ****
        PhoneNumberRegex = @"^1(3[0-9]|4[56789]|5[0-9]|6[6]|7[0-9]|8[0-9]|9[189])\\d{8}$";
    }else{
        //**** 固话 ****
        PhoneNumberRegex = @"^0\\d{2,3}(\\-)?\\d{7,8}$";
    }
	NSPredicate *PhoneNumberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", PhoneNumberRegex];
	return [PhoneNumberTest evaluateWithObject:self];
}


- (BOOL)validateWithRegExp: (NSString *)regExp

{
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat: @"SELF MATCHES %@", regExp];
    
    return [predicate evaluateWithObject: self];
    
}
- (NSUInteger)unicodeLengthOfString{
    NSUInteger asciiLength = 0;
    
    for (NSUInteger i = 0; i < self.length; i++) {
        
        
        unichar uc = [self characterAtIndex: i];
        
        asciiLength += isascii(uc) ? 1 : 2;
    }
    
    NSUInteger unicodeLength = asciiLength / 2;
    
    if(asciiLength % 2) {
        unicodeLength++;
    }
    
    return unicodeLength;
}
- (BOOL)isValidateNameWithNoMoreThanLimit:(NSInteger)limit{
    NSUInteger  character = 0;
    for(int i=0; i< [self length];i++){
        int a = [self characterAtIndex:i];
        if( a > 0x4e00 && a < 0x9fff){ //判断是否为中文
            character +=2;
        }else{
            character +=1;
        }
    }
    
    if (character <= limit) {
        return YES;
    }else{
        return NO;
    }
    
}

- (NSArray *)getUrlWithString
{
    NSError *error = NULL;
    NSString *regulaStr = @"(http[s]{0,1}://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regulaStr
                                                                           options:NSRegularExpressionCaseInsensitive error:&error];

    NSArray * matches = [regex matchesInString:self options:NSMatchingReportProgress range:NSMakeRange(0, [self length])];
    NSMutableArray *arr = [NSMutableArray arrayWithCapacity:0];
    for (NSTextCheckingResult * test in matches) {
        NSString *tagValuestr = [self substringWithRange:test.range];
        NSLog(@"tagvalue = %@",tagValuestr);
        [arr addObject:tagValuestr];
    }
    if (arr.count > 0) {
        return arr;
    }
    return @[];
}

#pragma mark - 字符串非空判断
+ (BOOL)isStringEmpty:(NSString *)str
{
    if (!str) {
        return YES;
    } else {
        //A character set containing only the whitespace characters space (U+0020) and tab (U+0009) and the newline and nextline characters (U+000A–U+000D, U+0085).
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        //Returns a new string made by removing from both ends of the receiver characters contained in a given character set.
        NSString *trimedString = [str stringByTrimmingCharactersInSet:set];
        
        if ([trimedString length] == 0) {
            return YES;
        } else if ([trimedString isEqualToString:[@"null" uppercaseString]]) {
            return NO;
        } else {
            return NO;
        }
    }
}

#pragma mark - json转字典、数组
+ (id)jsonToDictionaryArray:(NSString *)jsonString {
    if ([jsonString isEqual:[NSNull null]]) {
        return nil;
    }
    if (![jsonString isKindOfClass:[NSString class]]) {
        return jsonString;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    id dict = [NSJSONSerialization JSONObjectWithData:jsonData
                                              options:NSJSONReadingMutableContainers
                                                error:&error];
    
    if (error) {
        return nil;
    } else {
        return dict;
    }
}

#pragma mark - - - 计算字符串尺寸
+(CGSize)sizeWithString:(NSString *)string font:(UIFont *)font {
    NSDictionary *attrs = @{NSFontAttributeName : font};
    return [string boundingRectWithSize:CGSizeMake(0, 0) options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
}

@end







