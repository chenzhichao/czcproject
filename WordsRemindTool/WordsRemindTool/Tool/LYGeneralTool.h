//
//  LYGeneralTool.h
//  LYCampus-iPhone
//
//  Created by lsc on 2017/12/21.
//  Copyright © 2017年 LYCampus. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface LYGeneralTool : NSObject


+ (void)alertControllerTitle:(NSString *)title
                     Message:(NSString *)message
              preferredStyle:(UIAlertControllerStyle)style
                      action:(NSArray<NSString *> *)titleArray
              isCancelAction:(BOOL)isCancel
                 ActionBlock:(void(^)(int index, NSString *title, UIAlertAction *action))actionBlock
       presentViewController:(void(^)(UIAlertController *alert))controllerBlock;

+ (BOOL)clearCacheWithFilePath:(NSString *)path;

/**
 *  设置富文本 content:需要设置的string;lineSpacing行间距;keyText需要设置的富文本数组string;color需要设置的富文本数组颜色;font需要设置的富文本数组字体
 */
+ (NSMutableAttributedString *)attributedStringWithContent:(NSString *)content
                                               LineSpacing:(CGFloat)lineSpacing
                                                   keyText:(NSArray <NSString *>*)keyText
                                                     Color:(NSArray <UIColor *>*)color
                                                      Font:(NSArray <UIFont *>*)font;
/**
 *  获取网络状态，statusbar不能隐藏
 */
+ (NSString *)getNetWorkStates;


/**
 比较本地版本和最新版本号的大小
 @param lastVersionStr 最新版本,例:1.0.7或者2.0.2.11
 @return 如果本地版本<最新版本,返回1,否则返回0
 */
+ (BOOL)compareLastVersion:(NSString *)lastVersionStr;


//传入今天的时间，返回明天的时间（整点时间）
+ (NSString *)getTomorrowDay:(NSDate *)aDate;

//比较当前日期和指定日期的时间间隔秒数
+ (NSString *)compareNowDateToEndDateForSecound:(NSString *)endDate;

//日期格式转字符串
+ (NSString *)dateToString:(NSDate *)date withDateFormat:(NSString *)format;
//字符串转日期格式
+ (NSDate *)stringToDate:(NSString *)dateString withDateFormat:(NSString *)format;

//文字提示
+ (void)showTextHUD:(NSString *)text inView:(UIView *)view hideAfterDelay:(CGFloat)afterDelay;
//加载中提示
+ (void)showLoadingHUDInView:(UIView *)view;
+ (void)hiddleHUDInView:(UIView *)view;

//清理cookies
+ (void)deleteWebCache;

@end
