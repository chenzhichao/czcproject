//
//  CZCHomeHttpsTool.m
//  CZCWaterMarkTool
//
//  Created by czc on 2019/8/31.
//  Copyright © 2019年 czc. All rights reserved.
//

#import "CZCHttpsTool.h"
#import "LYCHTTPClient.h"

@implementation CZCHttpsTool


+ (void)getSystemConfigWithHandle:(void(^)(id result, NSError *error))handle{
    
    [LYCHTTPClient requestWithServiceIdentifier:[NSString stringWithFormat:@"%@%@", kCZCURL, kCZCGetConfig] requestParams:nil httpHeaderFields:nil methodName:@"POST" withComplete:^(id result, NSError *requestError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (handle) {
                handle(result, requestError);
            }
        });
    } ];
}



+ (void)LoginWithInfo:(NSDictionary *)param withHandle:(void(^)(id result, NSError *error))handle{
  
    [LYCHTTPClient requestWithServiceIdentifier:[NSString stringWithFormat:@"%@%@", kCZCURL, kCZCUserLogin] requestParams:param httpHeaderFields:nil methodName:@"POST" withComplete:^(id result, NSError *requestError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (handle) {
                handle(result, requestError);
            }
        });
    } ];
}

+ (void)getUserInfo:(NSString *)token withHandle:(void(^)(id result, NSError *error))handle{
    
    
    [LYCHTTPClient requestWithServiceIdentifier:[NSString stringWithFormat:@"%@%@", kCZCURL, kCZCUserGetUserInfo] requestParams:nil httpHeaderFields:@{@"authorization":token} methodName:@"POST" withComplete:^(id result, NSError *requestError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (handle) {
                handle(result, requestError);
            }
        });
    } ];
}


+ (void)getVipInfo:(NSString *)token withHandle:(void(^)(id result, NSError *error))handle{
    
    
    [LYCHTTPClient requestWithServiceIdentifier:[NSString stringWithFormat:@"%@%@", kCZCURL, kCZCUserGetVipInfo] requestParams:nil httpHeaderFields:@{@"authorization":token} methodName:@"POST" withComplete:^(id result, NSError *requestError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (handle) {
                handle(result, requestError);
            }
        });
    } ];
}

+ (void)getVipPriceHandle:(void(^)(id result, NSError *error))handle{
    
    
    [LYCHTTPClient requestWithServiceIdentifier:[NSString stringWithFormat:@"%@%@", kCZCURL, kCZCGetVipPrice] requestParams:nil httpHeaderFields:nil methodName:@"POST" withComplete:^(id result, NSError *requestError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (handle) {
                handle(result, requestError);
            }
        });
    } ];
}

+ (void)getProbationType:(NSUInteger )type WithHandle:(void(^)(id result, NSError *error))handle{
    
    [LYCHTTPClient requestWithServiceIdentifier:[NSString stringWithFormat:@"%@%@", kCZCURL, kCZCPutProbation] requestParams:@{@"id" : @(type)} httpHeaderFields:nil methodName:@"POST" withComplete:^(id result, NSError *requestError) {
           
           dispatch_async(dispatch_get_main_queue(), ^{
               if (handle) {
                   handle(result, requestError);
               }
           });
       } ];
}
+ (void)getDialogueList:(NSUInteger )page withHandle:(void(^)(id result, NSError *error))handle{
    
    [LYCHTTPClient requestWithServiceIdentifier:[NSString stringWithFormat:@"%@%@", kCZCURL, kCZCDialogueList] requestParams:@{@"page":@(page)} httpHeaderFields:@{@"authorization":[LYCUserModel fetchUser].token} methodName:@"POST" withComplete:^(id result, NSError *requestError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (handle) {
                handle(result, requestError);
            }
        });
    } ];
}
+ (void)getAddDialogue:(NSString *)title content:(NSString *)content withHandle:(void(^)(id result, NSError *error))handle{
    
    
    [LYCHTTPClient requestWithServiceIdentifier:[NSString stringWithFormat:@"%@%@", kCZCURL, kCZCAddDialogue] requestParams:@{@"title":title,@"content":content} httpHeaderFields:@{@"authorization":[LYCUserModel fetchUser].token} methodName:@"POST" withComplete:^(id result, NSError *requestError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (handle) {
                handle(result, requestError);
            }
        });
    } ];
    
}

+ (void)getEditDialogue:(NSString *)title content:(NSString *)content diaId:(NSInteger)diaId withHandle:(void(^)(id result, NSError *error))handle{
    
    [LYCHTTPClient requestWithServiceIdentifier:[NSString stringWithFormat:@"%@%@", kCZCURL, kCZCEditDialogue] requestParams:@{@"title":title,@"content":content,@"id":@(diaId)} httpHeaderFields:@{@"authorization":[LYCUserModel fetchUser].token} methodName:@"POST" withComplete:^(id result, NSError *requestError) {
          
          dispatch_async(dispatch_get_main_queue(), ^{
              if (handle) {
                  handle(result, requestError);
              }
          });
      } ];
}
+ (void)getDeleteDialogueId:(NSInteger)diaId withHandle:(void(^)(id result, NSError *error))handle{
    
    [LYCHTTPClient requestWithServiceIdentifier:[NSString stringWithFormat:@"%@%@", kCZCURL, kCZCDeleteDialogue] requestParams:@{@"id":@(diaId)} httpHeaderFields:@{@"authorization":[LYCUserModel fetchUser].token} methodName:@"POST" withComplete:^(id result, NSError *requestError) {
          
          dispatch_async(dispatch_get_main_queue(), ^{
              if (handle) {
                  handle(result, requestError);
              }
          });
      } ];
}




+ (void)getSearchDialogue:(NSString *)keyWord withHandle:(void(^)(id result, NSError *error))handle{
    
    
       [LYCHTTPClient requestWithServiceIdentifier:[NSString stringWithFormat:@"%@%@", kCZCURL, kCZCSearchDialogue] requestParams:@{@"key_word":keyWord,@"page":@"1"} httpHeaderFields:@{@"authorization":[LYCUserModel fetchUser].token} methodName:@"POST" withComplete:^(id result, NSError *requestError) {
           
           dispatch_async(dispatch_get_main_queue(), ^{
               if (handle) {
                   handle(result, requestError);
               }
           });
       }];
}







+ (void)weixinGetAccessToken:(NSString *)code withHandle:(void(^)(id result, NSError *error))handle{
    
    NSString * urlStr = [NSString stringWithFormat:@"%@?appid=%@&secret=%@&code=%@&grant_type=authorization_code",kCZCWeixinAccessToken,WEIXIN_APP_ID,WEIXIN_SECRET,code];
    
    [LYCHTTPClient requestWithNoFetchData:urlStr requestParams:nil httpHeaderFields:nil methodName:@"GET" withComplete:^(id result, NSError *requestError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (handle) {
                handle(result, requestError);
            }
        });
        
    }];
}

+ (void)weixinGetUserInfo:(NSString *)token openid:(NSString *)openid withHandle:(void(^)(id result, NSError *error))handle{
    
    NSString * urlStr = [NSString stringWithFormat:@"%@?openid=%@&access_token=%@",kCZCWeixinUserInfo,openid,token];
    
    [LYCHTTPClient requestWithNoFetchData:urlStr requestParams:nil httpHeaderFields:nil methodName:@"GET" withComplete:^(id result, NSError *requestError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (handle) {
                handle(result, requestError);
            }
        });
        
    }];
}


+ (void)buyVipWithOrder:(NSDictionary *)param withHandle:(void(^)(id result, NSError *error))handle{
    
    NSString *token=  [LYCUserModel fetchUser].token;
    if(!USER_DEFAULTS_GET(kLYCLoginUserId)){
        token = @"999";
    }
    
    [LYCHTTPClient requestWithServiceIdentifier:[NSString stringWithFormat:@"%@%@", kCZCURL, kCZCUserBuyVip] requestParams:param httpHeaderFields:@{@"authorization":token} methodName:@"POST" withComplete:^(id result, NSError *requestError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (handle) {
                handle(result, requestError);
            }
        });
    } ];
}




+ (void)PayCallBack:(NSDictionary *)param withHandle:(void(^)(id result, NSError *error))handle{
    
    NSString *token=  [LYCUserModel fetchUser].token;
    if(!USER_DEFAULTS_GET(kLYCLoginUserId)){
        token = @"999";
    }
    
    [LYCHTTPClient requestWithServiceIdentifier:[NSString stringWithFormat:@"%@%@", kCZCURL, kCZCPayCallBack] requestParams:param httpHeaderFields:@{@"authorization":token} methodName:@"POST" withComplete:^(id result, NSError *requestError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (handle) {
                handle(result, requestError);
            }
        });
    } ];
}


+ (void)PayCallbackSync:(NSDictionary *)param withHandle:(void(^)(id result, NSError *error))handle{
    
    NSString *token=  [LYCUserModel fetchUser].token;
    if(!USER_DEFAULTS_GET(kLYCLoginUserId)){
        token = @"999";
    }
    
    [LYCHTTPClient requestWithServiceIdentifier:[NSString stringWithFormat:@"%@%@", kCZCURL, kCZCPaySync] requestParams:param httpHeaderFields:@{@"authorization":token} methodName:@"POST" withComplete:^(id result, NSError *requestError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (handle) {
                handle(result, requestError);
            }
        });
    } ];
}


@end
