//
//  LYGeneralTool.m
//  LYCampus-iPhone
//
//  Created by lsc on 2017/12/21.
//  Copyright © 2017年 LYCampus. All rights reserved.
//

#import "LYGeneralTool.h"

@implementation LYGeneralTool


+ (void)alertControllerTitle:(NSString *)title
                     Message:(NSString *)message
              preferredStyle:(UIAlertControllerStyle)style
                      action:(NSArray<NSString *> *)titleArray
              isCancelAction:(BOOL)isCancel
                 ActionBlock:(void(^)(int index, NSString *title, UIAlertAction *action))actionBlock
       presentViewController:(void(^)(UIAlertController *alert))controllerBlock
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:style];
    NSMutableArray *array = [NSMutableArray arrayWithArray:titleArray];
    if (array && array.count > 0) {
        if (isCancel)
            [array addObject:@"取消"];
    } else {
        [array addObject:@"取消"];
    }
    for (int i = 0; i < array.count; i++) {
        UIAlertActionStyle style;
        if (isCancel && i == array.count - 1) {
            style = UIAlertActionStyleCancel;
        } else {
            style = UIAlertActionStyleDefault;
        }
        UIAlertAction *action = [UIAlertAction actionWithTitle:array[i]
                                                         style:style
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                           if (actionBlock) {
                                                               actionBlock(i, array[i], action);
                                                           }
                                                       }];
        [alert addAction:action];
    }
    if (controllerBlock) {
        controllerBlock(alert);
    }
}


+ (BOOL)clearCacheWithFilePath:(NSString *)path
{
    BOOL filePath = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (filePath) {
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
        if (error) {
            return NO;
        }
        return YES;
    }
    return YES;
}

+ (NSMutableAttributedString *)attributedStringWithContent:(NSString *)content
                                               LineSpacing:(CGFloat)lineSpacing
                                                   keyText:(NSArray <NSString *>*)keyText
                                                     Color:(NSArray <UIColor *>*)color
                                                      Font:(NSArray <UIFont *>*)font
{
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:content];
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineSpacing = lineSpacing;
    paraStyle.alignment = NSTextAlignmentCenter;
    [attStr addAttribute:NSParagraphStyleAttributeName value:paraStyle range:NSMakeRange(0, [content length])];
    if (keyText.count == color.count && keyText.count == font.count) {
        for (NSInteger i = 0; i < keyText.count; i++) {
            NSRange range = [content rangeOfString:keyText[i]];
            if (range.length > 0) {
                [attStr addAttribute:NSForegroundColorAttributeName value:color[i] range:NSMakeRange(range.location,[keyText[i] length])];
                [attStr addAttribute:NSFontAttributeName value:font[i] range:NSMakeRange(range.location, [keyText[i] length])];
            }
        }
    }
    return attStr;
}

+ (NSString *)getNetWorkStates
{
    NSArray *children;
    UIApplication *app = [UIApplication sharedApplication];
    NSString *state;
    //iPhone X
    if ([[app valueForKeyPath:@"_statusBar"] isKindOfClass:NSClassFromString(@"UIStatusBar_Modern")]) {
        children = [[[[app valueForKeyPath:@"_statusBar"] valueForKeyPath:@"_statusBar"] valueForKeyPath:@"foregroundView"] subviews];
        for (UIView *view in children) {
            for (id child in view.subviews) {
                //wifi
                if ([child isKindOfClass:NSClassFromString(@"_UIStatusBarWifiSignalView")]) {
                    state = @"wifi";
                }
                //2G 3G 4G
                if ([child isKindOfClass:NSClassFromString(@"_UIStatusBarStringView")]) {
                    if ([[child valueForKey:@"_originalText"] containsString:@"G"]) {
                        state = [child valueForKey:@"_originalText"];
                    }
                }
            }
        }
        if (!state) {
            state = @"无网络";
        }
    }else {
        children = [[[app valueForKeyPath:@"_statusBar"] valueForKeyPath:@"foregroundView"] subviews];
        for (id child in children) {
            if ([child isKindOfClass:NSClassFromString(@"UIStatusBarDataNetworkItemView")]) {
                //获取到状态栏
                switch ([[child valueForKeyPath:@"dataNetworkType"] intValue]) {
                    case 0:
                        state = @"无网络";
                        //无网模式
                        break;
                    case 1:
                        state = @"2G";
                        break;
                    case 2:
                        state = @"3G";
                        break;
                    case 3:
                        state = @"4G";
                        break;
                    case 5:
                        state = @"wifi";
                        break;
                    default:
                        break;
                }
            }
        }
    }
    return state;
}


+ (BOOL)compareLastVersion:(NSString *)lastVersionStr{
    //从info.plist文件获取Version
    NSString *version = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"];
    //本地版本号
    NSInteger localVersion = [[version stringByReplacingOccurrencesOfString:@"." withString:@""] integerValue];
    //最新版本号
    NSInteger lastVersion = [[lastVersionStr stringByReplacingOccurrencesOfString:@"." withString:@""] integerValue];
    //处理版本号缩减为两位
    lastVersion = lastVersion<100?lastVersion*=10:lastVersion;
    
    return localVersion<lastVersion?1:0;
    
}



+ (NSString *)getTomorrowDay:(NSDate *)aDate {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorian components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:aDate];
    [components setDay:([components day]+1)];
    NSDate *beginningOfWeek = [gregorian dateFromComponents:components];
    NSDateFormatter *dateday = [[NSDateFormatter alloc] init];
    [dateday setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateday stringFromDate:beginningOfWeek];
}


+(NSString *)compareNowDateToEndDateForSecound:(NSString *)endDate{
    NSString * nowDateStr =[LYGeneralTool dateToString:[NSDate date] withDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *startDate =[LYGeneralTool stringToDate:nowDateStr withDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *toDate =[LYGeneralTool stringToDate:endDate withDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //利用NSCalendar比较日期的差异
    NSCalendar *calendar = [NSCalendar currentCalendar];
    //比较的结果是NSDateComponents类对象
    NSDateComponents *delta = [calendar components:NSCalendarUnitSecond fromDate:startDate toDate:toDate options:0];
    return [NSString stringWithFormat:@"%ld",delta.second] ;
}

#pragma mark - 日期格式转字符串
+ (NSString *)dateToString:(NSDate *)date withDateFormat:(NSString *)format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    dateFormatter.locale = locale;
    
    NSString *strDate = [dateFormatter stringFromDate:date];
    
    return strDate;
}
#pragma mark - 字符串转日期格式
+ (NSDate *)stringToDate:(NSString *)dateString withDateFormat:(NSString *)format {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    formatter.locale = locale;
    
    return [formatter dateFromString:dateString];
}


+ (void)showTextHUD:(NSString *)text inView:(UIView *)view hideAfterDelay:(CGFloat)afterDelay{
    
    if (view == nil){
        view = [[UIApplication sharedApplication].windows lastObject];
    }
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.text = text;
    hud.bezelView.color =UIColor.grayColor;
    hud.bezelView.layer.cornerRadius = 6;
    [hud hideAnimated:true afterDelay:afterDelay];

}
//加载中提示
+ (void)showLoadingHUDInView:(UIView *)view{
    if (view == nil){
        view = [[UIApplication sharedApplication].windows lastObject];
    }
    
    [MBProgressHUD showHUDAddedTo:view animated:true];
}

+ (void)hiddleHUDInView:(UIView *)view{
    if (view == nil){
        view = [[UIApplication sharedApplication].windows lastObject];
    }
    
    [MBProgressHUD hideHUDForView:view animated:true];
}

#pragma mark - 清理cookies
+ (void)deleteWebCache {
    
    NSString *libraryDic = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) firstObject];
    NSString *bundleId = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
    
    NSString *web = [NSString stringWithFormat:@"%@/Caches/%@/WebKit", libraryDic, bundleId];
    NSString *webCaches = [NSString stringWithFormat:@"%@/Caches/%@/fsCachedData",libraryDic,bundleId];
    NSError *error;
    [[NSFileManager defaultManager] removeItemAtPath:web error:&error];
    [[NSFileManager defaultManager] removeItemAtPath:webCaches error:&error];
    
    if (error) {
        NSLog(@"Cookies清理失败:%@", error);
    } else {
        NSLog(@"Cookies清理成功");
    }
}


@end
