//
//  LYCHTTPClient.h
//  LYCampus-iPhone
//
//  Created by 4work on 23/08/2017.
//  Copyright © 2017 LYCampus. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LYCHTTPClient : NSObject





+ (instancetype)sharedInstance;



/**
 *  @brief 网络请求封装(POST、GET)。默认设置Content-Type、Unique-Code、Access-Token。serviceIdentifier为拼接好的完整URL。requestParams为默认为请求明文字典参数，方法内自动加密。result为返回原始数据(已解密)，格式如下：
{
 code = 0;
 data = "chQvYfr4yTb2i5idyOpjw+ItTB/wCo4F2nUrdLhxILg0lV9sCxjZ8/+3WeSH+BjM"; //对象、字符串
 msg = ok;
}
 */
+ (void)requestWithServiceIdentifier:(NSString *)serviceIdentifier
                       requestParams:(id)requestParams
                    httpHeaderFields:(NSDictionary *)headerFields
                          methodName:(NSString *)methodName
                        withComplete:(void(^)(id result, NSError *requestError))handle;


//
+ (void)requestWithNoFetchData:(NSString *)serviceIdentifier
             requestParams:(id)requestParams
          httpHeaderFields:(NSDictionary *)headerFields
                methodName:(NSString *)methodName
              withComplete:(void(^)(id result, NSError *requestError))handle;





@end
