//
//  LYCHTTPUntil.h
//  LYCampus-iPhone
//
//  Created by 4work on 28/08/2017.
//  Copyright © 2017 LYCampus. All rights reserved.
//

#import <Foundation/Foundation.h>

#define LYCAPIResponseTypeDefault @"0"

@interface LYCHTTPUntil : NSObject

//设备
/*
 deviceType：设备类型（1：android,2:apple）deviceModel:设备型号（eg: iphose 6s）deviceVersion:设备版本（eg: 10.1）clientVersion：客户端版本（eg: 3.0）
 */
+ (NSDictionary *)deviceBaseInfo;
//设备平台
+ (NSString *)devicePlatform;
//字典转JSON
+ (NSString *)dictionaryToJson:(NSDictionary *)dict;
//错误码转换
+ (NSError *)fetchErrorResponse:(NSDictionary *)responseDict;
//解密、处理请求返回数据
+ (void)fechRespons:(NSMutableDictionary *)responseDict withHandle:(void(^)(id result, NSError *requestError))handle; 

@end
