
#import "CZCGlobalTool.h"

@interface CZCGlobalTool ()




@end


@implementation CZCGlobalTool


+ (CZCGlobalTool *)sharedGlobalTool {
    static CZCGlobalTool *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [CZCGlobalTool new];
        [sharedInstance commonInit];
    });
    return sharedInstance;
}

- (void)commonInit{
    
    
}


@end















