//
//  CZCHomeHttpsTool.h
//  CZCWaterMarkTool
//
//  Created by czc on 2019/8/31.
//  Copyright © 2019年 czc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CZCHttpsTool : NSObject

//获取系统配置
+ (void)getSystemConfigWithHandle:(void(^)(id result, NSError *error))handle;

//登录
+ (void)LoginWithInfo:(NSDictionary *)param withHandle:(void(^)(id result, NSError *error))handle;

//用户信息
+ (void)getUserInfo:(NSString *)token withHandle:(void(^)(id result, NSError *error))handle;


+ (void)getVipInfo:(NSString *)token withHandle:(void(^)(id result, NSError *error))handle;

//获取vip价格
+ (void)getVipPriceHandle:(void(^)(id result, NSError *error))handle;


//试用
+ (void)getProbationType:(NSUInteger )type WithHandle:(void(^)(id result, NSError *error))handle;
+ (void)getDialogueList:(NSUInteger )page withHandle:(void(^)(id result, NSError *error))handle;
+ (void)getAddDialogue:(NSString *)title content:(NSString *)content withHandle:(void(^)(id result, NSError *error))handle;

+ (void)getEditDialogue:(NSString *)title content:(NSString *)content diaId:(NSInteger)diaId withHandle:(void(^)(id result, NSError *error))handle;
+ (void)getDeleteDialogueId:(NSInteger)diaId withHandle:(void(^)(id result, NSError *error))handle;

+ (void)getSearchDialogue:(NSString *)keyWord withHandle:(void(^)(id result, NSError *error))handle;

//微信登录
+ (void)weixinGetAccessToken:(NSString *)code withHandle:(void(^)(id result, NSError *error))handle;
+ (void)weixinGetUserInfo:(NSString *)token openid:(NSString *)openid withHandle:(void(^)(id result, NSError *error))handle;

//支付
+ (void)buyVipWithOrder:(NSDictionary *)param withHandle:(void(^)(id result, NSError *error))handle;


//IOS支付成功请求接口
+ (void)PayCallBack:(NSDictionary *)param withHandle:(void(^)(id result, NSError *error))handle;

+ (void)PayCallbackSync:(NSDictionary *)param withHandle:(void(^)(id result, NSError *error))handle;



@end

NS_ASSUME_NONNULL_END
