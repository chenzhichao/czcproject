//
//  LYCUserModel.h
//  LYCampus-iPhone
//
//  Created by 4work on 23/08/2017.
//  Copyright © 2017 LYCampus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LYCBaseModel.h"

@interface LYCUserModel : LYCBaseModel
#pragma mark - 公共
/**
 *	@brief	用户Id
 */
@property (nonatomic, copy) NSString *userId;


@property (nonatomic, assign) NSInteger loginType;
/**
 *	@brief	昵称
 */
@property (nonatomic, copy) NSString *nickname;
/**
 *	@brief	用户姓名
 */
@property (nonatomic, copy) NSString *userName;
/**
 *    @brief    头像
 */
@property (nonatomic, copy) NSString *avatar;


@property (nonatomic, copy) NSString *logintime;

@property (nonatomic, copy) NSString *createtime;
/**
 *	@brief	账号状态
 */
@property (nonatomic, assign) NSInteger status;
/**
 *	@brief	手机号码
 */
@property (nonatomic, copy) NSString *phone;

/**
 *    @brief    token
 */
@property (nonatomic, copy) NSString *token;

/**
 *    @brief    第三方登录
 */
@property (nonatomic, copy) NSString *thirdToken;

/**
 *    @brief    微信刷新
 */
@property (nonatomic, copy) NSString *refreshToken;

/**
 *    @brief    微信openid
 */
@property (nonatomic, copy) NSString *openid;

//是否vip 0：否； 1：是；
@property (nonatomic, assign) NSInteger isVip;
@property (nonatomic, assign) NSInteger isYearVip;
@property (nonatomic, copy) NSString *vipTime;

//获取当前登录用户
+ (LYCUserModel *)fetchUser;
//存储用户信息
- (void)recordUserData:(void (^)(BOOL isSuccessful))block;

@end
