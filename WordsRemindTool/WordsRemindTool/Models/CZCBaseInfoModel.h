//
//  CZCBaseInfoModel.h
//  CZCWaterMarkTool
//
//  Created by czc on 2019/9/1.
//  Copyright © 2019年 czc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CZCBaseInfoModel : LYCBaseModel

@property (nonatomic, copy) NSString *key;

@property (nonatomic, copy) NSString *val;

@property (nonatomic, copy) NSString *remark;


//缓存配置信息
+ (void)recordAPPBaseInfo:(NSArray <CZCBaseInfoModel *> *)infos;
//读取缓存
+ (CZCBaseInfoModel *)readAPPBaseInfo:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
