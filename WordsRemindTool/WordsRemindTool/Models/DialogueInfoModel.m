//
//  DialogueInfoModel.m
//  WordsRemindTool
//
//  Created by czc on 2020/8/31.
//  Copyright © 2020 czc. All rights reserved.
//

#import "DialogueInfoModel.h"

@implementation DialogueInfoModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    
    return @{@"identify":@"id"};
}

@end
