//
//  RemindInfoModel.m
//  WordsRemindTool
//
//  Created by czc on 2020/8/18.
//  Copyright © 2020 czc. All rights reserved.
//

#import "RemindInfoModel.h"

@implementation RemindInfoModel



+ (RemindInfoModel *)sharedRemindInfo {
    static RemindInfoModel *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [RemindInfoModel fetchRemindInfo];
    });
    return sharedInstance;
}


#pragma mark - 存储用户信息
- (void)recordRemindInfoModel:(void (^)(BOOL isSuccessful))block {
    if (self == nil) {
        if (block) {
            block(NO);
        }
        
        return;
    }
    
    LKDBHelper *helper = [LYCBaseModel getLYCDBHelper];
    id model = [helper searchSingle:[self class] where:nil orderBy:nil];
    
    RemindInfoModel *userModel = (RemindInfoModel *)model;
 
    
    if (userModel) {
        [helper updateToDB:self where:nil callback:^(BOOL result) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (block) {
                    block(result);
                }
                
                if (result) {
                    NSLog(@"update");
                }
            });
        }];
    } else {
        [helper insertToDB:self callback:^(BOOL result) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (block) {
                    block(result);
                }
            });
        }];
        NSLog(@"insert");
    }
}


#pragma mark - 获取当前登录用户
+ (RemindInfoModel *)fetchRemindInfo {
    
    LKDBHelper *helper = [LYCBaseModel getLYCDBHelper];
    id model = [helper searchSingle:[self class] where:nil orderBy:nil];
    
    RemindInfoModel *remindModel = (RemindInfoModel *)model;
    
    if (remindModel) {
        return remindModel;
    } else {
        
        return [RemindInfoModel getDefautRemindInfo];
    }
}

+(RemindInfoModel *)getDefautRemindInfo {
    
    RemindInfoModel *model = [[RemindInfoModel alloc]init];
    
    model.fontSise = 30;
    model.textSpeed = 5;
    model.textColor = UIColor.whiteColor;
    model.textBgColor = UIColor.blackColor;
    
    model.showCountDownView = true;
    model.countDownNum = 3;
    model.showMirrorMode = false;
    model.showHighSelectMode = false;
    model.isVerticalMode = true;
    
    model.textArea = 1;
    model.textTrans = 10;
    model.cameraMY = 5;
    model.cameraMB = 5;
    
    return model;
}


@end
