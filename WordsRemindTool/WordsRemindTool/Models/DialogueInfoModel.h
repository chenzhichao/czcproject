//
//  DialogueInfoModel.h
//  WordsRemindTool
//
//  Created by czc on 2020/8/31.
//  Copyright © 2020 czc. All rights reserved.
//

#import "LYCBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface DialogueInfoModel : LYCBaseModel

@property (nonatomic,assign) NSInteger identify;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *edittime;
@end

NS_ASSUME_NONNULL_END
