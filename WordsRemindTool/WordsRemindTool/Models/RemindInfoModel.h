//
//  RemindInfoModel.h
//  WordsRemindTool
//
//  Created by czc on 2020/8/18.
//  Copyright © 2020 czc. All rights reserved.
//


#define RM [RemindInfoModel sharedRemindInfo]

#import "LYCBaseModel.h"



@interface RemindInfoModel : LYCBaseModel


@property (nonatomic,assign) NSUInteger fontSise;

@property (nonatomic,assign) NSUInteger textSpeed;

@property (nonatomic,strong) UIColor * textColor;

@property (nonatomic,strong) UIColor * textBgColor;

//显示倒计时
@property (nonatomic,assign) BOOL showCountDownView;

@property (nonatomic,assign) NSUInteger countDownNum;

//镜像
@property (nonatomic,assign) BOOL showMirrorMode;

//高亮
@property (nonatomic,assign) BOOL showHighSelectMode;

//竖屏模式
@property (nonatomic,assign) BOOL isVerticalMode;

//相机
@property (nonatomic,assign) NSUInteger textArea;
//相机台词透明度
@property (nonatomic,assign) NSUInteger textTrans;

@property (nonatomic,assign) NSUInteger cameraMY;
//相机台词透明度
@property (nonatomic,assign) NSUInteger cameraMB;



+ (RemindInfoModel *)sharedRemindInfo;

- (void)recordRemindInfoModel:(void (^)(BOOL isSuccessful))block;


@end


