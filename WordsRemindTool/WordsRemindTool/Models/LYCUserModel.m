//
//  LYCUserModel.m
//  LYCampus-iPhone
//
//  Created by 4work on 23/08/2017.
//  Copyright © 2017 LYCampus. All rights reserved.
//

#import "LYCUserModel.h"

@implementation LYCUserModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    
    return @{@"useId":@"uid"};
}

#pragma mark - 主键
+ (NSString *)getPrimaryKey {
    return @"userId";
}
#pragma mark - 存储用户信息
- (void)recordUserData:(void (^)(BOOL isSuccessful))block {
    if (self == nil) {
        if (block) {
            block(NO);
        }
        
        return;
    }
    
    LKDBHelper *helper = [LYCBaseModel getLYCDBHelper];
    id model = [helper searchSingle:[self class] where:@{@"userId": self.userId} orderBy:nil];
    
    LYCUserModel *userModel = (LYCUserModel *)model;
 
    
    if (userModel) {
        [helper updateToDB:self where:@{@"userId": self.userId} callback:^(BOOL result) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (block) {
                    block(result);
                }
                
                if (result) {
                    NSLog(@"update");
                }
            });
        }];
    } else {
        [helper insertToDB:self callback:^(BOOL result) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (block) {
                    block(result);
                }
            });
        }];
        NSLog(@"insert");
    }
}



#pragma mark - 删除用户信息
- (void)deleteUserData:(void (^)(BOOL isSuccessful))block {
    if (self == nil) {
        if (block) {
            block(NO);
        }
        
        return;
    }
    
    LKDBHelper *helper = [LYCBaseModel getLYCDBHelper];
    id model = [helper searchSingle:[self class] where:@{@"userId": self.userId} orderBy:nil];
    
    LYCUserModel *userModel = (LYCUserModel *)model;
    
    
    if (userModel) {
       
        [helper deleteToDB:userModel];
    }
}


#pragma mark - 获取当前登录用户
+ (LYCUserModel *)fetchUser {
    if (USER_DEFAULTS_GET(kLYCLoginUserId)) {
        LKDBHelper *helper = [LYCBaseModel getLYCDBHelper];
        id model = [helper searchSingle:[self class] where:@{@"userId": USER_DEFAULTS_GET(kLYCLoginUserId)} orderBy:nil];
        
        LYCUserModel *userModel = (LYCUserModel *)model;
        
        if (userModel) {
            return userModel;
        } else {
            //用户未登录，提示重新登录
            return [LYCUserModel new];
        }
    } else {
        return [LYCUserModel new];
    }
}

@end
