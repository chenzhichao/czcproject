//
//  CZCBaseInfoModel.m
//  CZCWaterMarkTool
//
//  Created by czc on 2019/9/1.
//  Copyright © 2019年 czc. All rights reserved.
//

#import "CZCBaseInfoModel.h"

@implementation CZCBaseInfoModel

+ (NSString *)getPrimaryKey {
    return @"key";
}

+(void)recordAPPBaseInfo:(NSArray <CZCBaseInfoModel *> *)infos {
    
    
    LKDBHelper *helper = [LYCBaseModel getLYCDBHelper];
    id model = [helper searchSingle:[self class] where:nil orderBy:nil];
    
    if (model) {
        [infos enumerateObjectsUsingBlock:^(CZCBaseInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            [helper deleteToDB:obj];
            [helper updateToDB:obj where:@{@"key":obj.key} callback:^(BOOL result) {
                if (result) {
                    NSLog(@"更新字段%@", obj.key);
                }
            }];
        }];
        
    } else {
        
        [infos enumerateObjectsUsingBlock:^(CZCBaseInfoModel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
          
            [helper insertToDB:obj callback:^(BOOL result) {
                if (result) {
                    NSLog(@"配置信息插入");
                }
            }];
        }];
        
        
       
    }
}

+ (CZCBaseInfoModel *)readAPPBaseInfo:(NSString *)key{
    LKDBHelper *helper = [LYCBaseModel getLYCDBHelper];
    CZCBaseInfoModel *model = [helper searchSingle:[CZCBaseInfoModel class] where:@{@"key":key} orderBy:nil];
    
    if (model) {
        return model;
    } else {
        return [[CZCBaseInfoModel alloc]init];
    }
}

@end
