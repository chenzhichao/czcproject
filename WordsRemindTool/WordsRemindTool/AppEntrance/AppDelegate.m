//
//  AppDelegate.m
//  WordsRemindTool
//
//  Created by czc on 2020/8/17.
//  Copyright © 2020 czc. All rights reserved.
//

#import "AppDelegate.h"
#import "CZCTabBarController.h"
#import "CZCPlusButton.h"
#import "SFHFKeychainUtils.h"
#import <AuthenticationServices/AuthenticationServices.h>
@import TXLiteAVSDK_UGC;
#import <UMCommon/UMCommon.h>
#import <BUAdSDK/BUAdSDK.h>

@interface AppDelegate ()<WXApiDelegate,BUSplashAdDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self createAppWindow];
        // 苹果登录
    [TXUGCBase setLicenceURL:kTXLicenceURL key:kTXLicenceKey];
    NSLog(@"SDK Version = %@", [TXLiveBase getSDKVersionStr]);
    //向微信注册
    [WXApi registerApp:WEIXIN_APP_ID universalLink:@"https://www.dialogue.szshht.cn.promptlines/"];
    //注册友盟
    [UMConfigure initWithAppkey:@"5f478b529bb213091b8ada2e" channel:@"App Store"];
    //穿山甲
    [BUAdSDKManager setAppID:@"5109158"];
    
    CZCBaseInfoModel * model = [CZCBaseInfoModel readAPPBaseInfo:@"ios_ad"];
    
    if((model && [model.val intValue] == 1) && !USER_DEFAULTS_GET(kLYCLUserVipInfo)){
        
        CGRect frame = [UIScreen mainScreen].bounds;
        BUSplashAdView *splashView = [[BUSplashAdView alloc] initWithSlotID:@"887386491" frame:frame];
       splashView.delegate = self;
       UIWindow *keyWindow = [UIApplication sharedApplication].windows.firstObject;
       [splashView loadAdData];
       [keyWindow.rootViewController.view addSubview:splashView];
       splashView.rootViewController = keyWindow.rootViewController;
    }
    

    
    if (@available(iOS 13.0, *)) {
        [self getAppleLoginStatus];
        [self observeAppleSignInState];
    }
    return YES;
}

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(nullable UIWindow *)window{
    if (_allowRotation == YES) {   // 如果属性值为YES,仅允许屏幕向左旋转,否则仅允许竖屏
         return UIInterfaceOrientationMaskAll;;  // 这里是屏幕要旋转的方向
    }else{
        if(_shwoWordSetting){
            if(RM.isVerticalMode){
                
                return UIInterfaceOrientationMaskPortrait;
            }else{
                return UIInterfaceOrientationMaskLandscapeRight;
            }
        }else{
            return UIInterfaceOrientationMaskPortrait;
        }
        
    }
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    
    [WXApi handleOpenURL:url delegate:self];//微信登陆和分享回调url处理
    [QQApiInterface handleOpenURL:url delegate:self];//QQ分享回调url处理
    [TencentOAuth HandleOpenURL:url];//!!!如果不写, 不走QQ登陆回调代理方法

    return true;
}


- (void)splashAdDidClose:(BUSplashAdView *)splashAd {
    [splashAd removeFromSuperview];
}

#pragma mark - WXApiDelegate
- (void)onResp:(BaseResp *)resp {
    //登录
    if ([resp isKindOfClass: [SendAuthResp class]]) {
        SendAuthResp* authResp = (SendAuthResp*)resp;
        
        if (authResp.errCode == 0) {
            // User confirm authorization
            [[NSNotificationCenter defaultCenter] postNotificationName: NOTI_WXLOGIN_AUTHORIZED
                                                                object: authResp.code];
        } else {
            // User cancel login
            [[NSNotificationCenter defaultCenter] postNotificationName: NOTI_WXLOGIN_USERCANCELLED
                                                                object: nil];
        }
    }
    
    
}








- (void)createAppWindow{
    
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];

    CZCTabBarController *tabbarVC = [[CZCTabBarController alloc]init];
    [self.window setRootViewController:tabbarVC];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    [self customizeTabBarInterface];
}

// 自定义 TabBar 字体、背景、阴影
- (void)customizeTabBarInterface {

    // 设置文字属性
    if (@available(iOS 10.0, *)) {
        [self cyl_tabBarController].tabBar.unselectedItemTintColor = [UIColor cyl_systemGrayColor];
        [self cyl_tabBarController].tabBar.tintColor = [UIColor cyl_labelColor];
    } else {
        UITabBarItem *tabBar = [UITabBarItem appearance];
        // 普通状态下的文字属性
        [tabBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor cyl_systemGrayColor]}
                              forState:UIControlStateNormal];
        // 选中状态下的文字属性
        [tabBar setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor cyl_labelColor]}
                              forState:UIControlStateSelected];
    }

    
    // 去除 TabBar 自带的顶部阴影
    [[self cyl_tabBarController] hideTabBarShadowImageView];
    
    // 设置 TabBar 阴影
    CYLTabBarController *tabBarController = [self cyl_tabBarController];
    tabBarController.tabBar.layer.shadowColor = [UIColor blackColor].CGColor;
    tabBarController.tabBar.layer.shadowRadius = 15.0;
    tabBarController.tabBar.layer.shadowOpacity = 0.2;
    tabBarController.tabBar.layer.shadowOffset = CGSizeMake(0, 3);
    tabBarController.tabBar.layer.masksToBounds = NO;
    tabBarController.tabBar.clipsToBounds = NO;
}

#pragma mark - 苹果登录相关，获取当前用户的授权状态
- (void)getAppleLoginStatus API_AVAILABLE(ios(13.0))
{
    // 用于以下两种状况处理相关情况  退出登录操作，或者重新登录
    //∙ 用户终止 App 中使用 Sign in with Apple 功能
    //∙ 用户在设置里注销了 AppleId
    NSString *userIdentifier = [SFHFKeychainUtils getPasswordForUsername:kYBGLoginWithAppleUserName andServiceName:kYBGLoginWithAppleServer error:nil];;
    if (userIdentifier) {
        ASAuthorizationAppleIDProvider *appleIDProvider = [ASAuthorizationAppleIDProvider new];
        [appleIDProvider getCredentialStateForUserID:userIdentifier completion:^(ASAuthorizationAppleIDProviderCredentialState credentialState, NSError * _Nullable error)
         {
            switch (credentialState) {
                case ASAuthorizationAppleIDProviderCredentialAuthorized:
                    // 授权状态有效；
                    break;
                case ASAuthorizationAppleIDProviderCredentialRevoked:
                    // 上次使用苹果账号登录的凭据已被移除，需解除绑定并重新引导用户使用苹果登录；
                    break;
                case ASAuthorizationAppleIDProviderCredentialNotFound:
                    // 未登录授权，直接弹出登录页面，引导用户登录。
                    break;
                case ASAuthorizationAppleIDProviderCredentialTransferred:
                    
                    break;
            }
        }];
    }
}
#pragma mark - 监听苹果登录revoked状态,注销用户
- (void)observeAppleSignInState API_AVAILABLE(ios(13.0))
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSignInWithAppleStateChanged:) name:ASAuthorizationAppleIDProviderCredentialRevokedNotification object:nil];
}

- (void)handleSignInWithAppleStateChanged:(NSNotification *)notification
{
    // Sign the user out, optionally guide them to sign in again
    NSLog(@"%@", notification.userInfo);
}






@end
