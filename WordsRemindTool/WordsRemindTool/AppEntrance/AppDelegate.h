//
//  AppDelegate.h
//  WordsRemindTool
//
//  Created by czc on 2020/8/17.
//  Copyright © 2020 czc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;
@property(nonatomic,assign)BOOL allowRotation;//是否允许转向
@property (nonatomic,assign) BOOL shwoWordSetting;

@end

