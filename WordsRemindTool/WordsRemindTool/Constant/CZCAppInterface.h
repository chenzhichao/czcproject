//
//  CZCAppInterface.h
//  WordsRemindTool
//
//  Created by czc on 2020/8/17.
//  Copyright © 2020 czc. All rights reserved.
//

#ifndef CZCAppInterface_h
#define CZCAppInterface_h

#define kCZCURL @"https://dialogue.szshht.cn/"

//APP初始化
#define kCZCUserAccess @"api/Login/userAccess"

//检查版本
#define kCZCCheckVersion @"api/System/checkVersion"

//获取系统配置接口
#define kCZCGetConfig @"api/System/getConfig"

//客户端获取vip价格
#define kCZCGetVipPrice @"api/System/getVipPrice"

//客户端获取url地址
#define kCZCGetUrl @"api/System/getUrl"

//登录
#define kCZCUserLogin @"api/Login/login"

//获取用户包月信息
#define kCZCUserGetVipInfo @"api/User/getVipInfo"

//获取用户信息
#define kCZCUserGetUserInfo @"api/User/getUserInfo"

//购买包月VIP
#define kCZCUserBuyVip @"api/Order/buyVip"

//支付请求接口
#define kCZCPayCallBack @"api/PayCallBack/iosPayCallBack"


//IOS请求VIP同步接口
#define kCZCPaySync @"api/PayCallBack/iosSync"

//提交试用
#define kCZCPutProbation  @"api/User/putProbation"

//提词记录
#define kCZCDialogueList @"api/Dialogue/dialogueList"

//用户新增提词
#define kCZCAddDialogue @"api/Dialogue/addDialogue"

#define kCZCSearchDialogue @"api/Dialogue/searchDialogue"
//用户修改提词
#define kCZCEditDialogue @"api/Dialogue/editDialogue"
//用户删除提词
#define kCZCDeleteDialogue @"api/Dialogue/delDialogue"

//微信token链接
#define kCZCWeixinAccessToken @"https://api.weixin.qq.com/sns/oauth2/access_token"
#define kCZCWeixinUserInfo @"https://api.weixin.qq.com/sns/userinfo"
#define kCZCWeixiRefreshToken @"https://api.weixin.qq.com/sns/oauth2/refresh_token"




//通知NotifyCenter

//微信登录
#define NOTI_WXLOGIN_AUTHORIZED @"NotifyWXLoginAuthrized"
#define NOTI_WXLOGIN_USERCANCELLED @"NotifyWXLoginCancelled"



#define NOTI_USERLOGIN @"NotifyUserLogin"

#define NOTI_PAYSUCCESS @"NotifyPaySuccess"
//屏幕方向
#define NOTI_VERTICAL_MODE @"NotifyVerticalModel"
//新增题词
#define NOTI_ADD_REMIND @"NotifyAddRemind"
//提词器设置
#define NOTI_WORD_SETTING @"NotifyWordSetting"


#endif /* CZCAppInterface_h */
