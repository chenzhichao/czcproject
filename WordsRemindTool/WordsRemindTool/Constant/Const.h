//
//  Const.h
//  WordsRemindTool
//
//  Created by iOS on 2020/8/18.
//  Copyright © 2020 czc. All rights reserved.
//

#import <Foundation/Foundation.h>

//**************
//登录标识
#define kLYCLoginUserId @"LYCLoginUserId"

#define kLYCLUserVipInfo @"kLYCLUserVipInfo"

#define kLYCLFreeNumWord @"kLYCFreeNumWord"
#define kLYCLFreeNumVideo @"kLYCFreeNumVideo"
//已经登陆过的用户
#define kLYCLastLogin @"kLYCLastLogin"
//支付凭证
#define kLYCReceiptData @"kLYCReceiptData"

//苹果登录
static NSString *const kYBGLoginWithAppleUserName = @"YBGLoginWithAppleUserID";
static NSString *const kYBGLoginWithAppleServer = @"YBGLoginWithAppleServer";


static NSString *const kTXLicenceURL = @"http://license.vod2.myqcloud.com/license/v1/282aa4a96cc1a38775c55e5dd48ab12d/TXUgcSDK.licence";
static NSString *const kTXLicenceKey = @"6921613a55324e20a50fedd09ec6e2c0";


static NSString *const WEIXIN_APP_ID = @"wx8b74be8358bf4a2c";
static NSString *const WEIXIN_SECRET =  @"72ae062b1af396aa45acc4cf57d4c8c8";
static NSString *const QQ_APP_ID = @"101884827";

//*************


//非空判断
#define CheckNonNullNonEmpty(v) ((v) && ![(v) isEqual:[NSNull null]] && ![(v) isEqualToString:@""])

//获取RGB颜色
#define kRGBColor(Red,Green,Blue,Alpha) [UIColor colorWithRed:(Red)/255.0 green:(Green)/255.0 blue:(Blue)/255.0 alpha:(Alpha)]


#define kScreenW [[UIScreen mainScreen] bounds].size.width
#define KScreenH [[UIScreen mainScreen] bounds].size.height

#define kLYCScaleWidth(Width) ((kScreenW<375.0?375.0:kScreenW)*Width)/375.0


//x 和 xs 一样
#define kDevice_iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)]?CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size):NO)
//xr
#define kDevice_iPhoneXr ([UIScreen instancesRespondToSelector:@selector(currentMode)]?CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size):NO)
//xs_max
#define kDevice_iPhoneXs ([UIScreen instancesRespondToSelector:@selector(currentMode)]?CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size):NO)

#define kLYCTabBarHeight (kDevice_iPhoneX==YES || kDevice_iPhoneXr==YES || kDevice_iPhoneXs==YES ? 83:49)
#define kLYCNavigationBarHeight 44
#define kLYCStatusBarHeight (kDevice_iPhoneX==YES || kDevice_iPhoneXr==YES || kDevice_iPhoneXs==YES ? 44 : 20)
#define kLYCNavigationStatusBarHeight (44+kLYCStatusBarHeight)
#define kLYCStatusBarSeaHeight   (kDevice_iPhoneX==YES || kDevice_iPhoneXr==YES || kDevice_iPhoneXs==YES ? 24 : 0)
#define kLYCTabBarSeaHeight       (kDevice_iPhoneX==YES || kDevice_iPhoneXr==YES || kDevice_iPhoneXs==YES ? 34 : 0)


#define kThemeColor kRGBColor(254,230,13,1)
#define kThemeBgColor kRGBColor(32,32,32,1)
#define kThemeGrayColor kRGBColor(242,242,242,1)

//设置userDefault的value
#define USER_DEFAULTS_SET(__OBJECT,__KEY) {\
[[NSUserDefaults standardUserDefaults] setObject:__OBJECT forKey:__KEY];\
[[NSUserDefaults standardUserDefaults] synchronize];}

//获取userDefault的value
#define USER_DEFAULTS_GET(__KEY) ([[NSUserDefaults standardUserDefaults] objectForKey:__KEY])


//APPID、APPURL
#define APPID  @"1531272022"
#define APP_URL [NSString stringWithFormat:@"https://itunes.apple.com/cn/app/id%@?mt=8", APPID]







@interface Const : NSObject


@end

